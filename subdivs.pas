unit subdivs;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, FireDAC.Stan.Intf, FireDAC.Stan.Param,
  FireDAC.Phys.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, MemTableDataEh, Data.DB,
  MemTableEh, Vcl.Menus, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  Vcl.StdCtrls, Vcl.Buttons, PngSpeedButton, EhLibVCL, GridsEh, DBAxisGridsEh,
  DBGridEh, Vcl.ExtCtrls;

type
  TFormSubDivs = class(TFormGrid)
  private
    { Private declarations }
  public
    procedure ImportSheet(data:Variant; DimX:integer; DimY:integer); override;
    procedure Init; override;
  end;

var
  FormSubDivs: TFormSubDivs;

implementation

{$R *.dfm}

uses EditSubDiv;


procedure TFormSubDivs.Init;
begin
  inherited;
  self.FormEditLocal := FormEditSubDiv;
end;



procedure TFormSubDivs.ImportSheet(data:Variant; DimX:integer; DimY:integer);
var i, n_oktmo, n_code, n_name: integer; code, oktmo, name: string;
begin

  n_code := 0;
  n_name := 0;
  n_oktmo := 0;

  for i := 1 to DimX do
  begin

    if data[2,i]='��������' then
    n_code := i;

    if data[2,i]='������������' then
    n_name := i;

    if data[2,i]='�����' then
    n_oktmo := i;

  end;

  qrAux.Close;
  qrAux.SQL.Clear;
  qrAux.SQL.Add('insert into subdivs (code, name, oktmo)');
  qrAux.SQL.Add('select :code, :name, :oktmo where not exists (select 1 from subdivs s where s.code = :code1);');

  code := '';
  name := '';
  oktmo := '';

  for i := 3 to DimY do
  begin

      if n_code>0 then
      begin
        code := data[i,n_code];
        if (code = '') and (n_code+1<n_name) then code := data[i,n_code+1];
        if (code = '') and (n_code+2<n_name) then code := data[i,n_code+2];
      end;

      if n_name>0 then name := data[i,n_name];
      if n_oktmo>0 then oktmo := data[i,n_oktmo];

      if trim(code) = '' then continue;

      qrAux.Params.ParamByName('code').Value := code;
      qrAux.Params.ParamByName('code1').Value := code;
      qrAux.Params.ParamByName('name').Value := name;
      qrAux.Params.ParamByName('oktmo').Value := oktmo;

      try
        qrAux.ExecSQL;
      except
        on E : Exception do
        begin
          ShowMessage('������ ��� �������: '+E.Message);
          exit;
        end;
      end;
  end;

end;


end.
