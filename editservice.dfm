inherited FormEditService: TFormEditService
  Caption = #1059#1089#1083#1091#1075#1072
  ClientHeight = 366
  ClientWidth = 584
  ParentFont = False
  Font.Height = -13
  ExplicitWidth = 598
  ExplicitHeight = 405
  TextHeight = 17
  inherited plBottom: TPanel
    Top = 325
    Width = 584
    TabOrder = 4
    ExplicitTop = 325
    ExplicitWidth = 582
    inherited btnCancel: TButton
      Left = 468
      ExplicitLeft = 466
    end
    inherited btnOk: TButton
      Left = 349
      ParentFont = True
      ExplicitLeft = 347
    end
  end
  inherited edCode: TDBEditEh
    Height = 25
    ParentFont = True
    TabOrder = 0
    ExplicitHeight = 25
  end
  inherited edName: TDBEditEh
    Height = 25
    ParentFont = True
    TabOrder = 1
    ExplicitHeight = 25
  end
  object meNote: TDBMemoEh [3]
    Left = 8
    Top = 217
    Width = 561
    Height = 92
    ControlLabel.Width = 99
    ControlLabel.Height = 18
    ControlLabel.Caption = #1055#1086#1089#1090#1072#1085#1086#1074#1083#1077#1085#1080#1077
    ControlLabel.Font.Charset = DEFAULT_CHARSET
    ControlLabel.Font.Color = clWindowText
    ControlLabel.Font.Height = -15
    ControlLabel.Font.Name = 'Calibri'
    ControlLabel.Font.Style = []
    ControlLabel.ParentFont = False
    ControlLabel.Visible = True
    AutoSize = False
    BevelInner = bvNone
    BevelOuter = bvNone
    DataField = 'note'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 3
    Visible = True
    WantReturns = True
  end
  object edKBK: TDBEditEh [4]
    Left = 8
    Top = 152
    Width = 457
    Height = 25
    ControlLabel.Width = 24
    ControlLabel.Height = 18
    ControlLabel.Caption = #1050#1041#1050
    ControlLabel.Font.Charset = DEFAULT_CHARSET
    ControlLabel.Font.Color = clWindowText
    ControlLabel.Font.Height = -15
    ControlLabel.Font.Name = 'Calibri'
    ControlLabel.Font.Style = []
    ControlLabel.ParentFont = False
    ControlLabel.Visible = True
    DataField = 'kbk'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 2
    Visible = True
  end
  inherited dsLocal: TDataSource
    Left = 328
    Top = 32
  end
  inherited qrAux: TFDQuery
    Left = 280
    Top = 24
  end
end
