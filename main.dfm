object FormMain: TFormMain
  Left = 0
  Top = 0
  Caption = #1057#1091#1076#1077#1073#1085#1099#1077' '#1088#1077#1096#1077#1085#1080#1103', 2.0'
  ClientHeight = 607
  ClientWidth = 914
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = mm
  WindowState = wsMaximized
  TextHeight = 13
  object btClose: TPngSpeedButton
    Left = 893
    Top = 0
    Width = 21
    Height = 607
    Align = alRight
    Caption = 'X'
    Flat = True
    OnClick = btCloseClick
    ExplicitLeft = 1066
    ExplicitHeight = 21
  end
  object pc: TPageControl
    Left = 0
    Top = 0
    Width = 893
    Height = 607
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Calibri'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    OnChange = pcChange
    ExplicitWidth = 891
  end
  object mm: TMainMenu
    Left = 376
    Top = 204
    object N17: TMenuItem
      Caption = #1059#1095#1077#1090
      object N18: TMenuItem
        Caption = #1056#1077#1077#1089#1090#1088#1099' '#1087#1086#1089#1090#1072#1085#1086#1074#1083#1077#1085#1080#1081
        OnClick = N18Click
      end
    end
    object N3: TMenuItem
      Caption = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082#1080
      object N8: TMenuItem
        Caption = #1054#1090#1074#1077#1090#1095#1080#1082#1080
        OnClick = N8Click
      end
      object N10: TMenuItem
        Caption = '-'
      end
      object N7: TMenuItem
        Caption = #1059#1089#1083#1091#1075#1080
        OnClick = N7Click
      end
      object N11: TMenuItem
        Caption = #1051#1080#1094#1077#1074#1099#1077' '#1089#1095#1077#1090#1072
        OnClick = N11Click
      end
      object N15: TMenuItem
        Caption = #1057#1090#1088#1091#1082#1090#1091#1088#1085#1099#1077' '#1087#1086#1076#1088#1072#1079#1076#1077#1083#1077#1085#1080#1103
        OnClick = N15Click
      end
    end
    object N1: TMenuItem
      Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1072
      object N4: TMenuItem
        Caption = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1080
        OnClick = N4Click
      end
      object N9: TMenuItem
        Caption = #1055#1077#1095#1072#1090#1085#1099#1077' '#1092#1086#1088#1084#1099
        OnClick = N9Click
      end
      object N5: TMenuItem
        Caption = '-'
      end
      object N12: TMenuItem
        Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1080' '#1088#1072#1073#1086#1095#1077#1075#1086' '#1084#1077#1089#1090#1072
        OnClick = N12Click
      end
      object N13: TMenuItem
        Caption = '-'
      end
      object N6: TMenuItem
        Caption = #1055#1072#1088#1072#1084#1077#1090#1088#1099' '#1087#1086#1076#1082#1083#1102#1095#1077#1085#1080#1103' '#1082' '#1073#1072#1079#1077' '#1076#1072#1085#1085#1099#1093
        OnClick = N6Click
      end
      object N2: TMenuItem
        Caption = '-'
      end
      object N14: TMenuItem
        Caption = #1057#1084#1077#1085#1080#1090#1100' '#1087#1072#1088#1086#1083#1100
        OnClick = N14Click
      end
      object N16: TMenuItem
        Caption = '-'
      end
      object SQL1: TMenuItem
        Caption = #1042#1099#1087#1086#1083#1085#1080#1090#1100' SQL '#1079#1072#1087#1088#1086#1089
        OnClick = SQL1Click
      end
    end
  end
  object pmPC: TPopupMenu
    Left = 428
    Top = 204
  end
end
