inherited FormPrintForms: TFormPrintForms
  Caption = #1055#1077#1095#1072#1090#1085#1099#1077' '#1092#1086#1088#1084#1099
  ClientWidth = 932
  ExplicitWidth = 946
  TextHeight = 16
  inherited plMain: TPanel
    Width = 932
    ExplicitWidth = 930
    inherited dgMain: TDBGridEh
      Width = 932
      Columns = <
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'name'
          Footers = <>
          Title.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1092#1086#1088#1084#1099
          Width = 363
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'template'
          Footers = <>
          Title.Caption = #1064#1072#1073#1083#1086#1085
          Width = 361
        end>
    end
    inherited plTop: TPanel
      Width = 932
      ExplicitWidth = 930
    end
    inherited plBottom: TPanel
      Width = 932
      ExplicitWidth = 930
      inherited btnCancel: TButton
        Left = 816
        ExplicitLeft = 814
      end
      inherited btnOk: TButton
        Left = 697
        ExplicitLeft = 695
      end
    end
  end
  inherited FDUpdateSet: TFDUpdateSQL
    InsertSQL.Strings = (
      
        'INSERT INTO public.printforms (name, template, group_field, sql_' +
        'text, unit_num)'
      'values (:name, :template, :group_field, :sql_text, :unit_num) '
      'returning id;')
    ModifySQL.Strings = (
      'UPDATE public.printforms'
      'SET name=:name, template=:template, group_field=:group_field, '
      'sql_text = :sql_text, unit_num = :unit_num'
      'WHERE id=:id;')
    DeleteSQL.Strings = (
      'delete from printforms where id = :id;')
    FetchRowSQL.Strings = (
      'select * from printforms where id = :id;')
  end
  inherited FDDataQr: TFDQuery
    SQL.Strings = (
      'select * from printforms')
  end
end
