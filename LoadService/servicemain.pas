unit servicemain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.SvcMgr, Vcl.Dialogs,
  Vcl.ExtCtrls, IniFiles, Xml.xmldom, Xml.XMLIntf, Xml.XMLDoc, Data.DB, Variants, Data.Win.ADODB;

type TFakeAdoQuery = class(TAdoQuery);

type
  TJDLoadService = class(TService)
    Timer1: TTimer;
    procedure ServiceStart(Sender: TService; var Started: Boolean);
    procedure ServiceStop(Sender: TService; var Stopped: Boolean);
    procedure Timer1Timer(Sender: TObject);
  private
    { Private declarations }
  public

    dirname: string;
    connStr: string;
    supplier: string;
    document: string;
    catalog: string;
    pcatalog: string;
    jcatalog: string;

    root_catalog_tid: integer;
    root_catalog_id: string;

    person_catalog_tid: integer;
    person_catalog_id: string;

    jperson_catalog_tid: integer;
    jperson_catalog_id: string;

    contr_catalog_tid: integer;
    contr_catalog_id: string;

    so_tid: integer;
    so_id: string;

    rtid: integer;
    rid: string;

    do_tid: integer;
    do_id: string;

    ct_tid: integer;
    ct_id: string;

    stid: integer;
    sid: string;

    account_tid: integer;

    function GetServiceController: TServiceController; override;
    function QueryExec(ds: TAdoQuery): boolean;
    procedure Init;
    procedure LoadFilesFromUploadDir(dirname:string);
    procedure LoadFile(fn:string);
    procedure CreateDoc(node: IXMLNode);
    procedure AddRowLog(mess: string);

    procedure GetDictData(tn: string; codeName, codeValue: string; var tid: integer; var id: string; var ds: TDataSet; wherefilter:string='');
    procedure GetCatalog(startcatalog, catalogname: string; var tid: integer; var id: string);
    procedure GetRootCatalog(catalogname: string; var tid: integer; var id: string);
    procedure GetAccountTypeTid(var tid: integer);

  end;

var
  JDLoadService: TJDLoadService;

implementation

{$R *.dfm}

uses main;

procedure TJDLoadService.Init;
var inf: TIniFile;
begin
  SetCurrentDir(ExtractFileDir(ParamStr(0)));
  inf := TIniFile.Create(ExtractFileDir(ParamStr(0))+'/settings.ini');
  connStr := inf.ReadString('main','ConnectionString','');
  dirname := inf.ReadString('main','UnloadFolder','');
  supplier := inf.ReadString('main','DefaultSupplier','');
  document := inf.ReadString('main','DocumentCode','');
  catalog := inf.ReadString('main','DefaultCatalog','');
  pcatalog := inf.ReadString('main','PersonCatalog','');
  jcatalog := inf.ReadString('main','JPersonCatalog','');
end;


procedure ServiceController(CtrlCode: DWord); stdcall;
begin
  JDLoadService.Controller(CtrlCode);
end;

function TJDLoadService.GetServiceController: TServiceController;
begin
  Result := ServiceController;
end;

procedure TJDLoadService.ServiceStart(Sender: TService; var Started: Boolean);
begin
  Init;
  Timer1.Enabled := true;
end;

procedure TJDLoadService.ServiceStop(Sender: TService; var Stopped: Boolean);
begin
  Timer1.Enabled := false;
end;

procedure TJDLoadService.Timer1Timer(Sender: TObject);
begin
  LoadFilesFromUploadDir(dirname);
end;

procedure TJDLoadService.LoadFilesFromUploadDir(dirname:string);
var NFound: integer; SR: TSearchRec;
begin

  FormMain.meLog.Lines.Clear;

  NFound := FindFirst(dirname + '\*.xml', faAnyFile, SR);

  AddRowLog('����� ������ ��������.');

  while NFound = 0 do
  begin
    LoadFile(dirname+'\'+SR.Name);
    NFound := FindNext(SR);
  end;

  FindClose(SR);

end;

procedure TJDLoadService.LoadFile(fn:string);
var XMLDoc: TXMLDocument; node_root, node: IXMLNode; ds: TDataSet;
begin

  try
    XMLDoc := TXMLDocument.Create(FormMain);
    XMLDoc.LoadFromFile(fn);
  except
    AddRowLog('�� ������� ��������� ���� '+fn);
    exit;
  end;

  node_root := XMLDoc.ChildNodes.FindNode('������������������������');

  if node_root = nil then
  begin
    AddRowLog('���� '+fn+' �� �������� ������ �������� ���������� �� ��������� �������� �������.');
    exit;
  end;

  GetRootCatalog(self.catalog, root_catalog_tid, root_catalog_id);
  GetCatalog('���������� ����', pcatalog, person_catalog_tid, person_catalog_id);
  GetCatalog('����������� ����', jcatalog, jperson_catalog_tid, jperson_catalog_id);
  GetCatalog('�����������', '', contr_catalog_tid, contr_catalog_id);
  GetAccountTypeTid(account_tid);

  GetDictData('ParusBusinessCounteragent','Mnemo',self.supplier, so_tid, so_id, ds);
  rtid := so_tid;
  rid := so_id;

  GetDictData('ParusBusinessTypePersonalDocuments','Mnemo',self.document, do_tid, do_id, ds);
  GetDictData('ParusBusinessKindContactInformation','Name','������� �������', ct_tid, ct_id, ds);

  with FormMain do
  begin
    qrDict.Close;
    qrDict.SQL.Clear;
    qrDict.SQL.Add('select * from ParusBusinessServedOrganization where Counteragent_id = :id');
    qrDict.Parameters.ParamByName('id').Value := so_id;
    qrDict.Open;
    so_tid := qrDict.FieldByName('tid').AsInteger;
    so_id := qrDict.FieldByName('id').AsString;
  end;

  // ����������
  node := node_root.ChildNodes.FindNode('����������');
  while not (node = nil) do
  begin
    CreateDoc(node);
    FormMain.meLog.Lines.Add('�������� �������� � '+node.ChildNodes.FindNode('doc_num').Text+' �� ����� '+fn);
    node := node.NextSibling;
  end;

  // ��������������� ����
  RenameFile(fn,fn+'.bak');

  AddRowLog('�������� ���� '+fn);

end;

procedure TJDLoadService.CreateDoc(node: IXMLNode);
var ctid, tid, charge_tid, parameter_tid, ptid, service_tid: integer; cid, id, charge_id, parameter_id, pid, service_id, BillFor: string; ds: TDataSet;

procedure GenNewId(tn: string; var tid: integer; var id: string);
var ad_id: string;
begin

    with FormMain do
    begin

      qrAux.Close;
      qrAux.SQL.Text := 'select tid, newid(), ad_id from realtypes where lower(DbTable) = lower(:tablename)';
      qrAux.Parameters.ParamByName('tablename').Value := tn;
      qrAux.Open;

      tid := qrAux.Fields[0].AsInteger;
      id := qrAux.Fields[1].AsString;
      ad_id := qrAux.Fields[2].AsString;

      qrAux.Close;
      qrAux.SQL.Text := 'insert into object (tid,id,AD_id) values (:tid,:id,:ad_id)';
      qrAux.Parameters.ParamByName('tid').Value := tid;
      qrAux.Parameters.ParamByName('id').Value := id;
      qrAux.Parameters.ParamByName('ad_id').Value := ad_id;
      qrAux.ExecSql;

    end;

end;

procedure GetApproachCatalog(starttid: integer; startid: string; oktmo: string; var tid: integer; var id: string);
begin

    with FormMain do
    begin
      qrDict.Close;
      qrDict.SQL.Clear;
      qrDict.SQL.Add('select tid, id from ParusNetServerSystemCatalog ');
      qrDict.SQL.Add('where CHARINDEX( '''+oktmo+''', DisplayName )>0 and HierarchyLink_id = :startid');
      qrDict.Parameters.ParamByName('startid').Value := startid;
      qrDict.Open;

      if qrDict.RecordCount>0 then
      begin
        tid := qrDict.Fields[0].AsInteger;
        id := qrDict.Fields[1].AsString;
      end else
      begin
        tid := starttid;
        id := startid;
      end;

    end;

end;

function GetNodeValue(nodename: string): string;
var sub_node: IXMLNode;
begin
  result := '';
  sub_node := node.ChildNodes.FindNode(nodename);
  if sub_node<>nil then
  begin
    result := sub_node.Text;
  end;

end;


function GetNodeDateValue(nodename: string): Variant;
var sub_node: IXMLNode;
begin

  result := '';
  sub_node := node.ChildNodes.FindNode(nodename);

  if sub_node<>nil then
  begin
    if sub_node.Text<>'' then
     result := StrToDate(sub_node.Text)
    else
     result := null;
  end else
    result := null;

end;

procedure CreatePerson(var tid: integer; var id: string);
var pname, fam, ima, otc, mnemo, pid, did: string; dtid, ptid: integer;
begin

  with FormMain do
  begin

    mnemo := GetNodeValue('surname')+' '+copy(GetNodeValue('firstname'),1,1)+'.'+copy(GetNodeValue('secondname'),1,1)+'.';

    qrAux.Close;
    qrAux.SQL.Clear;
    qrAux.SQL.Add('select 1 from ParusBusinessPerson p where p.Mnemo = :mnemo');
    qrAux.Parameters.ParamByName('mnemo').Value := mnemo;
    qrAux.Open;

    if qrAux.RecordCount>0 then
    begin
      mnemo := mnemo+'1';
      qrAux.Close;
      qrAux.Parameters.ParamByName('mnemo').Value := mnemo;
      qrAux.Open;
    end;

    if qrAux.RecordCount>0 then
    begin
      mnemo := mnemo+'2';
      qrAux.Close;
      qrAux.Parameters.ParamByName('mnemo').Value := mnemo;
      qrAux.Open;
    end;

    if qrAux.RecordCount>0 then
    begin
      mnemo := mnemo+'3';
      qrAux.Close;
      qrAux.Parameters.ParamByName('mnemo').Value := mnemo;
      qrAux.Open;
    end;

    GenNewId('ParusBusinessPerson',ptid,pid);

    qrAux.Close;
    qrAux.SQL.Clear;
    qrAux.SQL.Add('insert into ParusBusinessPerson (');
    qrAux.SQL.Add('tid,id,Mnemo,Surnam,Name,SecondName,INN,DateBirth,Resident,Sex,InsuranceNumber,Catalog_tid,Catalog_id');
    qrAux.SQL.Add(') values (');
    qrAux.SQL.Add(':tid,:id,:Mnemo,:Surnam,:Name,:SecondName,:INN,:DateBirth,:Resident,:Sex,:InsuranceNumber,:Catalog_tid,:Catalog_id');
    qrAux.SQL.Add(')');

    qrAux.Parameters.ParamByName('tid').Value := ptid;
    qrAux.Parameters.ParamByName('id').Value := pid;
    qrAux.Parameters.ParamByName('Mnemo').Value := mnemo;
    qrAux.Parameters.ParamByName('Surnam').Value := GetNodeValue('surname');
    qrAux.Parameters.ParamByName('Name').Value := GetNodeValue('firstname');
    qrAux.Parameters.ParamByName('Secondname').Value := GetNodeValue('secondname');
    qrAux.Parameters.ParamByName('INN').Value := copy(GetNodeValue('inn'),1,12);
    qrAux.Parameters.ParamByName('DateBirth').Value := GetNodeDateValue('birthdate');
    qrAux.Parameters.ParamByName('Resident').Value := 1;

    if GetNodeValue('sex')='0' then
      qrAux.Parameters.ParamByName('Sex').Value := '{6FA4A521-E2EF-437D-94DA-8F8666A8A369}'
    else
      qrAux.Parameters.ParamByName('Sex').Value := '{858DB74A-370E-4A57-9D8F-72A8E792A441}';

    qrAux.Parameters.ParamByName('InsuranceNumber').Value := GetNodeValue('snils');
    qrAux.Parameters.ParamByName('Catalog_tid').Value := person_catalog_tid;
    qrAux.Parameters.ParamByName('Catalog_id').Value := person_catalog_id;

    if not QueryExec(qrAux) then
    begin
      AddRowLog('������ ������� ������ �������.');
      exit;
    end;

    GenNewId('ParusBusinessCounteragent',tid,id);

    qrAux.Close;
    qrAux.SQL.Clear;
    qrAux.SQL.Add('insert into ParusBusinessCounteragent (');
    qrAux.SQL.Add('tid,id,Mnemo,Person_tid,Person_id,Catalog_tid,Catalog_id');
    qrAux.SQL.Add(') values (');
    qrAux.SQL.Add(':tid,:id,:Mnemo,:Person_tid,:Person_id,:Catalog_tid,:Catalog_id');
    qrAux.SQL.Add(')');

    qrAux.Parameters.ParamByName('tid').Value := tid;
    qrAux.Parameters.ParamByName('id').Value := id;
    qrAux.Parameters.ParamByName('Mnemo').Value := mnemo;
    qrAux.Parameters.ParamByName('Person_tid').Value := ptid;
    qrAux.Parameters.ParamByName('Person_id').Value := pid;
    qrAux.Parameters.ParamByName('Catalog_tid').Value := contr_catalog_tid;
    qrAux.Parameters.ParamByName('Catalog_id').Value := contr_catalog_id;

    if not QueryExec(qrAux) then
    begin
      AddRowLog('������ ������� ������ �������.');
      exit;
    end;

    // ������� ��������

    if (GetNodeValue('docwhen')<>'')
    and (GetNodeValue('docseria')<>'')
    and (GetNodeValue('docnumber')<>'')
    then begin

      GenNewId('ParusBusinessPersonalDocuments',dtid,did);

      qrAux.Close;
      qrAux.SQL.Clear;
      qrAux.SQL.Add('insert into ParusBusinessPersonalDocuments (');
      qrAux.SQL.Add('tid, id, SeriesDocument, NumberDocument, DateDistribution, DocumentGiven, DateFrom, Person_tid, Person_id, TypePersonalDocuments_tid, TypePersonalDocuments_id');
      qrAux.SQL.Add(') values (');
      qrAux.SQL.Add(':tid, :id, :SeriesDocument, :NumberDocument, :DateDistribution, :DocumentGiven, :DateFrom, :Person_tid, :Person_id, :TypePersonalDocuments_tid, :TypePersonalDocuments_id');
      qrAux.SQL.Add(')');

      qrAux.Parameters.ParamByName('tid').Value := dtid;
      qrAux.Parameters.ParamByName('id').Value := did;
      qrAux.Parameters.ParamByName('SeriesDocument').Value := GetNodeValue('docseria');
      qrAux.Parameters.ParamByName('NumberDocument').Value := GetNodeValue('docnumber');
      qrAux.Parameters.ParamByName('DateDistribution').Value := GetNodeDateValue('docwhen');
      qrAux.Parameters.ParamByName('DocumentGiven').Value := GetNodeValue('docwho');
      qrAux.Parameters.ParamByName('DateFrom').Value := GetNodeDateValue('docwhen');
      qrAux.Parameters.ParamByName('Person_tid').Value := ptid;
      qrAux.Parameters.ParamByName('Person_id').Value := pid;
      qrAux.Parameters.ParamByName('TypePersonalDocuments_tid').Value := do_tid;
      qrAux.Parameters.ParamByName('TypePersonalDocuments_id').Value := do_id;

      if not QueryExec(qrAux) then
      begin
        AddRowLog('������ ������������� ��������� �������.');
      end;

    end else
    begin
        AddRowLog('������ ������������� ��������� �� �������������.');
    end;

  end;

end;

procedure GetCounteragent(code: string; var tid: integer; var id: string);
begin

   tid := 0;
   id := '';

   with FormMain do
   begin
     qrAux.Close;
     qrAux.SQL.Clear;
     qrAux.SQL.Add('select c.tid, c.id from ParusBusinessCounteragent c, ParusBusinessJuridicalPerson p');
     qrAux.SQL.Add('where c.JuridicalPerson_id = p.id and p.Mnemo = :code');
     qrAux.Parameters.ParamByName('code').Value := code;
     qrAux.Open;

     if qrAux.RecordCount >0 then
     begin
       tid := qrAux.Fields[0].AsInteger;
       id := qrAux.Fields[1].AsString;
     end;

    end;

end;


procedure CreateJurPerson(var tid: integer; var id: string);
var pname, mnemo: string;
var ptid: integer; pid: string;
begin

  with FormMain do
  begin

    mnemo := GetNodeValue('person_code');

    qrAux.Close;
    qrAux.SQL.Clear;
    qrAux.SQL.Add('select 1 from ParusBusinessJuridicalPerson p where p.Mnemo = :mnemo');
    qrAux.Parameters.ParamByName('mnemo').Value := mnemo;
    qrAux.Open;

    if qrAux.RecordCount>0 then
    begin
      mnemo := mnemo+'1';
      qrAux.Close;
      qrAux.Parameters.ParamByName('mnemo').Value := mnemo;
      qrAux.Open;
    end;

    if qrAux.RecordCount>0 then
    begin
      mnemo := mnemo+'2';
      qrAux.Close;
      qrAux.Parameters.ParamByName('mnemo').Value := mnemo;
      qrAux.Open;
    end;

    if qrAux.RecordCount>0 then
    begin
      mnemo := mnemo+'3';
      qrAux.Close;
      qrAux.Parameters.ParamByName('mnemo').Value := mnemo;
      qrAux.Open;
    end;

    GenNewId('ParusBusinessJuridicalPerson',ptid,pid);

    qrAux.Close;
    qrAux.SQL.Clear;
    qrAux.SQL.Add('insert into ParusBusinessJuridicalPerson (');
    qrAux.SQL.Add('tid, id, Mnemo, Name, INN, CheckInnResult, Catalog_tid, Catalog_id');
    qrAux.SQL.Add(') values (');
    qrAux.SQL.Add(':tid, :id, :Mnemo, :Name, :INN, :CheckInnResult, :Catalog_tid, :Catalog_id');
    qrAux.SQL.Add(')');
    qrAux.Parameters.ParamByName('tid').Value := ptid;
    qrAux.Parameters.ParamByName('id').Value := pid;
    qrAux.Parameters.ParamByName('Mnemo').Value := mnemo;
    qrAux.Parameters.ParamByName('Name').Value := GetNodeValue('person_name');
    qrAux.Parameters.ParamByName('CheckInnResult').Value := '{5187208C-A30F-4C9E-97B4-F52C7D03FD3C}';
    qrAux.Parameters.ParamByName('INN').Value := GetNodeValue('inn');
    qrAux.Parameters.ParamByName('Catalog_tid').Value := jperson_catalog_tid;
    qrAux.Parameters.ParamByName('Catalog_id').Value := jperson_catalog_id;

    if not QueryExec(qrAux) then
    begin
      AddRowLog('������ ������� ������ (����������� ����) �������.');
      exit;
    end;

    GenNewId('ParusBusinessCounteragent',tid,id);

    qrAux.Close;
    qrAux.SQL.Clear;
    qrAux.SQL.Add('insert into ParusBusinessCounteragent (');
    qrAux.SQL.Add('tid,id,Mnemo,JuridicalPerson_tid,JuridicalPerson_id,Catalog_tid,Catalog_id');
    qrAux.SQL.Add(') values (');
    qrAux.SQL.Add(':tid,:id,:Mnemo,:JuridicalPerson_tid,:JuridicalPerson_id,:Catalog_tid,:Catalog_id');
    qrAux.SQL.Add(')');

    qrAux.Parameters.ParamByName('tid').Value := tid;
    qrAux.Parameters.ParamByName('id').Value := id;
    qrAux.Parameters.ParamByName('Mnemo').Value := mnemo;
    qrAux.Parameters.ParamByName('JuridicalPerson_tid').Value := ptid;
    qrAux.Parameters.ParamByName('JuridicalPerson_id').Value := pid;
    qrAux.Parameters.ParamByName('Catalog_tid').Value := contr_catalog_tid;
    qrAux.Parameters.ParamByName('Catalog_id').Value := contr_catalog_id;

    if not QueryExec(qrAux) then
    begin
      AddRowLog('������ ������� ������ (����������) �������.');
      exit;
    end;

  end;

end;

begin

  // ������� ��� ��������� �����������

  with FormMain do
  begin

      if GetNodeValue('person_type')='0' then
      begin

         qrAux.Close;
         qrAux.SQL.Clear;
         qrAux.SQL.Add('select c.tid, c.id from ParusBusinessCounteragent c, ParusBusinessPerson p');
         qrAux.SQL.Add('where c.Person_id = p.id and p.Surnam	= :surname and p.Name	= :firstname and p.SecondName = :secondname');
         qrAux.SQL.Add('and convert(varchar(10), DateBirth, 104) = :datebirth');
         qrAux.Parameters.ParamByName('surname').Value := GetNodeValue('surname');
         qrAux.Parameters.ParamByName('firstname').Value := GetNodeValue('firstname');
         qrAux.Parameters.ParamByName('secondname').Value := GetNodeValue('secondname');
         qrAux.Parameters.ParamByName('datebirth').Value := GetNodeValue('birthdate');
         qrAux.Open;

         {qrAux.Close;
         qrAux.SQL.Clear;
         qrAux.SQL.Add('select c.tid, c.id from ParusBusinessCounteragent c, ParusBusinessPerson p');
         qrAux.SQL.Add('where c.Person_id = p.id and p.INN = :inn');
         qrAux.Parameters.ParamByName('inn').Value := GetNodeValue('inn');
         qrAux.Open;

          if qrAux.RecordCount=0 then
          begin
            qrAux.Close;
            qrAux.SQL.Clear;
            qrAux.SQL.Add('select c.tid, c.id from ParusBusinessCounteragent c, ParusBusinessPerson p');
            qrAux.SQL.Add('where c.Person_id = p.id and p.InsuranceNumber = :snils');
            qrAux.Parameters.ParamByName('snils').Value := GetNodeValue('snils');
            qrAux.Open;
          end;}

          if qrAux.RecordCount >0 then
          begin
            ptid := qrAux.Fields[0].AsInteger;
            pid := qrAux.Fields[1].AsString;
          end else
          begin
            CreatePerson(ptid, pid);
          end;

      end else
      begin

       with FormMain do
       begin
         qrAux.Close;
         qrAux.SQL.Clear;
         qrAux.SQL.Add('select c.tid, c.id from ParusBusinessCounteragent c, ParusBusinessJuridicalPerson p');
         qrAux.SQL.Add('where c.JuridicalPerson_id = p.id and p.INN = :inn');
         qrAux.Parameters.ParamByName('inn').Value := GetNodeValue('inn');
         qrAux.Open;
        end;

        if qrAux.RecordCount >0 then
        begin
          ptid := qrAux.Fields[0].AsInteger;
          pid := qrAux.Fields[1].AsString;
        end else
        begin
          CreateJurPerson(ptid, pid);
        end;

      end;

   end;

  GenNewId('ParusBusinessUNIFOChargeType',charge_tid,charge_id);

  with FormMain do
  begin

    qrAux.Close;
    qrAux.SQL.Clear;
    qrAux.SQL.Add('insert into ParusBusinessUNIFOChargeType (');
    qrAux.SQL.Add('tid,id,ChangeStatus,IsGISGMP,SupplierOrgAccount_tid,SupplierOrgAccount_id,ServiceRecipient_tid,ServiceRecipient_id,');
    qrAux.SQL.Add('BC_tid,BC_id,Payer_tid,Payer_id, ServiceCatalog_tid,ServiceCatalog_id');
    qrAux.SQL.Add(') values (');
    qrAux.SQL.Add(':tid,:id,:ChangeStatus,:IsGISGMP,:SupplierOrgAccount_tid,:SupplierOrgAccount_id,:ServiceRecipient_tid,:ServiceRecipient_id,');
    qrAux.SQL.Add(':BC_tid,:BC_id,:Payer_tid,:Payer_id,:ServiceCatalog_tid,:ServiceCatalog_id');
    qrAux.SQL.Add(')');

    qrAux.Parameters.ParamByName('tid').Value := charge_tid;
    qrAux.Parameters.ParamByName('id').Value := charge_id;
    qrAux.Parameters.ParamByName('ChangeStatus').Value := '{1327640A-1E23-4C2D-8978-F9CBC1886BAC}';
    qrAux.Parameters.ParamByName('IsGISGMP').Value := 1;

    GetDictData('ParusBusinessAccountInFinancialInstitution','Number',GetNodeValue('account_number'),tid, id, ds, 'and tid=520');
    qrAux.Parameters.ParamByName('SupplierOrgAccount_tid').Value := account_tid;
    qrAux.Parameters.ParamByName('SupplierOrgAccount_id').Value := id;

    GetDictData('ParusBusinessUNIFOServiceCatalog','Code',GetNodeValue('service_code'),service_tid, service_id, ds,'');
    qrAux.Parameters.ParamByName('ServiceCatalog_tid').Value := service_tid;
    qrAux.Parameters.ParamByName('ServiceCatalog_id').Value := service_id;
    BillFor := ds.FieldByName('Narrative').AsString;

    qrAux.Parameters.ParamByName('BC_tid').Value := ds.FieldByName('BC_tid').AsInteger;
    qrAux.Parameters.ParamByName('BC_id').Value := ds.FieldByName('BC_id').AsString;

    qrAux.Parameters.ParamByName('Payer_tid').Value := ptid;
    qrAux.Parameters.ParamByName('Payer_id').Value := pid;

    //--
    //qrAux.Parameters.ParamByName('PayerAccount_tid').Value := '';
    //qrAux.Parameters.ParamByName('PayerAccount_id').Value := '';

    if rtid>0 then
    begin
      qrAux.Parameters.ParamByName('ServiceRecipient_tid').Value := rtid;
      qrAux.Parameters.ParamByName('ServiceRecipient_id').Value := rid;
    end else
    begin
      qrAux.Parameters.ParamByName('ServiceRecipient_tid').Value := null;
      qrAux.Parameters.ParamByName('ServiceRecipient_id').Value := null;
    end;


    if not QueryExec(qrAux) then
    begin
      AddRowLog('������ ������� ������ �������.');
      exit;
    end;

    qrAux.Close;
    qrAux.SQL.Clear;
    qrAux.SQL.Add('insert into ParusBusinessUNIFOBill (');
    qrAux.SQL.Add('tid, id, BillDate, BillFor, TotalAmount, SupplierBillID, Number, State,');
    qrAux.SQL.Add('Catalog_tid, Catalog_id, SupplierOrg_tid, SupplierOrg_id, StructuralSubdivision_tid, StructuralSubdivision_id');
    qrAux.SQL.Add(') values (');
    qrAux.SQL.Add(':tid, :id, :BillDate, :BillFor, :TotalAmount, :SupplierBillID, :Number, :State,');
    qrAux.SQL.Add(':Catalog_tid, :Catalog_id, :SupplierOrg_tid, :SupplierOrg_id, :StructuralSubdivision_tid, :StructuralSubdivision_id');
    qrAux.SQL.Add(')');

    qrAux.Parameters.ParamByName('tid').Value := charge_tid;
    qrAux.Parameters.ParamByName('id').Value := charge_id;
    qrAux.Parameters.ParamByName('BillDate').Value := GetNodeDateValue('doc_date');
    qrAux.Parameters.ParamByName('BillFor').Value := BillFor;
    qrAux.Parameters.ParamByName('TotalAmount').Value := StrToFloat(GetNodeValue('summa'));
    qrAux.Parameters.ParamByName('SupplierBillID').Value := GetNodeValue('uin');
    qrAux.Parameters.ParamByName('Number').Value := GetNodeValue('doc_num');
    qrAux.Parameters.ParamByName('State').Value := '{54004D34-6DD7-414C-B286-118CCDFDC6C7}';

    GetApproachCatalog(root_catalog_tid, root_catalog_id, GetNodeValue('oktmo'), ctid, cid);

    qrAux.Parameters.ParamByName('Catalog_tid').Value := ctid;
    qrAux.Parameters.ParamByName('Catalog_id').Value := cid;

    qrAux.Parameters.ParamByName('SupplierOrg_tid').Value := so_tid;
    qrAux.Parameters.ParamByName('SupplierOrg_id').Value := so_id;

    GetDictData('ParusBusinessStructuralSubdivision','Mnemo',GetNodeValue('subdiv_code'),stid, sid, ds);
    if stid>0 then
    begin
      qrAux.Parameters.ParamByName('StructuralSubdivision_tid').Value := stid;
      qrAux.Parameters.ParamByName('StructuralSubdivision_id').Value := sid;
    end else
    begin
      qrAux.Parameters.ParamByName('StructuralSubdivision_tid').Value := null;
      qrAux.Parameters.ParamByName('StructuralSubdivision_id').Value := null;
    end;

   QueryExec(qrAux);

   // ��������� ���������

    qrAux.Close;
    qrAux.SQL.Clear;
    qrAux.SQL.Add('select tid, id from ParusBusinessUNIFOServiceParameter ');
    qrAux.SQL.Add('where Name = :Name and ServiceCatalog_id = :ServiceCatalog_id');

    qrAux.Parameters.ParamByName('Name').Value := 'AltPayerIdentifier';
    qrAux.Parameters.ParamByName('ServiceCatalog_id').Value := service_id;

    qrAux.Open;

    parameter_tid := qrAux.Fields[0].AsInteger;
    parameter_id := qrAux.Fields[1].AsString;

    GenNewId('ParusBusinessUNIFOChargeTypeParameter',tid, id);

    qrAux.Close;
    qrAux.SQL.Clear;
    qrAux.SQL.Add('insert into ParusBusinessUNIFOChargeTypeParameter (');
    qrAux.SQL.Add('tid, id, Value, ChargeType_tid, ChargeType_id, ServiceParameter_tid, ServiceParameter_id');
    qrAux.SQL.Add(') values (');
    qrAux.SQL.Add(':tid, :id, :Value, :ChargeType_tid, :ChargeType_id, :ServiceParameter_tid, :ServiceParameter_id');
    qrAux.SQL.Add(')');

    qrAux.Parameters.ParamByName('tid').Value := tid;
    qrAux.Parameters.ParamByName('id').Value := id;
    qrAux.Parameters.ParamByName('Value').Value := GetNodeValue('docnumber');
    qrAux.Parameters.ParamByName('ChargeType_tid').Value := charge_tid;
    qrAux.Parameters.ParamByName('ChargeType_id').Value := charge_id;
    qrAux.Parameters.ParamByName('ServiceParameter_tid').Value := parameter_tid;
    qrAux.Parameters.ParamByName('ServiceParameter_id').Value := parameter_id;

    QueryExec(qrAux);

  end;

end;


procedure TJDLoadService.AddRowLog(mess: string);
var F: TextFile;
begin
  FormMain.meLog.Lines.Add(mess);
  FormMain.meLog.Lines.SaveToFile(dirname+'log.txt');
end;


procedure TJDLoadService.GetCatalog(startcatalog, catalogname: string; var tid: integer; var id: string);
begin

    with FormMain do
    begin

      qrDict.Close;
      qrDict.SQL.Clear;

      if catalogname<>'' then
      begin
        qrDict.SQL.Add('select tid, id from ParusNetServerSystemCatalog where DisplayName = :catalogname');
        qrDict.SQL.Add('and HierarchyLink_id in (');
        qrDict.SQL.Add('select c.id from ParusNetServerSystemCatalog c, ParusNetServerCatalogForType ct, realtypes rt');
        qrDict.SQL.Add('where c.DisplayName = :startcatalog and ct.Catalog_id = c.id and ct.TypeGuid = rt.id');
        qrDict.SQL.Add(')');
        qrDict.Parameters.ParamByName('catalogname').Value := catalogname;
        qrDict.Parameters.ParamByName('startcatalog').Value := startcatalog;
      end else
      begin
        qrDict.SQL.Add('select c.tid, c.id from ParusNetServerSystemCatalog c, ParusNetServerCatalogForType ct, realtypes rt');
        qrDict.SQL.Add('where c.DisplayName = :startcatalog and ct.Catalog_id = c.id and ct.TypeGuid = rt.id');
        qrDict.Parameters.ParamByName('startcatalog').Value := startcatalog;
      end;

      qrDict.Open;

      tid := qrDict.Fields[0].AsInteger;
      id := qrDict.Fields[1].AsString;

    end;

end;


procedure TJDLoadService.GetRootCatalog(catalogname: string; var tid: integer; var id: string);
begin

    with FormMain do
    begin

      qrDict.Close;
      qrDict.SQL.Clear;

      qrDict.SQL.Add('select c.tid, c.id');
      qrDict.SQL.Add('from ParusNetServerSystemCatalog c, ParusTornadoUnitCatalogsInfo i');
      qrDict.SQL.Add('where c.id = i.Catalog_id and i.BrowseForm = ''ChargeData''');
      qrDict.SQL.Add('and DisplayName = :catalogname');

      qrDict.Parameters.ParamByName('catalogname').Value := catalogname;

      qrDict.Open;

      tid := qrDict.Fields[0].AsInteger;
      id := qrDict.Fields[1].AsString;

    end;

end;


procedure TJDLoadService.GetAccountTypeTid(var tid: integer);
begin

    with FormMain do
    begin
      qrDict.Close;
      qrDict.SQL.Clear;
      qrDict.SQL.Add('select tid from realtypes where Name = ''Parus.Business.Budget.AccountInTreasury''');
      qrDict.Open;
      tid := qrDict.Fields[0].AsInteger;
    end;

end;


procedure TJDLoadService.GetDictData(tn: string; codeName, codeValue: string; var tid: integer; var id: string; var ds: TDataSet; wherefilter:string='');
begin

    with FormMain do
    begin
      qrDict.Close;
      qrDict.SQL.Clear;
      qrDict.SQL.Add('select * from '+tn+' where lower('+codeName+') = :codevalue');
      if wherefilter<>'' then
      qrDict.SQL.Add(wherefilter);
      qrDict.Parameters.ParamByName('codevalue').Value := codeValue;
      qrDict.Open;

      tid := qrDict.FieldByName('tid').AsInteger;
      id := qrDict.FieldByName('id').AsString;

      ds := qrDict;

    end;

end;


function TJDLoadService.QueryExec(ds: TAdoQuery): boolean;
begin
  result := true;
  try
   ds.ExecSQL;
  except
    on E:Exception do
    begin
      AddRowLog(E.Message);
      result := false;
    end;
  end;
end;


end.
