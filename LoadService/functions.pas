unit functions;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.Buttons, QYN, DBCtrlsEh, DBGridEh,
  Data.DB, ComObj, DBSQLLookUp, StrUtils, Math;


function fQYN(mess: string):boolean;
procedure ExportExcel(grid: TDbGridEh; title: string; formats: TStringList = nil);
procedure Delay (dwMilliseconds: Longint);
function VarToInt(v: Variant):integer;
function VarToReal(v: Variant): Real;

implementation

function fQYN(mess: string): boolean;
begin
  FormQYN.Label1.Caption := mess;
  FormQYN.Height := FormQYN.Label1.Height + 100;
  FormQYN.ShowModal;
  result := (FormQYN.ModalResult = mrOk);
end;


procedure ExportExcel(grid: TDbGridEh; title: string; formats: TStringList = nil);
var i,j,index: Integer; ds: TDataSet;
ExcelApp,sheet: Variant;
Range, Cell1, Cell2, ArrayData: Variant;
BeginCol, BeginRow: integer;
RowCount, ColCount : integer;
begin
 Screen.Cursor := crHourGlass;
 try
   Application.ProcessMessages;
   ds := grid.DataSource.DataSet;
   ds.DisableControls;
   ExcelApp := CreateOleObject('Excel.Application');
   ExcelApp.Visible := False;
   ExcelApp.WorkBooks.Add(-4167);
   ExcelApp.WorkBooks[1].WorkSheets[1].name := 'Export';
   sheet:=ExcelApp.WorkBooks[1].WorkSheets['Export'];

   sheet.cells[1,1] := title;
   sheet.cells[1,1].Font.Bold:=1;
   sheet.cells[1,1].Font.Size:=14;

   for i := 1 to grid.Columns.Count do
   begin
     sheet.columns.columns[i].ColumnWidth := IntToStr(trunc(grid.Columns[i-1].Width/8));
     //if formats<>nil then
     sheet.columns.columns[i].NumberFormatLocal := '##'; //formats[i-1];
     sheet.columns.columns[i].WrapText:= true;
     sheet.cells[2,i] := grid.Columns[i-1].Title.Caption;
     sheet.cells[2,i].Interior.Color:=$00DDFFFF;
     sheet.cells[2,i].WrapText:= true;
     sheet.cells[2,i].Borders.LineStyle:=1;
     sheet.cells[2,i].Borders.Weight:=2;
     sheet.cells[2,i].VerticalAlignment := -4108;
   end;

   sheet.cells[1,1].WrapText:= false;

   index := 3;
   ds.First;

   BeginRow := 3;
   BeginCol := 1;

   RowCount := ds.RecordCount;
   ColCount := grid.Columns.Count;

  // ������� ���������� ������, ������� �������� ��������� �������
  ArrayData := VarArrayCreate([1, RowCount, 1, ColCount], varVariant);

   // ��������� ������
   for i:=1 to  ds.RecordCount do
   begin
      for j:=1 to grid.Columns.Count do
      begin
        {sheet.cells[index,j].WrapText:= true;
        sheet.cells[index,j].Borders.LineStyle:=1;
        sheet.cells[index,j].Borders.Weight:=2;
        sheet.cells[index,j].VerticalAlignment := -4108;
        sheet.cells[index,j]:=ds.DataSet.FieldByName(grid.Columns[j-1].FieldName).AsString;}
        try
          if grid.Columns[j-1].FieldName<>'' then ArrayData[i, j] := ds.FieldByName(grid.Columns[j-1].FieldName).AsString;
        except
         //
        end;
        // ������ � ������ ����

        {Cell1 := sheet.Cells[i, 1];
        Cell2 := sheet.Cells[i, ColCount];
        Range := sheet.Range[Cell1, Cell2];
        Range.Interior.Color := clYellow;}

      end;
      inc(index);
      ds.Next;
   end;

  // ����� ������� ������ �������, � ������� ����� �������� ������
    Cell1 := sheet.Cells[BeginRow, BeginCol];

  // ������ ������ ������ �������, � ������� ����� �������� ������
    Cell2 := sheet.Cells[BeginRow  + RowCount - 1, BeginCol + ColCount - 1];

  // �������, � ������� ����� �������� ������
    Range := sheet.Range[Cell1, Cell2];

  // � ��� � ��� ����� ������
    // ������� ������� ����������� ����������
    Range.Value := ArrayData;
    Range.Borders.LineStyle:=1;
    Range.Borders.Weight:=2;
    Range.VerticalAlignment := -4108;

    ExcelApp.Visible := true;
    ds.EnableControls;

 except
    on E : Exception do begin
      ds.EnableControls;
      ShowMessage('������: '+E.Message);
    end;
 end;
 Screen.Cursor := crDefault;
end;


procedure Delay (dwMilliseconds: Longint);
var  iStart, iStop: DWORD;
begin
  iStart := GetTickCount;
  repeat    iStop := GetTickCount;
  Sleep(1); // ����������� ��������� �������� ����������
  Application.ProcessMessages;
  until (iStop - iStart) >= dwMilliseconds;
end;


function VarToInt(v: Variant):integer;
begin
  if v = null then
  begin
    result := 0;
    exit;
  end;
  if v <= 0 then
  begin
    result := 0;
    exit;
  end;

  result := v;

end;

function VarToReal(v: Variant): Real;
begin
  if v = null then
  begin
    result := 0;
    exit;
  end;
  if VarToStr(v)='' then
  begin
    result := 0;
    exit;
  end;
  result := Real(v);
end;



end.
