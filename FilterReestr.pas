unit FilterReestr;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.StdCtrls, Vcl.ExtCtrls,
  Vcl.Mask, DBCtrlsEh, DateUtils;

type
  TFormFilterReestr = class(TFormEdit)
    cbSelectPeriod: TDBComboBoxEh;
    GroupBox1: TGroupBox;
    dtBegin: TDBDateTimeEditEh;
    dtEnd: TDBDateTimeEditEh;
    procedure cbSelectPeriodChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure dtBeginChange(Sender: TObject);
  private
    { Private declarations }
  public
    strBegin: string;
    strEnd: string;
  end;

var
  FormFilterReestr: TFormFilterReestr;

implementation

{$R *.dfm}

procedure TFormFilterReestr.cbSelectPeriodChange(Sender: TObject);
var dDate, dDate2: TDateTime;
begin
  dDate := now();
  dtBegin.Enabled := false;
  dtEnd.Enabled := false;
  case self.cbSelectPeriod.Value of
    0:
    begin
      dtBegin.Value := EncodeDate(YearOf(dDate),MonthOf(dDate),1);
      dtEnd.Value := EncodeDate(YearOf(dDate),MonthOf(dDate),DaysInMonth(dDate));
    end;
    1:
    begin
      dDate := IncMonth(now(),-1);
      dDate2 := now();
      dtBegin.Value := EncodeDate(YearOf(dDate),MonthOf(dDate),1);
      dtEnd.Value := EncodeDate(YearOf(dDate2),MonthOf(dDate2),DaysInMonth(dDate2));
    end;
    2:
    begin
      dDate := IncMonth(now(),-6);
      dDate2 := now();
      dtBegin.Value := EncodeDate(YearOf(dDate),MonthOf(dDate),1);
      dtEnd.Value := EncodeDate(YearOf(dDate2),MonthOf(dDate2),DaysInMonth(dDate2));
    end;
    3:
    begin
      dDate := IncMonth(now(),-12);
      dDate2 := now();
      dtBegin.Value := EncodeDate(YearOf(dDate),MonthOf(dDate),1);
      dtEnd.Value := EncodeDate(YearOf(dDate2),MonthOf(dDate2),DaysInMonth(dDate2));
    end;
    4:
    begin
      dtBegin.Value := EncodeDate(YearOf(dDate),MonthOf(dDate),1);
      dtEnd.Value := EncodeDate(YearOf(dDate),MonthOf(dDate),DaysInMonth(dDate));
      dtBegin.Enabled := true;
      dtEnd.Enabled := true;
    end;
  end;
end;

procedure TFormFilterReestr.dtBeginChange(Sender: TObject);
begin
  if (dtBegin.Value<>null) and (dtEnd.Value<>null) then
  begin
    DateTimeToString(strBegin, 'dd.mm.yyyy', dtBegin.Value);
    DateTimeToString(strEnd, 'dd.mm.yyyy', dtEnd.Value);
  end;
end;

procedure TFormFilterReestr.FormCreate(Sender: TObject);
begin
  inherited;
  self.cbSelectPeriod.ItemIndex := 0;
  self.cbSelectPeriod.ItemIndex := 1;
end;

end.
