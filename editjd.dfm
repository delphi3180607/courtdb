inherited FormEditJD: TFormEditJD
  Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1085#1080#1077' '#1089#1091#1076#1077#1073#1085#1086#1075#1086' '#1088#1077#1096#1077#1085#1080#1103
  ClientHeight = 403
  ClientWidth = 907
  OnActivate = FormActivate
  ExplicitWidth = 921
  ExplicitHeight = 442
  TextHeight = 16
  object sbPersons: TSpeedButton [0]
    Left = 875
    Top = 81
    Width = 23
    Height = 28
    Caption = '...'
    OnClick = sbPersonsClick
  end
  object sbServices: TSpeedButton [1]
    Left = 874
    Top = 25
    Width = 23
    Height = 28
    Caption = '...'
    OnClick = sbServicesClick
  end
  inherited plBottom: TPanel
    Top = 362
    Width = 907
    TabOrder = 11
    ExplicitTop = 362
    ExplicitWidth = 905
    inherited btnCancel: TButton
      Left = 791
      ExplicitLeft = 789
    end
    inherited btnOk: TButton
      Left = 672
      OnClick = btnOkClick
      ExplicitLeft = 670
    end
  end
  object edDocNum: TDBEditEh [3]
    Left = 8
    Top = 26
    Width = 121
    Height = 24
    ControlLabel.Width = 42
    ControlLabel.Height = 16
    ControlLabel.Caption = #1053#1086#1084#1077#1088
    ControlLabel.Visible = True
    DataField = 'doc_num'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Enabled = False
    TabOrder = 0
    Visible = True
  end
  object dtDocDate: TDBDateTimeEditEh [4]
    Left = 139
    Top = 26
    Width = 121
    Height = 24
    ControlLabel.Width = 33
    ControlLabel.Height = 16
    ControlLabel.Caption = #1044#1072#1090#1072
    ControlLabel.Visible = True
    DataField = 'doc_date'
    DataSource = dsLocal
    DynProps = <>
    Enabled = False
    EditButtons = <>
    Kind = dtkDateEh
    TabOrder = 1
    Visible = True
  end
  object laService: TDBSQLLookUp [5]
    Left = 280
    Top = 26
    Width = 589
    Height = 26
    Color = 14875388
    ControlLabel.Width = 47
    ControlLabel.Height = 16
    ControlLabel.Caption = #1059#1089#1083#1091#1075#1072
    ControlLabel.Visible = True
    DataField = 'service_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Calibri'
    Font.Style = []
    ParentFont = False
    StyleElements = [seFont, seBorder]
    TabOrder = 2
    Visible = True
    SqlSet = dm.ssServices
    KeyValue = Null
    RowCount = 0
    TextEditing = False
    OnKeyValueChange = laServiceKeyValueChange
  end
  object laPerson: TDBSQLLookUp [6]
    Left = 280
    Top = 82
    Width = 589
    Height = 26
    Color = 14875388
    ControlLabel.Width = 64
    ControlLabel.Height = 16
    ControlLabel.Caption = #1054#1090#1074#1077#1090#1095#1080#1082
    ControlLabel.Visible = True
    DataField = 'person_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Calibri'
    Font.Style = []
    ParentFont = False
    StyleElements = [seFont, seBorder]
    TabOrder = 3
    Visible = True
    SqlSet = dm.ssPersons
    KeyValue = Null
    RowCount = 0
    TextEditing = False
  end
  object meNote: TDBMemoEh [7]
    Left = 8
    Top = 189
    Width = 889
    Height = 92
    ControlLabel.Width = 104
    ControlLabel.Height = 16
    ControlLabel.Caption = #1055#1086#1089#1090#1072#1085#1086#1074#1083#1077#1085#1080#1077
    ControlLabel.Visible = True
    AutoSize = False
    Color = 14875388
    DataField = 'note'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    StyleElements = [seFont, seBorder]
    TabOrder = 6
    Visible = True
    WantReturns = True
  end
  object edUIN: TDBEditEh [8]
    Left = 8
    Top = 319
    Width = 190
    Height = 26
    ControlLabel.Width = 27
    ControlLabel.Height = 16
    ControlLabel.Caption = #1059#1048#1053
    ControlLabel.Visible = True
    DataField = 'uin'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Calibri'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    TabOrder = 7
    Visible = True
  end
  object laAccount: TDBSQLLookUp [9]
    Left = 205
    Top = 319
    Width = 224
    Height = 26
    Color = 14875388
    ControlLabel.Width = 95
    ControlLabel.Height = 16
    ControlLabel.Caption = #1051#1080#1094#1077#1074#1086#1081' '#1089#1095#1077#1090
    ControlLabel.Visible = True
    DataField = 'account_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Calibri'
    Font.Style = []
    ParentFont = False
    StyleElements = [seFont, seBorder]
    TabOrder = 8
    Visible = True
    SqlSet = dm.ssAccounts
    KeyValue = Null
    RowCount = 0
    TextEditing = False
  end
  object neSumma: TDBNumberEditEh [10]
    Left = 280
    Top = 138
    Width = 201
    Height = 26
    ControlLabel.Width = 43
    ControlLabel.Height = 16
    ControlLabel.Caption = #1057#1091#1084#1084#1072
    ControlLabel.Font.Charset = DEFAULT_CHARSET
    ControlLabel.Font.Color = clWindowText
    ControlLabel.Font.Height = -13
    ControlLabel.Font.Name = 'Verdana'
    ControlLabel.Font.Style = []
    ControlLabel.ParentFont = False
    ControlLabel.Visible = True
    Color = 14875388
    DataField = 'summa'
    DataSource = dsLocal
    DisplayFormat = '### ### ##0.00'
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Calibri'
    Font.Style = []
    ParentFont = False
    StyleElements = [seFont, seBorder]
    TabOrder = 4
    Visible = True
  end
  object edSubDiv: TDBSQLLookUp [11]
    Left = 436
    Top = 319
    Width = 297
    Height = 26
    Color = 14875388
    ControlLabel.Width = 104
    ControlLabel.Height = 16
    ControlLabel.Caption = #1055#1086#1076#1088#1072#1079#1076#1077#1083#1077#1085#1080#1077
    ControlLabel.Visible = True
    DataField = 'subdiv_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Calibri'
    Font.Style = []
    ParentFont = False
    StyleElements = [seFont, seBorder]
    TabOrder = 9
    Visible = True
    SqlSet = dm.ssSubdivs
    KeyValue = Null
    RowCount = 0
    TextEditing = False
  end
  object edOKTMO: TDBEditEh [12]
    Left = 744
    Top = 319
    Width = 153
    Height = 24
    ControlLabel.Width = 48
    ControlLabel.Height = 16
    ControlLabel.Caption = #1054#1050#1058#1052#1054
    ControlLabel.Visible = True
    DataField = 'oktmo'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 10
    Visible = True
  end
  object dtEif: TDBDateTimeEditEh [13]
    Left = 517
    Top = 138
    Width = 244
    Height = 26
    ControlLabel.Width = 238
    ControlLabel.Height = 16
    ControlLabel.Caption = #1044#1072#1090#1072' '#1074#1089#1090#1091#1087#1083#1077#1085#1080#1103' '#1074' '#1079#1072#1082#1086#1085#1085#1091#1102' '#1089#1080#1083#1091
    ControlLabel.Visible = True
    DataField = 'date_eif'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Verdana'
    Font.Style = []
    Kind = dtkDateEh
    ParentFont = False
    TabOrder = 5
    Visible = True
  end
  inherited dsLocal: TDataSource
    Left = 152
    Top = 168
  end
  inherited qrAux: TFDQuery
    Left = 104
    Top = 168
  end
end
