unit EditPerson;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, editcodename, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  Vcl.StdCtrls, Vcl.ExtCtrls, DBCtrlsEh, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, Vcl.Mask, Vcl.ComCtrls;

type
  TFormEditPerson = class(TFormEditCodeName)
    edINN: TDBEditEh;
    pcVars: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    edAddress: TDBEditEh;
    edDocSeria: TDBEditEh;
    edDocNumber: TDBEditEh;
    dtDocWhen: TDBDateTimeEditEh;
    edDocWho: TDBEditEh;
    dtBirthDate: TDBDateTimeEditEh;
    edSNILS: TDBEditEh;
    edJTCode: TDBEditEh;
    cbType: TDBComboBoxEh;
    cbSex: TDBComboBoxEh;
    edSurName: TDBEditEh;
    edFirstName: TDBEditEh;
    edSecondName: TDBEditEh;
    edContacts: TDBEditEh;
    cbDocType: TDBComboBoxEh;
    procedure rgPersonKindChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure edSurNameChange(Sender: TObject);
    procedure edNameChange(Sender: TObject);
    procedure edSNILSKeyPress(Sender: TObject; var Key: Char);
    procedure edINNKeyPress(Sender: TObject; var Key: Char);
    procedure edDocNumberKeyPress(Sender: TObject; var Key: Char);
    procedure edAddressEditButtons0Click(Sender: TObject; var Handled: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditPerson: TFormEditPerson;

implementation

{$R *.dfm}

uses FillAddress;

procedure TFormEditPerson.btnOkClick(Sender: TObject);
begin
  if (edCode.Text = '')
  or (edINN.Text = '')
  or (edName.Text  = '')
  then begin
    ShowMessage('�� ��������� ������������ ���� - ������������, �������� ������������, ���!');
    self.ModalResult := mrNone;
  end;

  if cbType.Value = 0 then
  begin
    if (edSurname.Text = '')
    or (edFirstName.Text = '')
    or (edSecondName.Text  = '')
    then begin
      ShowMessage('�� ��������� ������������ ���� - �������, ���, ��������!');
      self.ModalResult := mrNone;
    end;

    if (edSNILS.Text = '')
    then begin
      ShowMessage('�� ��������� ������������ ���� - �����!');
      self.ModalResult := mrNone;
    end;

  end;

end;

procedure TFormEditPerson.edAddressEditButtons0Click(Sender: TObject;
  var Handled: Boolean);
begin

  FormFillAddress.dsLocal.DataSet := self.dsLocal.DataSet;

  if pos('�����:', edAddress.Text)>0 then
    FormFillAddress.pcAddress.ActivePageIndex := 1
  else
    FormFillAddress.pcAddress.ActivePageIndex := 0;

  FormFillAddress.ShowModal;

  with FormFillAddress do
  begin

    if FormFillAddress.pcAddress.ActivePageIndex = 0 then
      edAddress.Text := laRegion.Text+', '+laArea.Text+', '+laTown.Text+', '+laStreet.Text+', '+edHouse.Text+', '+edBuilding.Text+', '+edFlat.Text
    else
      edAddress.Text := '�����: '+armRegion.Text+', '+armArea.Text+', '+armCity.Text+', '+armStreet.Text+', '+edHouse.Text+', '+edBuilding.Text+', '+edFlat.Text

  end;

end;

procedure TFormEditPerson.edDocNumberKeyPress(Sender: TObject; var Key: Char);
begin
  if (not (Key in ['0'..'9'])) then Key := #0;
end;

procedure TFormEditPerson.edINNKeyPress(Sender: TObject; var Key: Char);
begin
  if (not (Key in ['0'..'9'])) then Key := #0;
end;

procedure TFormEditPerson.edNameChange(Sender: TObject);
begin
  if self.Visible then
  edCode.Text := trim(edSurName.Text+' '+copy(edFirstName.Text,1,1)+'.'+copy(edSecondName.Text,1,1)+'.');
end;

procedure TFormEditPerson.edSNILSKeyPress(Sender: TObject; var Key: Char);
begin
  if (not (Key in ['0'..'9'])) and (Key<>'-') then Key := #0;
end;

procedure TFormEditPerson.edSurNameChange(Sender: TObject);
begin
  if self.Visible then
  edName.Text := trim(edSurName.Text+' '+edFirstName.Text+' '+edSecondName.Text);
end;

procedure TFormEditPerson.FormShow(Sender: TObject);
begin

  inherited;

  if self.current_mode = 'update' then
    cbType.Enabled := false
  else
    cbType.Enabled := true;

end;

procedure TFormEditPerson.rgPersonKindChange(Sender: TObject);
begin
  pcVars.ActivePageIndex := cbType.ItemIndex;
end;

end.
