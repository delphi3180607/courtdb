inherited FormSubDivs: TFormSubDivs
  Caption = #1055#1086#1076#1088#1072#1079#1076#1077#1083#1077#1085#1080#1103
  ClientWidth = 920
  ExplicitWidth = 934
  TextHeight = 16
  inherited plMain: TPanel
    Width = 920
    inherited dgMain: TDBGridEh
      Columns = <
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'code'
          Footers = <>
          Title.Caption = #1050#1086#1076
          Width = 165
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'name'
          Footers = <>
          Title.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
          Width = 387
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'oktmo'
          Footers = <>
          Title.Caption = #1054#1050#1058#1052#1054
          Width = 206
        end>
    end
  end
  inherited FDUpdateSet: TFDUpdateSQL
    InsertSQL.Strings = (
      'INSERT INTO subdivs'
      '(code, name, oktmo)'
      'VALUES (:code, :name, :oktmo)'
      'RETURNING id;')
    ModifySQL.Strings = (
      'UPDATE subdivs'
      'SET code = :code, name = :name, oktmo = :oktmo'
      'WHERE id = :id'
      'RETURNING id;')
    DeleteSQL.Strings = (
      'DELETE FROM subdivs WHERE id = :id')
    FetchRowSQL.Strings = (
      'select * from subdivs WHERE id = :id')
  end
  inherited FDDataQr: TFDQuery
    SQL.Strings = (
      'select * from subdivs order by code')
  end
end
