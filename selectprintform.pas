unit selectprintform;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Vcl.ComCtrls, Data.DB, Vcl.StdCtrls, Vcl.ExtCtrls,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TFormSelectPrintForm = class(TFormEdit)
    FileList: TListView;
  private
    { Private declarations }
  public
    procedure Init;
  end;

var
  FormSelectPrintForm: TFormSelectPrintForm;

implementation

{$R *.dfm}

procedure TFormSelectPrintForm.Init;
var SR: TSearchRec; li:TListItem;
begin

  FileList.Items.BeginUpdate;
  try
      FileList.Items.Clear;

      FindFirst(ExtractFilePath(Application.ExeName) +'/templates/*.xls*', faAnyFile, SR);
      try
          repeat
              li := FileList.Items.Add;
              li.Caption := SR.Name;

              if ((SR.Attr and faDirectory) <> 0)  then li.ImageIndex := 1
              else li.ImageIndex := 0;

          until (FindNext(SR) <> 0);
      finally
          FindClose(SR);
      end;
  finally
      FileList.Items.EndUpdate;
  end;

end;

end.
