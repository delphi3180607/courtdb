unit FillAddress;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  Vcl.StdCtrls, Vcl.Mask, DBCtrlsEh, DBSQLLookUp, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, Vcl.ExtCtrls, Vcl.ComCtrls;

type
  TFormFillAddress = class(TFormEdit)
    pcAddress: TPageControl;
    �����: TTabSheet;
    TabSheet2: TTabSheet;
    laRegion: TDBSQLLookUp;
    laTown: TDBSQLLookUp;
    laStreet: TDBSQLLookUp;
    edHouse: TDBEditEh;
    edBuilding: TDBEditEh;
    edFlat: TDBEditEh;
    edPIndex: TDBEditEh;
    laArea: TDBSQLLookUp;
    ssRegion: TDBXLookUpSqlSet;
    ssArea: TDBXLookUpSqlSet;
    ssTown: TDBXLookUpSqlSet;
    ssStreet: TDBXLookUpSqlSet;
    armRegion: TDBEditEh;
    armArea: TDBEditEh;
    armCity: TDBEditEh;
    armStreet: TDBEditEh;
    armHouse: TDBEditEh;
    armBuilding: TDBEditEh;
    armFlat: TDBEditEh;
    armPIndex: TDBEditEh;
    procedure laRegionKeyValueChange(Sender: TObject);
    procedure laAreaKeyValueChange(Sender: TObject);
    procedure laTownKeyValueChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    onshow: boolean;
  public
    { Public declarations }
  end;

var
  FormFillAddress: TFormFillAddress;

implementation

{$R *.dfm}

procedure TFormFillAddress.FormShow(Sender: TObject);
begin
  onshow := true;
  laAreaKeyValueChange(self);
  laRegionKeyValueChange(self);
  laTownKeyValueChange(self);
  laTown.KeyValue := dsLocal.DataSet.FieldByName('town_id').Value;
  laStreet.KeyValue := dsLocal.DataSet.FieldByName('street_id').Value;
  onshow := false;
end;

procedure TFormFillAddress.laAreaKeyValueChange(Sender: TObject);
begin
  if laArea.KeyValue = null then
  begin
    ssTown.Parameters.SetPairValue('areaid', null);
    ssTown.Parameters.SetPairValue('areaid1', null);
    if not onshow then laTown.KeyValue := null;
    laTown.Enabled := true;
  end else
  begin
    ssTown.Parameters.SetPairValue('areaid', laArea.KeyValue);
    ssTown.Parameters.SetPairValue('areaid1', laArea.KeyValue);
    if not onshow then laTown.KeyValue := null;
    laTown.Enabled := true;
  end;
end;

procedure TFormFillAddress.laRegionKeyValueChange(Sender: TObject);
begin
  if laRegion.KeyValue = null then
  begin
    laArea.KeyValue := null;
    laArea.Enabled := false;
  end else
  begin
    ssArea.Parameters.SetPairValue('regionid', laRegion.KeyValue);
    ssTown.Parameters.SetPairValue('regionid', laRegion.KeyValue);
    laArea.Enabled := true;
  end;
end;

procedure TFormFillAddress.laTownKeyValueChange(Sender: TObject);
begin
  if laTown.KeyValue = null then
  begin
    laStreet.KeyValue := null;
    laStreet.Enabled := false;
  end else
  begin
    ssStreet.Parameters.SetPairValue('townid', laTown.KeyValue);
    ssStreet.Parameters.SetPairValue('settlid', laTown.KeyValue);
    if not onshow then laStreet.KeyValue := null;
    laStreet.Enabled := true;
  end;
end;

end.
