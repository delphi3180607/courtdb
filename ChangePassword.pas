unit ChangePassword;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Vcl.StdCtrls, Vcl.Mask, DBCtrlsEh,
  Data.DB, Data.Win.ADODB, Vcl.ExtCtrls, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client;

type
  TFormChangePassword = class(TFormEdit)
    edPasswordNew: TDBEditEh;
    edPasswordNew1: TDBEditEh;
    procedure btnOkClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormChangePassword: TFormChangePassword;

implementation

{$R *.dfm}

procedure TFormChangePassword.btnOkClick(Sender: TObject);
begin
  if self.edPasswordNew.Text <> self.edPasswordNew1.Text  then
  begin
    ShowMessage('������ �� ���������.');
    self.ModalResult := mrNone;
  end;
end;

end.
