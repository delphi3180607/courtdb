unit persons;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, FireDAC.Stan.Intf, FireDAC.Stan.Param,
  FireDAC.Phys.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Vcl.Menus, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.Buttons, PngSpeedButton,
  EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh, Vcl.ExtCtrls, MemTableDataEh,
  MemTableEh, Vcl.StdCtrls, EXLReportExcelTLB, EXLReportBand, EXLReport;

type
  TFormPersons = class(TFormGrid)
    procedure FDDataQrAfterInsert(DataSet: TDataSet);
    procedure FDDataQrAfterPost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    procedure Init; override;
  end;

var
  FormPersons: TFormPersons;

implementation

{$R *.dfm}

uses editcodename, EditPerson, gridmain;

procedure TFormPersons.FDDataQrAfterInsert(DataSet: TDataSet);
begin
  FDDataQr.FieldByName('type').Value := 0;
end;

procedure TFormPersons.FDDataQrAfterPost(DataSet: TDataSet);
begin
  inherited;
  if Assigned(FormGridMain) then
  begin
    if not (FormGridMain.FDDataQr.State in [dsInsert, dsEdit]) then
    begin
      FormGridMain.FDDataQr.Close;
      FormGridMain.FDDataQr.Open;
    end;
  end;
end;

procedure TFormPersons.Init;
begin
  inherited;
  self.FormEditLocal := FormEditPerson;
end;

end.
