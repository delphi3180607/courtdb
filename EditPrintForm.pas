unit EditPrintForm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Data.DB, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Mask, DBCtrlsEh,
  SynEdit, Vcl.DBCtrls, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TFormEditPrintForm = class(TFormEdit)
    edTitle: TDBEditEh;
    edTemplate: TDBEditEh;
    Label1: TLabel;
    meSQL: TDBMemo;
    edGroupField: TDBEditEh;
    cbUnit: TDBComboBoxEh;
    procedure edTemplateEditButtons0Click(Sender: TObject;
      var Handled: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditPrintForm: TFormEditPrintForm;

implementation

{$R *.dfm}

uses selectPrintForm;

procedure TFormEditPrintForm.edTemplateEditButtons0Click(Sender: TObject;
  var Handled: Boolean);
begin
  FormSelectPrintForm.Init;
  FormSelectPrintForm.ShowModal;

  if FormSelectPrintForm.ModalResult = mrOk then
  begin
     edTemplate.Text := FormSelectPrintForm.FileList.Selected.Caption;
  end;

end;

end.
