unit wait;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Imaging.pngimage,
  Vcl.ExtCtrls;

type
  TFormWait = class(TForm)
    Label1: TLabel;
    Image1: TImage;
    Timer1: TTimer;
    procedure FormShow(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    { Private declarations }
  public
    canClose: boolean;
  end;

var
  FormWait: TFormWait;

implementation

{$R *.dfm}

procedure TFormWait.FormShow(Sender: TObject);
begin
  canClose := false;
end;

procedure TFormWait.Timer1Timer(Sender: TObject);
begin
  canClose := true;
end;

end.
