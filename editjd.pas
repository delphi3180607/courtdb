unit editjd;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Data.DB, Vcl.StdCtrls, Vcl.ExtCtrls, DBCtrlsEh, Vcl.Mask,
  DBSQLLookUp, Vcl.Buttons,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  functions, persons, services;

type
  TFormEditJD = class(TFormEdit)
    edDocNum: TDBEditEh;
    dtDocDate: TDBDateTimeEditEh;
    laService: TDBSQLLookUp;
    laPerson: TDBSQLLookUp;
    sbPersons: TSpeedButton;
    meNote: TDBMemoEh;
    edUIN: TDBEditEh;
    laAccount: TDBSQLLookUp;
    neSumma: TDBNumberEditEh;
    sbServices: TSpeedButton;
    edSubDiv: TDBSQLLookUp;
    edOKTMO: TDBEditEh;
    dtEif: TDBDateTimeEditEh;
    procedure sbPersonsClick(Sender: TObject);
    procedure sbServicesClick(Sender: TObject);
    procedure laServiceKeyValueChange(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditJD: TFormEditJD;

implementation

{$R *.dfm}

uses dmu;

procedure TFormEditJD.btnOkClick(Sender: TObject);
begin
  if (laService.Text = '')
  or (laPerson.Text = '')
  or (laAccount.Text  = '')
  or (VarToReal(neSumma.Value) = 0)
  then begin
    ShowMessage('�� ��������� ������������ ����!');
    self.ModalResult := mrNone;
  end;
end;

procedure TFormEditJD.FormActivate(Sender: TObject);
begin
  inherited;
  btnOk.Enabled := true;
  if not dsLocal.DataSet.CanModify then
  begin
    btnOk.Enabled := false;
  end;
end;

procedure TFormEditJD.laServiceKeyValueChange(Sender: TObject);
begin
//  meNote.Value := dm.ssServices.ListDataSet.FieldByName('note').AsString;
end;

procedure TFormEditJD.sbPersonsClick(Sender: TObject);
begin
  if edDocNum.Text='' then
  SFDE(TFormPersons, self.laPerson);
end;

procedure TFormEditJD.sbServicesClick(Sender: TObject);
begin
  if edDocNum.Text='' then
  SFDE(TFormServices, self.laService);
end;

end.
