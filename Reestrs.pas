unit Reestrs;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, FireDAC.Stan.Intf, FireDAC.Stan.Param,
  FireDAC.Phys.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, MemTableDataEh, Data.DB,
  MemTableEh, Vcl.Menus, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  Vcl.StdCtrls, Vcl.Buttons, PngSpeedButton, EhLibVCL, GridsEh, DBAxisGridsEh,
  DBGridEh, Vcl.ExtCtrls, Vcl.Mask, DBCtrlsEh, Functions, EXLReportExcelTLB,
  EXLReportBand, EXLReport;

type
  TFormReestrs = class(TFormGrid)
    Splitter1: TSplitter;
    dgDetail: TDBGridEh;
    FDDetailQuery: TFDQuery;
    dsDetail: TDataSource;
    plFilter: TPanel;
    btFilter: TPngSpeedButton;
    FDGetLastNumber: TFDQuery;
    sbExport: TPngSpeedButton;
    pmDetail: TPopupMenu;
    MenuItem4: TMenuItem;
    MenuItem5: TMenuItem;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    procedure btFilterClick(Sender: TObject);
    procedure FDDataQrBeforeOpen(DataSet: TDataSet);
    procedure FDDataQrAfterScroll(DataSet: TDataSet);
    procedure FDDetailQueryBeforeOpen(DataSet: TDataSet);
    procedure MenuItem5Click(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure sbPrintClick(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure sbExportClick(Sender: TObject);
  private
    dtBegin: TDateTime;
    dtEnd: TDateTime;
  public
    procedure Init; override;
    procedure AfterAppendRecord; override;
  end;

var
  FormReestrs: TFormReestrs;

implementation

{$R *.dfm}

uses EditReestr, FilterReestr, dmu, selecttemplate, exportJD;

{ TFormReestrs }

procedure TFormReestrs.AfterAppendRecord;
begin
  inherited;
  FDDataQr.FieldByName('reestr_date').Value := now();
  qrAux.Close;
  qrAux.Params.ParamByName('docdate').Value := now();
  qrAux.Open;
  FDDataQr.FieldByName('reestr_num').Value := qrAux.Fields[0].AsString;
end;

procedure TFormReestrs.btFilterClick(Sender: TObject);
begin
  FormFilterReestr.ShowModal;
  if FormFilterReestr.ModalResult = mrOk then
  begin
    plFilter.Caption := '������ ��������� �������� � '+FormFilterReestr.strBegin+' �� '+FormFilterReestr.strEnd;
    FDDataQr.Close;
    FDDataQr.Open;
  end;
end;

procedure TFormReestrs.FDDataQrAfterScroll(DataSet: TDataSet);
begin
  FDDetailQuery.Close;
  FDDetailQuery.Open;
end;

procedure TFormReestrs.FDDataQrBeforeOpen(DataSet: TDataSet);
begin
  FDDataQr.Params.ParamByName('date_begin').Value := FormFilterReestr.dtBegin.Value;
  FDDataQr.Params.ParamByName('date_end').Value := FormFilterReestr.dtEnd.Value;
  plFilter.Caption := '������ ��������� �������� � '+FormFilterReestr.strBegin+' �� '+FormFilterReestr.strEnd;
end;

procedure TFormReestrs.FDDetailQueryBeforeOpen(DataSet: TDataSet);
begin
  FDDetailQuery.Params.ParamByName('reestr_id').Value := FDDataQr.FieldByName('id').AsInteger;
end;

procedure TFormReestrs.Init;
begin
  inherited;
  self.FormEditLocal := FormEditReestr;
  FDDataQr.Open;
end;

procedure TFormReestrs.MenuItem5Click(Sender: TObject);
begin
  FDDetailQuery.Close;
  FDDetailQuery.Open;
end;

procedure TFormReestrs.N1Click(Sender: TObject);
begin
  if fQYN('��������� ��������� ������ �� �������?') then
  begin
    GetIds(dgDetail, FDDetailQuery);
    dm.qrReestrOp.Close;
    dm.qrReestrOp.Params.ParamByName('sIds').Value := Ids;
    dm.qrReestrOp.Params.ParamByName('nReestrId').Value := 0;
    dm.qrReestrOp.Params.ParamByName('nCheckSign').Value := 0;
    try
     dm.qrReestrOp.Open;
     FDDetailQuery.Close;
     FDDetailQuery.Open;
     ShowMessage('��������� ��������� �� �������.');
    except
    on E: Exception do
      ShowMessage(E.Message);
    end;
  end;
  dgDetail.Selection.Clear;
end;

procedure TFormReestrs.N3Click(Sender: TObject);
begin
  if fQYN('���������� ������?') then
  begin
    FDDataQr.Edit;
    FDDataQr.FieldByName('date_confirm').Value := now();
    FDDataQr.Post;
  end;
end;

procedure TFormReestrs.N4Click(Sender: TObject);
begin
  if fQYN('����� ��������� � �������?') then
  begin
    FDDataQr.Edit;
    FDDataQr.FieldByName('date_confirm').Value := null;
    FDDataQr.Post;
  end;
end;

procedure TFormReestrs.sbExportClick(Sender: TObject);
begin
  FormExportJD.meLog.Lines.Clear;
  FormExportJD.Grid := dgDetail;
  FormExportJD.Query := FDDetailQuery;
  FormExportJD.prefix := FDDataQr.FieldByName('reestr_num').AsString;
  FormExportJD.ShowModal;
  if FormExportJD.Processed then
  begin
    FDDataQr.Edit;
    FDDataQr.FieldByName('date_export').Value := now();
    FDDataQr.Post;
  end;
  FormExportJD.Grid := nil;
  FormExportJD.Query := nil;
  FormExportJD.prefix := '';
end;

procedure TFormReestrs.sbPrintClick(Sender: TObject);
begin
  unit_num1 := 2;
  unit_num2 := 2;
  PrintReport;
end;

end.
