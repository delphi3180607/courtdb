unit base;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Param,
  FireDAC.Phys.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TFormBase = class(TForm)
    FDUpdateSet: TFDUpdateSQL;
    FDDataQr: TFDQuery;
    dsData: TDataSource;
    qrAux: TFDQuery;
  private
    { Private declarations }
  public
    procedure Init; virtual;
  end;

var
  FormBase: TFormBase;

implementation

{$R *.dfm}

procedure TFormBase.Init;
begin
  FDDataQr.Active := true;
end;


end.
