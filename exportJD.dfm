inherited FormExportJD: TFormExportJD
  Caption = #1069#1082#1089#1087#1086#1088#1090' '#1082#1074#1080#1090#1072#1085#1094#1080#1081
  ClientWidth = 533
  OnShow = FormShow
  ExplicitWidth = 547
  TextHeight = 16
  object sbExport: TPngSpeedButton [0]
    Left = 8
    Top = 8
    Width = 97
    Height = 33
    Caption = #1069#1082#1089#1087#1086#1088#1090
    OnClick = sbExportClick
    PngImage.Data = {
      89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
      F8000000017352474200AECE1CE9000000097048597300000EC300000EC301C7
      6FA864000001904944415478DAD595CF2B445114C7EF9821F263C88F21D9B022
      916C959D4236967E9458D9CBC2CEC642FE0024CB89859D1A2BFE04120BD94829
      1216568A199FD39CA9DBF5E6BD99D79B85539FEE79AF3BE77BCF39F7CC8B990A
      5BEC3F08F4C33D7C57424082DF421A162117B540027661198E60DE1589A24409
      0D3E0BC730678B8411A88201988069E8816668805AC8C00C645D81829FF3093E
      0E07D067BD7B813834414D3101592FD51FF1109132ECC3923EAFC3093C6AF074
      508964FD50BFC51190E0E730A68156E1D32AD71EAC988026CBFAAC7EA7B3E950
      4FBEA9D826BDB831255CD398D6522C656D949A5F688085227D2969D0BC0424FD
      3B6D68D22A4B59E6273008D726DFD09D30C18304D6601B7AE1214A810E5DCF4C
      7E98EAE02B2A01A97B9BBE93E77A6834FEC3572C66CE15906B1AB704E4FAC934
      0E9721F06760DD0C7EA02B6C39D4DE4D7E3893B680D8150C413BBC79A41C6492
      652BBC6A16A3EE8FA7E0547DC9A65AFD6C8902927D4A7DF997CD789D6E12B6A0
      5B4F54CEE965EF136C1482FBA51FF64314F92733D07E01B1145D19DE43155300
      00000049454E44AE426082}
  end
  inherited plBottom: TPanel
    Width = 533
    ExplicitWidth = 531
    inherited btnCancel: TButton
      Left = 417
      Caption = #1047#1072#1082#1088#1099#1090#1100
      ExplicitLeft = 415
    end
    inherited btnOk: TButton
      Left = 298
      Visible = False
      ExplicitLeft = 296
    end
  end
  object meLog: TMemo [2]
    AlignWithMargins = True
    Left = 3
    Top = 45
    Width = 527
    Height = 258
    Margins.Top = 45
    Align = alClient
    Color = 15268605
    ReadOnly = True
    TabOrder = 1
    StyleElements = [seFont, seBorder]
    ExplicitWidth = 525
  end
  inherited qrAux: TFDQuery
    SQL.Strings = (
      'select j.*, '
      'p.code as person_name, '
      's.code as service_name, '
      'a.code as account_number,'
      'p.* '
      'from jdmain j'
      'left outer join persons p on (p.id  = j.person_id)'
      'left outer join services s on (s.id  = j.service_id )'
      'left outer join accounts a on (a.id  = j.account_id )'
      'order by j.doc_date;')
  end
  object XMLDoc: TXMLDocument
    Left = 212
    Top = 95
  end
  object sd: TFileOpenDialog
    FavoriteLinks = <>
    FileTypes = <>
    Options = [fdoPickFolders]
    Left = 112
    Top = 144
  end
end
