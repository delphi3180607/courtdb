object FormEdit: TFormEdit
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'FormEdit'
  ClientHeight = 347
  ClientWidth = 537
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Verdana'
  Font.Style = []
  Position = poMainFormCenter
  OnCreate = FormCreate
  TextHeight = 16
  object plBottom: TPanel
    Left = 0
    Top = 306
    Width = 537
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 535
    object btnCancel: TButton
      AlignWithMargins = True
      Left = 421
      Top = 3
      Width = 113
      Height = 35
      Align = alRight
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 0
      ExplicitLeft = 419
    end
    object btnOk: TButton
      AlignWithMargins = True
      Left = 302
      Top = 3
      Width = 113
      Height = 35
      Align = alRight
      Caption = #1047#1072#1087#1080#1089#1072#1090#1100
      Default = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Verdana'
      Font.Style = []
      ModalResult = 1
      ParentFont = False
      TabOrder = 1
      ExplicitLeft = 300
    end
  end
  object dsLocal: TDataSource
    Left = 360
    Top = 40
  end
  object qrAux: TFDQuery
    Connection = dm.FDConnMain
    UpdateOptions.AssignedValues = [uvEInsert, uvRefreshMode, uvRefreshDelete]
    UpdateOptions.KeyFields = 'id'
    SQL.Strings = (
      'select j.*, '
      'p.code as person_name, '
      's.code as service_name, '
      'a.code as account_number '
      'from jdmain j'
      'left outer join persons p on (p.id  = j.person_id)'
      'left outer join services s on (s.id  = j.service_id )'
      'left outer join accounts a on (a.id  = j.account_id )'
      'order by j.doc_date;')
    Left = 357
    Top = 100
  end
end
