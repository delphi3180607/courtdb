unit exportJD;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, FireDAC.Stan.Intf, GridsEh, DBCtrlsEh,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, DBAxisGridsEh, DBGridEh,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.StdCtrls, Vcl.ExtCtrls,
  Vcl.Buttons, PngSpeedButton, Xml.xmldom, Xml.XMLIntf, Xml.XMLDoc;

type
  TFormExportJD = class(TFormEdit)
    meLog: TMemo;
    sbExport: TPngSpeedButton;
    XMLDoc: TXMLDocument;
    sd: TFileOpenDialog;
    procedure sbExportClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    function CreateNodeByField(ds: TDataSet; fn: string): IXMLNode;
    procedure CreateAndAddNodeByField(ds: TDataSet; fn: string; var parent_node: IXMLNode);
    procedure CreateAndAddNodeByValue(fn, value: string; var parent_node: IXMLNode);
  public
    Processed: bool;
    node_root: IXMLNode;
    skipped_nonregistered: integer;
    prefix: string;
    Grid: TDBGridEh;
    Query: TFDQuery;
    procedure MakeRecordNode(ds: TDataSet);
  end;

var
  FormExportJD: TFormExportJD;

implementation

{$R *.dfm}

uses gridmain, main, Settings;

procedure TFormExportJD.sbExportClick(Sender: TObject);
var i: integer; dir, fn: string;
begin

  skipped_nonregistered := 0;

  if sd.Execute() then
  begin
    Screen.Cursor := crHourGlass;

    xmlDoc.Active := false;
    xmlDoc.Active := true;
    XmlDoc.Encoding := 'Windows-1251';
    XmlDoc.Version := '1.0';
    node_root := xmlDoc.CreateNode('������������������������');

    if Grid.Selection.SelectionType in [gstAll, gstNon] then
    begin
      Query.Close;
      Query.Open;
      Query.First;
      while not Query.Eof do
      begin
        MakeRecordNode(Query);
        Query.Next;
      end;

    end else
    if Grid.Selection.SelectionType = (gstRecordBookmarks) then
    begin

      for i := 0 to Grid.Selection.Rows.Count-1 do
      begin
        Query.Bookmark := Grid.Selection.Rows[i];
        MakeRecordNode(Query);
      end;

    end else
    begin
        MakeRecordNode(Query);
    end;

    xmlDoc.ChildNodes.Add(node_root);

    dir := sd.FileName+'\';
    fn := DateToStr(now())+FormSettings.jdnumber+FormSettings.jpnumber;
    fn := StringReplace(fn, '/','',[rfReplaceAll]);
    fn := StringReplace(fn, '.','',[rfReplaceAll]);
    if prefix <> '' then
      fn := prefix+'_'+fn;

    try
      xmlDoc.SaveToFile(dir+fn+'.xml');
      meLog.Lines.Add('��������� ��������� � ���� '+dir+fn+'.xml');

      if skipped_nonregistered>0 then
      meLog.Lines.Add('��������� '+IntToStr(skipped_nonregistered)+' �������������������� ����������.');

      meLog.Lines.Add('������� ��������.');
    except
    on e: Exception do begin
      Screen.Cursor := crDefault;
      meLog.Lines.Add('��� �������� ����� �������� �������� ������:');
      meLog.Lines.Add(E.Message);
    end;
    end;

    Processed := true;
    Screen.Cursor := crDefault;

  end;

end;


procedure TFormExportJD.MakeRecordNode(ds: TDataSet);
var node: IXMLNode; node_record: IXMLNode; soktmo: string;
begin

  soktmo := trim(ds.FieldByName('oktmo').AsString);

  if soktmo = ''  then
  soktmo := FormSettings.FDDataQr.FieldByName('oktmo').AsString;

  if ds.FieldByName('doc_date').Value=null then
  begin
    skipped_nonregistered := skipped_nonregistered+1;
    exit;
  end;

  node_record := XMLDoc.CreateNode('����������');

  CreateAndAddNodeByValue('oktmo', soktmo, node_record);
  CreateAndAddNodeByField(ds, 'doc_num', node_record);
  CreateAndAddNodeByField(ds, 'doc_date', node_record);

  CreateAndAddNodeByField(ds, 'service_code', node_record);
  CreateAndAddNodeByField(ds, 'account_number', node_record);
  CreateAndAddNodeByField(ds, 'subdiv_code', node_record);
  CreateAndAddNodeByField(ds, 'person_type', node_record);
  CreateAndAddNodeByField(ds, 'person_name', node_record);
  CreateAndAddNodeByField(ds, 'inn', node_record);
  CreateAndAddNodeByField(ds, 'person_code', node_record);
  CreateAndAddNodeByField(ds, 'note', node_record);
  CreateAndAddNodeByField(ds, 'summa', node_record);
  CreateAndAddNodeByField(ds, 'uin', node_record);

  CreateAndAddNodeByField(ds, 'region_id', node_record);
  CreateAndAddNodeByField(ds, 'area_id', node_record);
  CreateAndAddNodeByField(ds, 'town_id', node_record);
  CreateAndAddNodeByField(ds, 'street_id', node_record);
  CreateAndAddNodeByField(ds, 'house', node_record);
  CreateAndAddNodeByField(ds, 'building', node_record);
  CreateAndAddNodeByField(ds, 'flat', node_record);

  if ds.FieldByName('person_type').AsInteger = 0 then
  begin
    CreateAndAddNodeByField(ds, 'snils', node_record);

    CreateAndAddNodeByField(ds, 'surname', node_record);
    CreateAndAddNodeByField(ds, 'firstname', node_record);
    CreateAndAddNodeByField(ds, 'secondname', node_record);
    CreateAndAddNodeByField(ds, 'sex', node_record);

    CreateAndAddNodeByField(ds, 'birthdate', node_record);
    CreateAndAddNodeByField(ds, 'address', node_record);
    CreateAndAddNodeByField(ds, 'doctype', node_record);
    CreateAndAddNodeByField(ds, 'docnumber', node_record);
    CreateAndAddNodeByField(ds, 'docseria', node_record);
    CreateAndAddNodeByField(ds, 'docwhen', node_record);
    CreateAndAddNodeByField(ds, 'docwho', node_record);
    CreateAndAddNodeByField(ds, 'contact_phone', node_record);
  end else
  begin
    CreateAndAddNodeByField(ds, 'jtcode', node_record);
  end;

  node_root.ChildNodes.Add(node_record);

end;

function TFormExportJD.CreateNodeByField(ds: TDataSet; fn: string): IXMLNode;
begin
  result := XMLDoc.CreateNode(fn, ntElement);
  result.NodeValue := ds.FieldByName(fn).AsString;
end;


procedure TFormExportJD.FormShow(Sender: TObject);
begin
  inherited;
  Processed := false;
end;

procedure TFormExportJD.CreateAndAddNodeByField(ds: TDataSet; fn: string; var parent_node: IXMLNode);
var node: IXMLNode;
begin
  node := CreateNodeByField(ds, fn);
  parent_node.ChildNodes.Add(node);
end;


procedure TFormExportJD.CreateAndAddNodeByValue(fn, value: string; var parent_node: IXMLNode);
var node: IXMLNode;
begin
  node := XMLDoc.CreateNode(fn, ntElement);
  node.NodeValue := value;
  parent_node.ChildNodes.Add(node);
end;

end.
