inherited FormSelectTemplate: TFormSelectTemplate
  Caption = #1042#1099#1073#1077#1088#1080#1090#1077' '#1096#1072#1073#1083#1086#1085' '#1087#1077#1095#1072#1090#1080
  ClientHeight = 247
  ClientWidth = 593
  ExplicitWidth = 607
  ExplicitHeight = 286
  TextHeight = 16
  inherited plBottom: TPanel
    Top = 206
    Width = 593
    ExplicitTop = 206
    ExplicitWidth = 591
    inherited btnCancel: TButton
      Left = 477
      ExplicitLeft = 475
    end
    inherited btnOk: TButton
      Left = 358
      Caption = #1042#1099#1073#1088#1072#1090#1100
      ExplicitLeft = 356
    end
  end
  object dgTemplates: TDBGridEh [1]
    Left = 0
    Top = 0
    Width = 593
    Height = 206
    Align = alClient
    AutoFitColWidths = True
    DataSource = dsLocal
    DynProps = <>
    GridLineParams.VertEmptySpaceStyle = dessNonEh
    Options = [dgEditing, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghDialogFind, dghColumnResize, dghColumnMove]
    ReadOnly = True
    TabOrder = 1
    Columns = <
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'name'
        Footers = <>
        TextEditing = False
        Title.Caption = #1064#1072#1073#1083#1086#1085
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -13
        Title.Font.Name = 'Verdana'
        Title.Font.Style = [fsBold]
        Width = 419
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  inherited dsLocal: TDataSource
    DataSet = qrAux
  end
  inherited qrAux: TFDQuery
    SQL.Strings = (
      'select * from printforms'
      
        'where (coalesce(unit_num,0) = :unit_num1 or coalesce(unit_num,0)' +
        ' = :unit_num2) '
      'order by name')
    ParamData = <
      item
        Name = 'UNIT_NUM1'
        ParamType = ptInput
      end
      item
        Name = 'UNIT_NUM2'
        ParamType = ptInput
      end>
  end
end
