inherited FormImport: TFormImport
  Caption = #1048#1084#1087#1086#1088#1090
  ClientHeight = 140
  ClientWidth = 534
  ExplicitWidth = 548
  ExplicitHeight = 179
  TextHeight = 17
  inherited plBottom: TPanel
    Top = 99
    Width = 534
    TabOrder = 1
    ExplicitTop = 99
    ExplicitWidth = 532
    inherited btnCancel: TButton
      Left = 418
      ExplicitLeft = 416
    end
    inherited btnOk: TButton
      Left = 299
      OnClick = btnOkClick
      ExplicitLeft = 297
    end
  end
  object edFileName: TDBEditEh [1]
    Left = 31
    Top = 40
    Width = 481
    Height = 25
    ControlLabel.Width = 110
    ControlLabel.Height = 17
    ControlLabel.Caption = #1060#1072#1081#1083' '#1076#1083#1103' '#1080#1084#1087#1086#1088#1090#1072
    ControlLabel.Visible = True
    DynProps = <>
    EditButtons = <
      item
        ShortCut = 45
        Style = ebsEllipsisEh
        OnClick = edFileNameEditButtons0Click
      end>
    TabOrder = 0
    Visible = True
  end
  inherited dsLocal: TDataSource
    Left = 184
    Top = 72
  end
  inherited qrAux: TFDQuery
    Left = 141
    Top = 76
  end
  object ExcelApplication1: TExcelApplication
    AutoConnect = False
    ConnectKind = ckRunningOrNew
    AutoQuit = False
    Left = 16
    Top = 72
  end
  object od: TOpenDialog
    Left = 80
    Top = 80
  end
end
