inherited FormGridMain: TFormGridMain
  Caption = #1043#1083#1072#1074#1085#1099#1081' '#1089#1087#1080#1089#1086#1082
  ClientHeight = 676
  ClientWidth = 1071
  ExplicitWidth = 1085
  ExplicitHeight = 715
  TextHeight = 16
  object Splitter1: TSplitter [0]
    Left = 169
    Top = 0
    Height = 676
    ExplicitTop = -55
    ExplicitHeight = 709
  end
  inherited plMain: TPanel
    Left = 172
    Width = 899
    Height = 676
    ExplicitLeft = 172
    ExplicitWidth = 897
    ExplicitHeight = 676
    inherited dgMain: TDBGridEh
      Width = 899
      Height = 604
      Ctl3D = False
      Flat = True
      Font.Height = -13
      OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghRowHighlight, dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove]
      ParentCtl3D = False
      STFilter.HorzLineColor = clSilver
      STFilter.VertLineColor = clSilver
      TitleParams.FillStyle = cfstGradientEh
      Columns = <
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'reestr_num'
          Footers = <>
          Title.Caption = #1056#1077#1077#1089#1090#1088
          Width = 112
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'doc_num'
          Footers = <>
          Title.Caption = #1053#1086#1084#1077#1088
          Width = 87
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'doc_date'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072
          Width = 80
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'account_number'
          Footers = <>
          Title.Caption = #1051#1080#1094#1077#1074#1086#1081' '#1089#1095#1077#1090
          Width = 131
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'service_name'
          Footers = <>
          Title.Caption = #1059#1089#1083#1091#1075#1072
          Width = 122
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'kbk'
          Footers = <>
          Title.Caption = #1050#1041#1050
          Width = 153
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'person_name'
          Footers = <>
          Title.Caption = #1054#1090#1074#1077#1090#1095#1080#1082
          Width = 224
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'note'
          Footers = <>
          Title.Caption = #1055#1086#1089#1090#1072#1085#1086#1074#1083#1077#1085#1080#1077
          Width = 128
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DisplayFormat = '### ### ##0.00'
          DynProps = <>
          EditButtons = <>
          FieldName = 'summa'
          Footers = <>
          Title.Caption = #1057#1091#1084#1084#1072
          Width = 94
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'uin'
          Footers = <>
          Title.Caption = #1059#1048#1053
          Width = 196
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'subdiv_name'
          Footers = <>
          Title.Caption = #1055#1086#1076#1088#1072#1079#1076#1077#1083#1077#1085#1080#1077
          Width = 214
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'oktmo'
          Footers = <>
          Title.Caption = #1054#1050#1058#1052#1054
          Width = 58
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'date_create'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072'/'#1074#1088#1077#1084#1103' '#1089#1086#1079#1076#1072#1085#1080#1103
          Width = 114
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'date_eif'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072' '#1047#1057
          Width = 82
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'register_cancelled'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072'/'#1074#1088#1077#1084#1103' '#1086#1090#1084#1077#1085#1099' '#1088#1077#1075#1080#1089#1090#1088#1072#1094#1080#1080
          Width = 129
        end>
    end
    inherited plTop: TPanel
      Width = 899
      ExplicitWidth = 897
      inherited sbDelete: TPngSpeedButton
        AlignWithMargins = False
        Left = 117
        Top = 0
        Height = 31
        ParentShowHint = False
        ShowHint = True
        ExplicitLeft = 184
        ExplicitTop = 0
        ExplicitHeight = 31
      end
      inherited sbEdit: TPngSpeedButton
        AlignWithMargins = False
        Left = 80
        Top = 0
        Height = 31
        ParentShowHint = False
        ShowHint = True
        ExplicitLeft = 141
        ExplicitHeight = 31
      end
      inherited sbAdd: TPngSpeedButton
        AlignWithMargins = False
        Left = 0
        Top = 0
        Height = 31
        ParentShowHint = False
        ShowHint = True
        ExplicitLeft = 95
        ExplicitTop = 0
        ExplicitHeight = 31
      end
      object sbConfirm: TPngSpeedButton [3]
        Left = 157
        Top = 0
        Width = 40
        Height = 31
        Hint = #1059#1090#1074#1077#1088#1076#1080#1090#1100
        Margins.Top = 0
        Margins.Bottom = 5
        Align = alLeft
        Flat = True
        ParentShowHint = False
        ShowHint = True
        OnClick = sbConfirmClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D4948445200000018000000180803000000D7A9CD
          CA000000017352474200AECE1CE9000000097048597300000EC300000EC301C7
          6FA86400000054504C5445000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000014
          E07DB80000001C74524E530001020304051315171D262999A3A4A6B0C6C8DFE3
          E7E9EAECF0F2FEEDC85DD5000000D74944415478DA8592EB0E82300C465B0853
          EE5122EFFF746A50895C160475956D8008A8FB35CE81F64B0BC29783BFC54813
          02F568E55AD42B61D5456331AEC4D66E10FBF79FBC32DCE0CA5B80B1B8887731
          6A795E3DA4D84132EA2A7949A0BE80843E38173013E869AE059DE883B3FBBB94
          5B3F066E3B69278C83B3510D15C788EDBB5278345DBFCC21F0948EC599861E86
          E767101605C9F8988CE262E04199CB10DDF3101743C8F4F826024C78EAC82AFE
          7C24003174623AC4C8D0A5C66397825895EA45AD6D46C312DB5B53DD1656ABDC
          FF9F61E1BC00FD3B761924F8B7260000000049454E44AE426082}
        ExplicitLeft = 184
        ExplicitTop = -3
        ExplicitHeight = 26
      end
      inherited sbPrint: TPngSpeedButton
        AlignWithMargins = False
        Left = 197
        Top = 0
        Height = 31
        ParentShowHint = False
        ShowHint = True
        OnClick = sbPrintClick
        ExplicitLeft = 184
        ExplicitTop = 0
        ExplicitHeight = 31
      end
      inherited sbExcel: TPngSpeedButton
        AlignWithMargins = False
        Left = 237
        Top = 0
        Height = 31
        ParentShowHint = False
        ShowHint = True
        ExplicitLeft = 230
        ExplicitTop = 0
        ExplicitHeight = 31
      end
      inherited sbImport: TPngSpeedButton
        Left = 856
        Align = alRight
        Visible = False
        ExplicitLeft = 49
      end
      inherited sbCopy: TPngSpeedButton
        AlignWithMargins = False
        Left = 40
        Top = 0
        Height = 31
        ParentShowHint = False
        ShowHint = True
        ExplicitLeft = 141
        ExplicitTop = 0
        ExplicitHeight = 31
      end
      object sbExport: TPngSpeedButton
        Left = 277
        Top = 0
        Width = 40
        Height = 31
        Hint = #1069#1082#1089#1087#1086#1088#1090' '#1076#1072#1085#1085#1099#1093' '#1074' '#1055#1072#1088#1091#1089' 10'
        Margins.Top = 0
        Margins.Bottom = 5
        Align = alLeft
        Flat = True
        ParentShowHint = False
        ShowHint = True
        OnClick = sbExportClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
          F8000000017352474200AECE1CE9000000097048597300000EC300000EC301C7
          6FA864000001594944415478DAC595CF2B055114C7EF45292551A29485A46CD8
          4916FE066BE259F9AF448A850DFE09D95216D8A084283B0B3FDEF89CDE999C66
          DECCDC3B8FDEA94F4D77EE39DF3BDF3BF75CEFFE397CB704CA84938839B94963
          7002533AC99B847EB887391DBB8049F8D0F7EF3000B7B0988A6405CE60019EA1
          2740601A1E61084674FC05C6DB09782DFC0D13D02CB0C15A24CF7D70082B3A7E
          671CC8093C19AB424272B661138E61591758FA05A94052515C2CDC8355D8852D
          B882419B5F57C06BF135388075F7BBD1B2E9C39D08D8E2FBD030FB75AE73E6EB
          0A648B6F54591923105D3C4620A4786D8BE43FDF0958B9E4BFE973D426CFC265
          802DF61C459F0311B981AF0ABB73F99D1CB42281A4EC0B625B4536D205160A48
          27FC74E5CDAE5D4831691D0FD05B6491C4A96BF5F26CBB0E11686A6169F94B76
          D53646E108665CD8CDE532F3AE5DAB6DBFBA8AE4BA7775EEC7E8DAA5FF67F103
          BC3874199D93FC0F0000000049454E44AE426082}
        ExplicitLeft = -10
        ExplicitTop = -3
        ExplicitHeight = 26
      end
    end
    inherited plBottom: TPanel
      Top = 635
      Width = 899
      ExplicitTop = 635
      ExplicitWidth = 897
      inherited btnCancel: TButton
        Left = 783
        ExplicitLeft = 781
      end
      inherited btnOk: TButton
        Left = 664
        ExplicitLeft = 662
      end
    end
  end
  object plLeft: TPanel [2]
    Left = 0
    Top = 0
    Width = 169
    Height = 676
    Align = alLeft
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object lbFilter: TLabel
      AlignWithMargins = True
      Left = 3
      Top = 496
      Width = 163
      Height = 15
      Align = alTop
      Alignment = taCenter
      Caption = #1092#1080#1083#1100#1090#1088' '#1087#1088#1080#1084#1077#1085#1077#1085
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -13
      Font.Name = 'Calibri'
      Font.Style = []
      ParentFont = False
      StyleElements = [seClient, seBorder]
      ExplicitWidth = 98
    end
    object grpPeriod: TGroupBox
      AlignWithMargins = True
      Left = 3
      Top = 3
      Width = 163
      Height = 142
      Align = alTop
      Caption = #1055#1077#1088#1080#1086#1076
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Calibri'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object sbChangePeriod: TSpeedButton
        AlignWithMargins = True
        Left = 5
        Top = 115
        Width = 153
        Height = 22
        Align = alBottom
        Caption = #1048#1079#1084#1077#1085#1080#1090#1100
        Flat = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -13
        Font.Name = 'Calibri'
        Font.Style = [fsUnderline]
        ParentFont = False
        StyleElements = [seClient, seBorder]
        OnClick = sbChangePeriodClick
        ExplicitLeft = 96
        ExplicitTop = 112
        ExplicitWidth = 23
      end
      object dtStart: TDBDateTimeEditEh
        Left = 11
        Top = 41
        Width = 132
        Height = 22
        ControlLabel.Width = 57
        ControlLabel.Height = 15
        ControlLabel.Caption = #1053#1072#1095#1080#1085#1072#1103' '#1089
        ControlLabel.Visible = True
        DynProps = <>
        EditButtons = <>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        Kind = dtkDateEh
        ParentFont = False
        TabOrder = 0
        Visible = True
        OnChange = meServicesChange
      end
      object dtEnd: TDBDateTimeEditEh
        Left = 11
        Top = 84
        Width = 132
        Height = 22
        ControlLabel.Width = 14
        ControlLabel.Height = 15
        ControlLabel.Caption = #1087#1086
        ControlLabel.Visible = True
        DynProps = <>
        EditButtons = <>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        Kind = dtkDateEh
        ParentFont = False
        TabOrder = 1
        Visible = True
        OnChange = meServicesChange
      end
    end
    object grpServices: TGroupBox
      AlignWithMargins = True
      Left = 3
      Top = 308
      Width = 163
      Height = 142
      Align = alTop
      Caption = #1059#1089#1083#1091#1075#1072
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Calibri'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      object meServices: TMemo
        AlignWithMargins = True
        Left = 5
        Top = 20
        Width = 153
        Height = 117
        Align = alClient
        BorderStyle = bsNone
        TabOrder = 0
        OnChange = meServicesChange
      end
    end
    object btApply: TButton
      AlignWithMargins = True
      Left = 3
      Top = 456
      Width = 163
      Height = 34
      Align = alTop
      Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      Enabled = False
      TabOrder = 2
      OnClick = btApplyClick
    end
    object rgStates: TDBRadioGroupEh
      AlignWithMargins = True
      Left = 3
      Top = 151
      Width = 163
      Height = 106
      Align = alTop
      Caption = #1057#1086#1089#1090#1086#1103#1085#1080#1077
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Calibri'
      Font.Style = []
      Items.Strings = (
        #1042#1089#1090#1091#1087#1080#1074#1096#1080#1077' '#1074' '#1089#1080#1083#1091
        #1053#1077' '#1074#1089#1090#1091#1087#1080#1074#1096#1080#1077' '#1074' '#1089#1080#1083#1091
        #1042#1089#1077)
      ParentBackground = True
      ParentFont = False
      TabOrder = 3
      OnChange = meServicesChange
    end
    object edReestr: TDBEditEh
      AlignWithMargins = True
      Left = 3
      Top = 280
      Width = 163
      Height = 22
      Margins.Top = 20
      Align = alTop
      ControlLabel.Width = 37
      ControlLabel.Height = 14
      ControlLabel.Caption = #1056#1077#1077#1089#1090#1088
      ControlLabel.Visible = True
      DynProps = <>
      EditButtons = <
        item
          Style = ebsEllipsisEh
          OnClick = edReestrEditButtons0Click
        end
        item
          Glyph.Data = {
            F6000000424DF600000000000000760000002800000010000000100000000100
            0400000000008000000000000000000000001000000000000000000000000000
            8000008000000080800080000000800080008080000080808000C0C0C0000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFFFFFFFFFF
            FFFFF8FFFFFFFFFFF8FF7078FFFFFFF8700F00078FFFFF87000F800008FFF870
            008FF800008F800008FFFF80000700008FFFFFF800000008FFFFFFFF8000008F
            FFFFFFFF8000008FFFFFFFF800000007FFFFFF70007870007FFFF700078F8700
            07FF700078FFFF70007F0007FFFFFFF7000F707FFFFFFFFF707F}
          Style = ebsGlyphEh
          OnClick = edReestrEditButtons1Click
        end>
      ReadOnly = True
      TabOrder = 4
      Visible = True
      OnChange = meServicesChange
    end
  end
  inherited FDUpdateSet: TFDUpdateSQL
    InsertSQL.Strings = (
      'INSERT INTO public.jdmain'
      
        '(note, doc_num, service_id, person_id, subdiv_id, account_id, su' +
        'mma, oktmo, date_eif)'
      
        'VALUES (:note, :doc_num, :service_id, :person_id, :subdiv_id, :a' +
        'ccount_id, :summa, :oktmo, :date_eif)'
      'RETURNING id;')
    ModifySQL.Strings = (
      'UPDATE jdmain'
      'SET '
      'docdaynum = :docdaynum, '
      'doc_num = :doc_num, '
      'doc_date = :doc_date,'
      'service_id = :service_id, '
      'person_id = :person_id,'
      'account_id = :account_id, '
      'uin = :uin, '
      'summa = :summa,'
      'oktmo = :oktmo, '
      'note = :note, '
      'subdiv_id = :subdiv_id, '
      'date_eif = :date_eif'
      'WHERE id = :id;')
    DeleteSQL.Strings = (
      'delete from jdmain where id = :id;')
    FetchRowSQL.Strings = (
      'select j.*,'
      
        'substring((select reestr_num||'#39' '#1086#1090' '#39'||to_char(reestr_date, '#39'dd.m' +
        'm.yyyy'#39') from reestr r where r.id = j.reestr_id), 1, 80)::varcha' +
        'r(80) as reestr_num,'
      'p.code as person_code,'
      's.code as service_code,'
      's.name as service_name,'
      'sd.code as subdiv_code,'
      'sd.name as subdiv_name,'
      's.KBK  as kbk,'
      'a.code as account_number,'
      'p.type as person_type,'
      'p.name as person_name,'
      'p.jtcode,'
      'p.inn,'
      'p.snils,'
      'p.surname,'
      'p.firstname,'
      'p.secondname,'
      'p.sex,'
      'p.birthdate,'
      'p.address,'
      'p.doctype,'
      'p.docnumber,'
      'p.docseria,'
      'p.docwhen,'
      'p.docwho,'
      'p.contact_phone,'
      'p.region_id,'
      'p.area_id,'
      'p.town_id,'
      'p.street_id,'
      'p.house,'
      'p.building,'
      'p.flat,'
      'p.postindex'
      'from jdmain j'
      'left outer join persons p on (p.id  = j.person_id)'
      'left outer join services s on (s.id  = j.service_id )'
      'left outer join subdivs sd on (sd.id  = j.subdiv_id )'
      'left outer join accounts a on (a.id  = j.account_id )'
      'where j.id = :id;')
  end
  inherited FDDataQr: TFDQuery
    BeforeOpen = FDDataQrBeforeOpen
    AfterInsert = FDDataQrAfterInsert
    AfterEdit = FDDataQrAfterEdit
    BeforePost = FDDataQrBeforePost
    AfterPost = FDDataQrAfterPost
    AfterCancel = FDDataQrAfterCancel
    AfterScroll = FDDataQrAfterScroll
    AfterRefresh = FDDataQrAfterRefresh
    SQL.Strings = (
      
        'with vars (nIsConfirmed, sServices, dStart, dEnd, nReestrId) as ' +
        '(values(:nIsConfirmed, :sServices, :dStart::date, :dEnd::date, :' +
        'nReestrId::integer))'
      'select row_number() over() as rownum, '
      'j.*,'
      
        'substring((select reestr_num||'#39' '#1086#1090' '#39'||to_char(reestr_date, '#39'dd.m' +
        'm.yyyy'#39') from reestr r where r.id = j.reestr_id), 1, 80)::varcha' +
        'r(80) as reestr_num,'
      'p.code as person_code,'
      's.code as service_code,'
      's.name as service_name,'
      'sd.code as subdiv_code,'
      'sd.name as subdiv_name,'
      's.KBK  as kbk,'
      'a.code as account_number,'
      'p.type as person_type,'
      'p.name as person_name,'
      'p.jtcode,'
      'p.inn,'
      'p.snils,'
      'p.surname,'
      'p.firstname,'
      'p.secondname,'
      'p.sex,'
      'p.birthdate,'
      'p.address,'
      'p.doctype,'
      'p.docnumber,'
      'p.docseria,'
      'p.docwhen,'
      'p.docwho,'
      'p.contact_phone,'
      'p.region_id,'
      'p.area_id,'
      'p.town_id,'
      'p.street_id,'
      'p.house,'
      'p.building,'
      'p.flat,'
      'p.postindex'
      'from jdmain j'
      'left outer join persons p on (p.id  = j.person_id)'
      'left outer join services s on (s.id  = j.service_id )'
      'left outer join subdivs sd on (sd.id  = j.subdiv_id )'
      'left outer join accounts a on (a.id  = j.account_id )'
      'inner join vars on ('
      '('
      ' (nIsConfirmed<0 or nIsConfirmed = 2) or'
      ' (nIsConfirmed =0 and j.doc_date is not null) or'
      ' (nIsConfirmed =1 and j.doc_date is null)'
      ')'
      'and'
      '('
      ' (nReestrId = 0)'
      ' or'
      ' (j.reestr_id = nReestrId)'
      ')'
      'and'
      '('
      ' (sServices = '#39#39')'
      ' or'
      ' (position(sServices in s.code)>0)'
      '  or'
      ' (position(sServices in s.Name)>0)'
      ')'
      'and ('
      ''
      #9'('
      #9' (nIsConfirmed = 0) and'
      #9' ('
      #9'   (doc_date >= dStart or dStart is null) and'
      #9'   (doc_date < (dEnd+1) or dEnd is null)'
      #9' )'
      #9')'
      #9'or'
      #9'('
      #9' (nIsConfirmed<>0) and'
      #9' ('
      
        #9'   ((date_create >= dStart or dStart is null) and (date_create ' +
        '< (dEnd+1) or dEnd is null))'
      #9'   or'
      
        #9'   ((doc_date >= dStart or dStart is null) and (doc_date < (dEn' +
        'd+1) or dEnd is null))'
      #9' )'
      #9')'
      ''
      ')'
      ')'
      'order by j.doc_date;')
    ParamData = <
      item
        Name = 'NISCONFIRMED'
        DataType = ftSmallint
        ParamType = ptInput
      end
      item
        Name = 'SSERVICES'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Name = 'DSTART'
        DataType = ftDateTime
        ParamType = ptInput
      end
      item
        Name = 'DEND'
        DataType = ftDateTime
        ParamType = ptInput
      end
      item
        Name = 'NREESTRID'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
  end
  inherited pmMain: TPopupMenu
    Left = 416
    Top = 232
    object N1: TMenuItem [3]
      Caption = '-'
    end
    object N4: TMenuItem [4]
      Caption = #1042#1082#1083#1102#1095#1080#1090#1100' '#1074' '#1088#1077#1077#1089#1090#1088'...'
      OnClick = N4Click
    end
    object N5: TMenuItem [5]
      Caption = #1048#1089#1082#1083#1102#1095#1080#1090#1100' '#1080#1079' '#1088#1077#1077#1089#1090#1088#1072
      OnClick = N5Click
    end
    object N3: TMenuItem [6]
      Caption = '-'
    end
    object N2: TMenuItem [7]
      Caption = #1054#1090#1084#1077#1085#1080#1090#1100' '#1088#1077#1075#1080#1089#1090#1088#1072#1094#1080#1102
      OnClick = N2Click
    end
  end
  inherited qrAux: TFDQuery
    SQL.Strings = (
      'select max(docdaynum) as docnummax'
      'from jdmain jd where jd.doc_date = :docdate;')
    Left = 416
    Top = 116
    ParamData = <
      item
        Name = 'DOCDATE'
        ParamType = ptInput
      end>
  end
end
