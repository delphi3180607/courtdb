inherited FormSQLQuery: TFormSQLQuery
  Caption = 'SQL '#1079#1072#1087#1088#1086#1089
  ClientHeight = 537
  ClientWidth = 735
  ExplicitWidth = 751
  ExplicitHeight = 576
  TextHeight = 16
  object Splitter1: TSplitter [0]
    Left = 0
    Top = 273
    Width = 735
    Height = 3
    Cursor = crVSplit
    Align = alTop
    ExplicitLeft = -111
    ExplicitWidth = 860
  end
  inherited plBottom: TPanel
    Top = 496
    Width = 735
    ExplicitTop = 496
    ExplicitWidth = 735
    inherited btnCancel: TButton
      Left = 633
      Caption = #1047#1072#1082#1088#1099#1090#1100
      ExplicitLeft = 619
    end
    inherited btnOk: TButton
      Left = 514
      Visible = False
      ExplicitLeft = 500
    end
  end
  object plTool: TPanel [2]
    Left = 0
    Top = 0
    Width = 735
    Height = 32
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 1
    object btExec: TButton
      AlignWithMargins = True
      Left = 134
      Top = 4
      Width = 124
      Height = 24
      Align = alLeft
      Caption = #1044#1077#1081#1089#1090#1074#1080#1077
      TabOrder = 0
      OnClick = btExecClick
    end
    object btSelect: TButton
      AlignWithMargins = True
      Left = 4
      Top = 4
      Width = 124
      Height = 24
      Align = alLeft
      Caption = #1042#1099#1073#1086#1088#1082#1072
      TabOrder = 1
      OnClick = btSelectClick
    end
    object btFile: TButton
      AlignWithMargins = True
      Left = 264
      Top = 4
      Width = 124
      Height = 24
      Align = alLeft
      Caption = #1054#1090#1082#1088#1099#1090#1100' '#1092#1072#1081#1083
      TabOrder = 2
      OnClick = btFileClick
    end
  end
  object se: TSynEdit [3]
    Left = 0
    Top = 32
    Width = 735
    Height = 241
    Align = alTop
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Courier New'
    Font.Style = []
    Font.Quality = fqClearTypeNatural
    TabOrder = 2
    UseCodeFolding = False
    Gutter.Font.Charset = DEFAULT_CHARSET
    Gutter.Font.Color = clWindowText
    Gutter.Font.Height = -11
    Gutter.Font.Name = 'Courier New'
    Gutter.Font.Style = []
    Gutter.Font.Quality = fqClearTypeNatural
    Gutter.Bands = <
      item
        Kind = gbkMarks
        Width = 13
      end
      item
        Kind = gbkLineNumbers
      end
      item
        Kind = gbkFold
      end
      item
        Kind = gbkTrackChanges
      end
      item
        Kind = gbkMargin
        Width = 3
      end>
    SelectedColor.Alpha = 0.400000005960464500
  end
  object pc: TPageControl [4]
    AlignWithMargins = True
    Left = 3
    Top = 279
    Width = 729
    Height = 214
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 3
    object TabSheet1: TTabSheet
      Caption = #1042#1099#1073#1086#1088#1082#1072
      object dg: TDBGridEh
        Left = 0
        Top = 0
        Width = 735
        Height = 189
        Align = alClient
        DataSource = dsLocal
        DynProps = <>
        TabOrder = 0
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = #1044#1077#1081#1089#1090#1074#1080#1077
      ImageIndex = 1
      object meLog: TMemo
        Left = 0
        Top = 0
        Width = 735
        Height = 189
        Align = alClient
        TabOrder = 0
      end
    end
  end
  inherited dsLocal: TDataSource
    DataSet = qrAux
  end
  object od: TOpenDialog
    Left = 280
    Top = 152
  end
end
