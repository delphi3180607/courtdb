object FormConnectionSettings: TFormConnectionSettings
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1080' '#1087#1086#1076#1082#1083#1102#1095#1077#1085#1080#1103' '#1080' '#1080#1084#1087#1086#1088#1090#1072
  ClientHeight = 270
  ClientWidth = 482
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Segoe UI'
  Font.Style = []
  Position = poScreenCenter
  TextHeight = 17
  object Label2: TLabel
    Left = 6
    Top = 60
    Width = 107
    Height = 17
    Caption = #1048#1084#1103' '#1073#1072#1079#1099' '#1076#1072#1085#1085#1099#1093
  end
  object Label1: TLabel
    Left = 6
    Top = 9
    Width = 122
    Height = 17
    Caption = #1059#1082#1072#1078#1080#1090#1077' SQL '#1089#1077#1088#1074#1077#1088
  end
  object lePassword: TLabeledEdit
    Left = 6
    Top = 177
    Width = 288
    Height = 25
    EditLabel.Width = 46
    EditLabel.Height = 17
    EditLabel.Caption = #1055#1072#1088#1086#1083#1100
    PasswordChar = '*'
    TabOrder = 4
    Text = ''
  end
  object leLogin: TLabeledEdit
    Left = 6
    Top = 127
    Width = 288
    Height = 25
    EditLabel.Width = 36
    EditLabel.Height = 17
    EditLabel.Caption = #1051#1086#1075#1080#1085
    TabOrder = 3
    Text = ''
  end
  object cmbDBName: TComboBox
    Left = 6
    Top = 77
    Width = 465
    Height = 25
    Color = clWhite
    TabOrder = 2
  end
  object cmbServer: TComboBox
    Left = 6
    Top = 27
    Width = 288
    Height = 25
    Color = clWhite
    TabOrder = 0
  end
  object plBottom: TPanel
    Left = 0
    Top = 224
    Width = 482
    Height = 46
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 5
    ExplicitWidth = 480
    object btOk: TSpeedButton
      AlignWithMargins = True
      Left = 168
      Top = 3
      Width = 150
      Height = 40
      Align = alRight
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
      OnClick = btOkClick
    end
    object btCancel: TSpeedButton
      AlignWithMargins = True
      Left = 324
      Top = 3
      Width = 155
      Height = 40
      Align = alRight
      Caption = #1042#1099#1093#1086#1076
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
      OnClick = btCancelClick
    end
  end
  object lePort: TLabeledEdit
    Left = 312
    Top = 27
    Width = 159
    Height = 24
    EditLabel.Width = 30
    EditLabel.Height = 17
    EditLabel.Caption = #1055#1086#1088#1090
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    Text = ''
  end
end
