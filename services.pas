unit services;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, FireDAC.Stan.Intf, FireDAC.Stan.Param,
  FireDAC.Phys.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Vcl.Menus, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.Buttons, PngSpeedButton,
  EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh, Vcl.ExtCtrls, MemTableDataEh,
  MemTableEh, Vcl.StdCtrls, EXLReportExcelTLB, EXLReportBand, EXLReport;

type
  TFormServices = class(TFormGrid)
  private
    { Private declarations }
  public
    procedure Init; override;
    procedure ImportSheet(data:Variant; DimX:integer; DimY:integer); override;
  end;

var
  FormServices: TFormServices;

implementation

{$R *.dfm}

uses editcodename, editservice;

procedure TFormServices.Init;
begin
  inherited;
  self.FormEditLocal := FormEditService;
end;


procedure TFormServices.ImportSheet(data:Variant; DimX:integer; DimY:integer);
var i, n_kbk, n_code, n_name, n_note: integer; code, kbk, name, note: string;
begin

  n_code := 0;
  n_name := 0;
  n_note := 0;
  n_kbk := 0;

  for i := 1 to DimX do
  begin

    if data[1,i]='���' then
    n_code := i;

    if data[1,i]='������������' then
    n_name := i;

    if data[1,i]='���������� �������' then
    n_note := i;

    if data[1,i]='���' then
    n_kbk := i;

  end;

  qrAux.Close;
  qrAux.SQL.Clear;
  qrAux.SQL.Add('insert into services (code, name, kbk, note)');
  qrAux.SQL.Add('select :code, :name, :kbk, :note where not exists (select 1 from services s where s.code = :code1);');

  code := '';
  name := '';
  kbk := '';
  note := '';

  for i := 2 to DimY do
  begin

      if n_code>0 then code := data[i,n_code];
      if n_name>0 then name := data[i,n_name];
      if n_kbk>0 then kbk := data[i,n_kbk];
      if n_note>0 then note := data[i,n_note];

      if trim(code) = '' then continue;

      qrAux.Params.ParamByName('code').Value := code;
      qrAux.Params.ParamByName('code1').Value := code;
      qrAux.Params.ParamByName('name').Value := name;
      qrAux.Params.ParamByName('kbk').Value := kbk;
      qrAux.Params.ParamByName('note').Value := note;

      try
        qrAux.ExecSQL;
      except
        on E : Exception do
        begin
          ShowMessage('������ ��� �������: '+E.Message);
          exit;
        end;
      end;
  end;

end;



end.
