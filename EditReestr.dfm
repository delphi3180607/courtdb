inherited FormEditReestr: TFormEditReestr
  Caption = #1056#1077#1077#1089#1090#1088
  ClientHeight = 175
  ClientWidth = 305
  ExplicitWidth = 319
  ExplicitHeight = 214
  TextHeight = 16
  inherited plBottom: TPanel
    Top = 134
    Width = 305
    TabOrder = 2
    ExplicitTop = 134
    ExplicitWidth = 303
    inherited btnCancel: TButton
      Left = 189
      ExplicitLeft = 187
    end
    inherited btnOk: TButton
      Left = 70
      ExplicitLeft = 68
    end
  end
  object edDocNum: TDBEditEh [1]
    Left = 8
    Top = 30
    Width = 121
    Height = 24
    ControlLabel.Width = 42
    ControlLabel.Height = 16
    ControlLabel.Caption = #1053#1086#1084#1077#1088
    ControlLabel.Visible = True
    DataField = 'reestr_num'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 0
    Visible = True
  end
  object dtDocDate: TDBDateTimeEditEh [2]
    Left = 140
    Top = 30
    Width = 121
    Height = 24
    ControlLabel.Width = 33
    ControlLabel.Height = 16
    ControlLabel.Caption = #1044#1072#1090#1072
    ControlLabel.Visible = True
    DataField = 'reestr_date'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Kind = dtkDateEh
    TabOrder = 1
    Visible = True
    OnChange = dtDocDateChange
  end
  inherited dsLocal: TDataSource
    Left = 40
    Top = 52
  end
  inherited qrAux: TFDQuery
    Left = 37
    Top = 108
  end
end
