unit Settings;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, dmu, DBSQLLookUp, Vcl.StdCtrls, Vcl.Mask, DBCtrlsEh,
  FireDAC.Stan.Intf, FireDAC.Stan.Param, FireDAC.Phys.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.Stan.Async,
  FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client, base,
  Vcl.ExtCtrls;

type
  TFormSettings = class(TFormBase)
    laDefaultAccount: TDBSQLLookUp;
    edOKTMO: TDBEditEh;
    edJDNumber: TDBEditEh;
    edJPNumber: TDBEditEh;
    btSave: TButton;
    lbMessage: TLabel;
    edURN: TDBEditEh;
    luSubDiv: TDBSQLLookUp;
    procedure laDefaultAccountChange(Sender: TObject);
    procedure btSaveClick(Sender: TObject);
  private
    { Private declarations }
  public
    urn: string;
    default_account_id: integer;
    default_subdiv_id: integer;
    oktmo: string;
    jdnumber: string;
    jpnumber: string;
    procedure Init; override;
  end;

var
  FormSettings: TFormSettings;

implementation

{$R *.dfm}

procedure TFormSettings.Init;
begin

  FDDataQr.Close;
  FDDataQr.Open;

  urn := FDDataQr.FieldByName('urn').AsString;
  default_account_id := FDDataQr.FieldByName('default_account_id').AsInteger;
  default_subdiv_id := FDDataQr.FieldByName('default_subdiv_id').AsInteger;
  oktmo := FDDataQr.FieldByName('oktmo').AsString;
  jdnumber := FDDataQr.FieldByName('jdnumber').AsString;
  jpnumber := FDDataQr.FieldByName('jpnumber').AsString;

  FDDataQr.Edit;

end;


procedure TFormSettings.btSaveClick(Sender: TObject);
begin

  FDDataQr.Post;
  btSave.Enabled := false;
  lbMessage.Caption := '��������� ���������.';
  lbMessage.Font.Color := clBlack;

  Init;

end;

procedure TFormSettings.laDefaultAccountChange(Sender: TObject);
begin
  if not Assigned(btSave) then exit;
  btSave.Enabled := true;
  lbMessage.Caption := '��������� ��������.';
  lbMessage.Font.Color := clRed;
end;

end.
