unit EditUser;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, editcodename, Vcl.StdCtrls, DBCtrlsEh,
  Data.DB, Vcl.Mask, Vcl.ExtCtrls, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client;

type
  TFormEditUser = class(TFormEditCodeName)
    DBCheckBoxEh1: TDBCheckBoxEh;
    cbBlocked: TDBCheckBoxEh;
    edPassword: TDBEditEh;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditUser: TFormEditUser;

implementation

{$R *.dfm}

end.
