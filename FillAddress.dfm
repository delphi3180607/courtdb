﻿inherited FormFillAddress: TFormFillAddress
  Caption = #1040#1076#1088#1077#1089' '#1080#1079' '#1050#1083#1072#1076#1088
  ClientHeight = 356
  ClientWidth = 630
  OnShow = FormShow
  ExplicitWidth = 636
  ExplicitHeight = 384
  PixelsPerInch = 96
  TextHeight = 16
  inherited plBottom: TPanel
    Top = 315
    Width = 630
    ExplicitTop = 315
    ExplicitWidth = 630
    inherited btnCancel: TButton
      Left = 514
      ExplicitLeft = 514
    end
    inherited btnOk: TButton
      Left = 395
      ExplicitLeft = 395
    end
  end
  object pcAddress: TPageControl [1]
    Left = 0
    Top = 0
    Width = 630
    Height = 315
    ActivePage = КЛАДР
    Align = alClient
    TabOrder = 1
    object КЛАДР: TTabSheet
      Caption = #1050#1051#1040#1044#1056
      object laRegion: TDBSQLLookUp
        Left = 8
        Top = 26
        Width = 589
        Height = 27
        Color = 14875388
        ControlLabel.Width = 46
        ControlLabel.Height = 16
        ControlLabel.Caption = #1056#1077#1075#1080#1086#1085
        ControlLabel.Visible = True
        DataField = 'region_id'
        DataSource = dsLocal
        DynProps = <>
        EditButtons = <
          item
          end>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
        StyleElements = [seFont, seBorder]
        TabOrder = 0
        Visible = True
        SqlSet = ssRegion
        KeyValue = Null
        RowCount = 0
        OnKeyValueChange = laRegionKeyValueChange
      end
      object laTown: TDBSQLLookUp
        Left = 8
        Top = 130
        Width = 589
        Height = 27
        Color = 14875388
        ControlLabel.Width = 171
        ControlLabel.Height = 16
        ControlLabel.Caption = #1043#1086#1088#1086#1076'/'#1053#1072#1089#1077#1083#1077#1085#1085#1099#1081' '#1087#1091#1085#1082#1090
        ControlLabel.Visible = True
        DataField = 'town_id'
        DataSource = dsLocal
        DynProps = <>
        EditButtons = <
          item
          end>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
        StyleElements = [seFont, seBorder]
        TabOrder = 1
        Visible = True
        SqlSet = ssTown
        KeyValue = Null
        RowCount = 0
        OnKeyValueChange = laTownKeyValueChange
      end
      object laStreet: TDBSQLLookUp
        Left = 8
        Top = 188
        Width = 589
        Height = 27
        Color = 14875388
        ControlLabel.Width = 42
        ControlLabel.Height = 16
        ControlLabel.Caption = #1059#1083#1080#1094#1072
        ControlLabel.Visible = True
        DataField = 'street_id'
        DataSource = dsLocal
        DynProps = <>
        EditButtons = <
          item
          end>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
        StyleElements = [seFont, seBorder]
        TabOrder = 2
        Visible = True
        SqlSet = ssStreet
        KeyValue = Null
        RowCount = 0
      end
      object edHouse: TDBEditEh
        Left = 8
        Top = 245
        Width = 121
        Height = 24
        ControlLabel.Width = 27
        ControlLabel.Height = 16
        ControlLabel.Caption = #1044#1086#1084
        ControlLabel.Visible = True
        DataField = 'house'
        DataSource = dsLocal
        DynProps = <>
        EditButtons = <>
        TabOrder = 3
        Visible = True
      end
      object edBuilding: TDBEditEh
        Left = 144
        Top = 245
        Width = 121
        Height = 24
        ControlLabel.Width = 64
        ControlLabel.Height = 16
        ControlLabel.Caption = #1057#1090#1088#1086#1077#1085#1080#1077
        ControlLabel.Visible = True
        DataField = 'building'
        DataSource = dsLocal
        DynProps = <>
        EditButtons = <>
        TabOrder = 4
        Visible = True
      end
      object edFlat: TDBEditEh
        Left = 282
        Top = 245
        Width = 121
        Height = 24
        ControlLabel.Width = 63
        ControlLabel.Height = 16
        ControlLabel.Caption = #1050#1074#1072#1088#1090#1080#1088#1072
        ControlLabel.Visible = True
        DataField = 'flat'
        DataSource = dsLocal
        DynProps = <>
        EditButtons = <>
        TabOrder = 5
        Visible = True
      end
      object edPIndex: TDBEditEh
        Left = 422
        Top = 245
        Width = 121
        Height = 24
        ControlLabel.Width = 63
        ControlLabel.Height = 16
        ControlLabel.Caption = #1055'.'#1048#1085#1076#1077#1082#1089
        ControlLabel.Visible = True
        DataField = 'postindex'
        DataSource = dsLocal
        DynProps = <>
        EditButtons = <>
        TabOrder = 6
        Visible = True
      end
      object laArea: TDBSQLLookUp
        Left = 8
        Top = 77
        Width = 589
        Height = 27
        Color = 14875388
        ControlLabel.Width = 40
        ControlLabel.Height = 16
        ControlLabel.Caption = #1056#1072#1081#1086#1085
        ControlLabel.Visible = True
        DataField = 'area_id'
        DataSource = dsLocal
        DynProps = <>
        EditButtons = <
          item
          end>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
        StyleElements = [seFont, seBorder]
        TabOrder = 7
        Visible = True
        SqlSet = ssArea
        KeyValue = Null
        RowCount = 0
        OnKeyValueChange = laAreaKeyValueChange
      end
    end
    object TabSheet2: TTabSheet
      Caption = #1056#1091#1095#1085#1086#1081' '#1074#1074#1086#1076
      ImageIndex = 1
      object armRegion: TDBEditEh
        Left = 11
        Top = 25
        Width = 589
        Height = 27
        Color = 14875388
        ControlLabel.Width = 46
        ControlLabel.Height = 16
        ControlLabel.Caption = #1056#1077#1075#1080#1086#1085
        ControlLabel.Visible = True
        DataField = 'region'
        DataSource = dsLocal
        DynProps = <>
        EditButtons = <>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
        StyleElements = [seFont, seBorder]
        TabOrder = 0
        Visible = True
      end
      object armArea: TDBEditEh
        Left = 11
        Top = 76
        Width = 589
        Height = 27
        Color = 14875388
        ControlLabel.Width = 40
        ControlLabel.Height = 16
        ControlLabel.Caption = #1056#1072#1081#1086#1085
        ControlLabel.Visible = True
        DataField = 'area'
        DataSource = dsLocal
        DynProps = <>
        EditButtons = <>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
        StyleElements = [seFont, seBorder]
        TabOrder = 1
        Visible = True
      end
      object armCity: TDBEditEh
        Left = 11
        Top = 129
        Width = 589
        Height = 27
        Color = 14875388
        ControlLabel.Width = 171
        ControlLabel.Height = 16
        ControlLabel.Caption = #1043#1086#1088#1086#1076'/'#1053#1072#1089#1077#1083#1077#1085#1085#1099#1081' '#1087#1091#1085#1082#1090
        ControlLabel.Visible = True
        DataField = 'town'
        DataSource = dsLocal
        DynProps = <>
        EditButtons = <>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
        StyleElements = [seFont, seBorder]
        TabOrder = 2
        Visible = True
      end
      object armStreet: TDBEditEh
        Left = 11
        Top = 187
        Width = 589
        Height = 27
        Color = 14875388
        ControlLabel.Width = 42
        ControlLabel.Height = 16
        ControlLabel.Caption = #1059#1083#1080#1094#1072
        ControlLabel.Visible = True
        DataField = 'street'
        DataSource = dsLocal
        DynProps = <>
        EditButtons = <>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
        StyleElements = [seFont, seBorder]
        TabOrder = 3
        Visible = True
      end
      object armHouse: TDBEditEh
        Left = 11
        Top = 244
        Width = 121
        Height = 24
        ControlLabel.Width = 27
        ControlLabel.Height = 16
        ControlLabel.Caption = #1044#1086#1084
        ControlLabel.Visible = True
        DataField = 'house'
        DataSource = dsLocal
        DynProps = <>
        EditButtons = <>
        TabOrder = 4
        Visible = True
      end
      object armBuilding: TDBEditEh
        Left = 147
        Top = 244
        Width = 121
        Height = 24
        ControlLabel.Width = 64
        ControlLabel.Height = 16
        ControlLabel.Caption = #1057#1090#1088#1086#1077#1085#1080#1077
        ControlLabel.Visible = True
        DataField = 'building'
        DataSource = dsLocal
        DynProps = <>
        EditButtons = <>
        TabOrder = 5
        Visible = True
      end
      object armFlat: TDBEditEh
        Left = 285
        Top = 244
        Width = 121
        Height = 24
        ControlLabel.Width = 63
        ControlLabel.Height = 16
        ControlLabel.Caption = #1050#1074#1072#1088#1090#1080#1088#1072
        ControlLabel.Visible = True
        DataField = 'flat'
        DataSource = dsLocal
        DynProps = <>
        EditButtons = <>
        TabOrder = 6
        Visible = True
      end
      object armPIndex: TDBEditEh
        Left = 425
        Top = 244
        Width = 121
        Height = 24
        ControlLabel.Width = 63
        ControlLabel.Height = 16
        ControlLabel.Caption = #1055'.'#1048#1085#1076#1077#1082#1089
        ControlLabel.Visible = True
        DataField = 'postindex'
        DataSource = dsLocal
        DynProps = <>
        EditButtons = <>
        TabOrder = 7
        Visible = True
      end
    end
  end
  inherited qrAux: TFDQuery
    Top = 148
  end
  object ssRegion: TDBXLookUpSqlSet
    TmplSql.Strings = (
      'select  k.id::text, k."Name"||'#39' '#39'||a."Short"  as FullName'
      
        'from public."KLADR" k, public."KLADRAcronym" a where k."Acronym_' +
        'id"  = a.id'
      'and k."Type" = '#39'b3a65fdc-f214-4d77-81bc-5c69e0fa2b47'#39
      'order by k."Name" ')
    DownSql.Strings = (
      'select  k.id::text, k."Name"||'#39' '#39'||a."Short"  as FullName'
      
        'from public."KLADR" k, public."KLADRAcronym" a where k."Acronym_' +
        'id"  = a.id'
      'and k."Type" = '#39'b3a65fdc-f214-4d77-81bc-5c69e0fa2b47'#39
      'order by k."Name" ')
    InitSql.Strings = (
      'select  k.id::text, k."Name"||'#39' '#39'||a."Short"  as FullName'
      
        'from public."KLADR" k, public."KLADRAcronym" a where k."Acronym_' +
        'id"  = a.id'
      'and k."Type" = '#39'b3a65fdc-f214-4d77-81bc-5c69e0fa2b47'#39
      'and k.id = @id')
    KeyName = 'id'
    DisplayName = 'FullName'
    SQLConnection = dm.FDConnMain
    Left = 168
    Top = 24
  end
  object ssArea: TDBXLookUpSqlSet
    TmplSql.Strings = (
      'select  k.id::text, k."Name"||'#39' '#39'||a."Short"  as FullName'
      
        'from public."KLADR" k, public."KLADRAcronym" a where k."Acronym_' +
        'id"  = a.id'
      'and k."Region_id" = uuid( :regionid )'
      'and k."Type" = '#39'7d738715-6082-4bf9-bf5f-bc64797dc610'#39
      'order by k."Name" ')
    DownSql.Strings = (
      'select  k.id::text, k."Name"||'#39' '#39'||a."Short"  as FullName'
      
        'from public."KLADR" k, public."KLADRAcronym" a where k."Acronym_' +
        'id"  = a.id'
      'and k."Region_id" = uuid( :regionid )'
      'and k."Type" = '#39'7d738715-6082-4bf9-bf5f-bc64797dc610'#39
      'order by k."Name" ')
    InitSql.Strings = (
      'select  k.id::text, k."Name"||'#39' '#39'||a."Short"  as FullName'
      
        'from public."KLADR" k, public."KLADRAcronym" a where k."Acronym_' +
        'id"  = a.id'
      'and k."Type" = '#39'7d738715-6082-4bf9-bf5f-bc64797dc610'#39
      'and k.id = @id')
    KeyName = 'id'
    DisplayName = 'FullName'
    SQLConnection = dm.FDConnMain
    Left = 224
    Top = 72
  end
  object ssTown: TDBXLookUpSqlSet
    TmplSql.Strings = (
      'select  k.id::text, k."Name"||'#39' '#39'||a."Short"  as FullName'
      
        'from public."KLADR" k, public."KLADRAcronym" a where k."Acronym_' +
        'id"  = a.id'
      
        'and (k."Area_id" = uuid( :areaid ) or (k."Area_id" is null and u' +
        'uid( :areaid1 ) is null) )'
      'and k."Region_id" = uuid( :regionid )'
      
        'and k."Type" in ('#39'6e5c668e-bec7-45bd-bc77-6c3fb40c802b'#39', '#39'c5e87e' +
        '32-8064-4309-ad18-459abc4e6ec3'#39')'
      'and k."Name" like '#39'@pattern%'#39
      'order by k."Name" ')
    DownSql.Strings = (
      'select  k.id::text, k."Name"||'#39' '#39'||a."Short"  as FullName'
      
        'from public."KLADR" k, public."KLADRAcronym" a where k."Acronym_' +
        'id"  = a.id'
      
        'and (k."Area_id" = uuid( :areaid ) or (k."Area_id" is null and u' +
        'uid( :areaid1 ) is null) )'
      'and k."Region_id" = uuid( :regionid )'
      
        'and k."Type" in ('#39'6e5c668e-bec7-45bd-bc77-6c3fb40c802b'#39', '#39'c5e87e' +
        '32-8064-4309-ad18-459abc4e6ec3'#39')'
      'and k."Name" like '#39'@pattern%'#39
      'order by k."Name"')
    InitSql.Strings = (
      'select  k.id::text, k."Name"||'#39' '#39'||a."Short"  as FullName'
      
        'from public."KLADR" k, public."KLADRAcronym" a where k."Acronym_' +
        'id"  = a.id'
      
        'and k."Type" in ('#39'6e5c668e-bec7-45bd-bc77-6c3fb40c802b'#39', '#39'c5e87e' +
        '32-8064-4309-ad18-459abc4e6ec3'#39')'
      'and k.id = @id')
    KeyName = 'id'
    DisplayName = 'FullName'
    SQLConnection = dm.FDConnMain
    Left = 256
    Top = 120
  end
  object ssStreet: TDBXLookUpSqlSet
    TmplSql.Strings = (
      'select  k.id::text, k."Name"||'#39' '#39'||a."Short"  as FullName'
      
        'from public."KLADR" k, public."KLADRAcronym" a where k."Acronym_' +
        'id"  = a.id'
      
        'and (k."Town_id" = uuid( :townid ) or k."Settlement_id" = uuid( ' +
        ':settlid ))'
      'and k."Type" = '#39'34805f03-ef60-4397-b5d8-594ee5a32971'#39
      'and k."Name" like '#39'@pattern%'#39
      'order by k."Name" ')
    DownSql.Strings = (
      'select  k.id::text, k."Name"||'#39' '#39'||a."Short"  as FullName'
      
        'from public."KLADR" k, public."KLADRAcronym" a where k."Acronym_' +
        'id"  = a.id'
      
        'and (k."Town_id" = uuid( :townid ) or k."Settlement_id" = uuid( ' +
        ':settlid ))'
      'and k."Type" = '#39'34805f03-ef60-4397-b5d8-594ee5a32971'#39
      'and k."Name" like '#39'@pattern%'#39
      'order by k."Name" '
      '')
    InitSql.Strings = (
      'select  k.id::text, k."Name"||'#39' '#39'||a."Short"  as FullName'
      
        'from public."KLADR" k, public."KLADRAcronym" a where k."Acronym_' +
        'id"  = a.id'
      'and k."Type" = '#39'34805f03-ef60-4397-b5d8-594ee5a32971'#39
      'and k.id = @id')
    KeyName = 'id'
    DisplayName = 'FullName'
    SQLConnection = dm.FDConnMain
    Left = 152
    Top = 168
  end
end
