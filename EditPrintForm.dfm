inherited FormEditPrintForm: TFormEditPrintForm
  Caption = #1064#1072#1073#1083#1086#1085' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
  ClientHeight = 531
  ClientWidth = 862
  ExplicitWidth = 876
  ExplicitHeight = 570
  TextHeight = 16
  object Label1: TLabel [0]
    Left = 8
    Top = 125
    Width = 43
    Height = 17
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    Caption = #1047#1072#1087#1088#1086#1089
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
  end
  inherited plBottom: TPanel
    Top = 490
    Width = 862
    ParentFont = False
    TabOrder = 5
    ExplicitTop = 490
    ExplicitWidth = 860
    inherited btnCancel: TButton
      Left = 746
      ExplicitLeft = 744
    end
    inherited btnOk: TButton
      Left = 627
      ParentFont = True
      ExplicitLeft = 625
    end
  end
  object edTitle: TDBEditEh [2]
    Left = 7
    Top = 27
    Width = 436
    Height = 25
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    ControlLabel.Width = 216
    ControlLabel.Height = 16
    ControlLabel.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1087#1077#1095#1072#1090#1085#1086#1081' '#1092#1086#1088#1084#1099
    ControlLabel.Visible = True
    Ctl3D = True
    DataField = 'name'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = []
    HighlightRequired = True
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 0
    Visible = True
  end
  object edTemplate: TDBEditEh [3]
    Left = 8
    Top = 83
    Width = 435
    Height = 25
    ControlLabel.Width = 53
    ControlLabel.Height = 16
    ControlLabel.Caption = #1064#1072#1073#1083#1086#1085
    ControlLabel.Visible = True
    DataField = 'template'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
        Style = ebsEllipsisEh
        OnClick = edTemplateEditButtons0Click
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    Visible = True
  end
  object meSQL: TDBMemo [4]
    Left = 8
    Top = 146
    Width = 841
    Height = 338
    DataField = 'sql_text'
    DataSource = dsLocal
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
  end
  object edGroupField: TDBEditEh [5]
    Left = 454
    Top = 83
    Width = 227
    Height = 25
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    ControlLabel.Width = 148
    ControlLabel.Height = 16
    ControlLabel.Caption = #1043#1088#1091#1087#1087#1080#1088#1086#1074#1086#1095#1085#1086#1077' '#1087#1086#1083#1077
    ControlLabel.Visible = True
    Ctl3D = True
    DataField = 'group_field'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = []
    HighlightRequired = True
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 2
    Visible = True
  end
  object cbUnit: TDBComboBoxEh [6]
    Left = 454
    Top = 28
    Width = 227
    Height = 24
    ControlLabel.Width = 47
    ControlLabel.Height = 16
    ControlLabel.Caption = #1056#1072#1079#1076#1077#1083
    ControlLabel.Visible = True
    DataField = 'unit_num'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Items.Strings = (
      ''
      #1043#1083#1072#1074#1085#1099#1081' '#1089#1087#1080#1089#1086#1082
      #1056#1077#1077#1089#1090#1088#1099)
    KeyItems.Strings = (
      '0'
      '1'
      '2')
    TabOrder = 4
    Visible = True
  end
end
