inherited FormReestrs: TFormReestrs
  Caption = #1056#1077#1077#1089#1090#1088#1099' '#1087#1086#1089#1090#1072#1085#1086#1074#1083#1077#1085#1080#1081
  ClientWidth = 934
  TextHeight = 16
  inherited plMain: TPanel
    Width = 934
    object Splitter1: TSplitter [0]
      Left = 0
      Top = 324
      Width = 934
      Height = 4
      Cursor = crVSplit
      Align = alBottom
      ExplicitLeft = -3
      ExplicitTop = 414
      ExplicitWidth = 932
    end
    inherited dgMain: TDBGridEh
      Top = 50
      Height = 274
      Font.Height = -13
      IndicatorParams.FillStyle = cfstGradientEh
      OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghRowHighlight, dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove]
      SearchPanel.Enabled = False
      SearchPanel.FilterEnabled = False
      TitleParams.FillStyle = cfstGradientEh
      Columns = <
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'reestr_num'
          Footers = <>
          Title.Caption = #1053#1086#1084#1077#1088
          Width = 117
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'reestr_date'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072' '#1088#1077#1077#1089#1090#1088#1072
          Width = 113
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'date_create'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072' '#1089#1086#1079#1076#1072#1085#1080#1103
          Width = 116
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'date_confirm'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072' '#1086#1090#1088#1072#1073#1086#1090#1082#1080
          Width = 149
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'date_export'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072' '#1074#1099#1075#1088#1091#1079#1082#1080
          Width = 120
        end>
    end
    inherited plTop: TPanel
      inherited sbDelete: TPngSpeedButton
        Left = 230
        ParentShowHint = False
        ShowHint = True
        ExplicitLeft = 565
      end
      inherited sbEdit: TPngSpeedButton
        Left = 187
        ParentShowHint = False
        ShowHint = True
        ExplicitLeft = 522
      end
      inherited sbAdd: TPngSpeedButton
        Left = 95
        ParentShowHint = False
        ShowHint = True
        ExplicitLeft = 430
      end
      inherited sbPrint: TPngSpeedButton
        Left = 316
        ParentShowHint = False
        ShowHint = True
        OnClick = sbPrintClick
        ExplicitLeft = 611
      end
      inherited sbExcel: TPngSpeedButton
        Left = 362
        ParentShowHint = False
        ShowHint = True
        ExplicitLeft = 657
      end
      inherited sbImport: TPngSpeedButton
        Visible = False
        ExplicitLeft = 384
      end
      inherited sbCopy: TPngSpeedButton
        Left = 141
        ParentShowHint = False
        ShowHint = True
        Visible = False
        ExplicitLeft = 476
      end
      object btFilter: TPngSpeedButton
        AlignWithMargins = True
        Left = 49
        Top = 2
        Width = 40
        Height = 27
        Hint = #1054#1090#1086#1073#1088#1072#1090#1100
        Margins.Top = 2
        Margins.Bottom = 2
        Align = alLeft
        Flat = True
        ParentShowHint = False
        ShowHint = True
        OnClick = btFilterClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
          F80000000970485973000000B1000000B101C62D498D0000001974455874536F
          667477617265007777772E696E6B73636170652E6F72679BEE3C1A0000024549
          44415478DAB5D65B884E5114C0F1352397342ED33C8B229749A4942697DCA2E1
          C1C8A5494432359E3C206986092552BC4821D71719452952E352424C9428F988
          3CF0E89EC865C67FB5D6D6E9CCFEBE7366C6ACFAF5F59DF3CD5E67AFBDF73A53
          267D1C65FE39083B313571AD587CC7515CF1EFB3311E0FF1089DB104BBD1847B
          F8919160144662025EE114D6F9BD97588307E9046DA8404D8E594FC453D4E3BC
          5FD38433D182119887BBC904B7FC734E8E04E3F01CAB702E75AF12EDF889C9F8
          1D129CC622FFE38F190936E208A6E071E4FE0AB4FA8CEE8404933CB32ED2027C
          2B32F85A1CC74DD44A6A413DB4D41FB01F3B923BA60E17701B8BA5EB626FC221
          1F7C193E97986501F7F581D25B7235CEE0129627AEEFC576BFD7805F1965D4C1
          DFEB83C6F6FC161C083514DBE3CF70D867D129D9F16FD3C4120C175BE8CD3828
          B6AFCF62345EE7183C9AA0C13F7501078B2DB296641F36F875DDDF6F7B9A2079
          0EB212E8C3AC8C0CDAEABFEB768266EC42153EFD8F047AED1DDE889D8B46B1C6
          5697B33CD104D73004D3FDFB529C103B34C7B04D8A1FBE5868D3D473521B12E8
          F4AAC51A59887E1820D69EA544696225D26DFD04F521C11E6CC550B146158BBC
          0906E28BD8FAB584040BBD4C4B70B91BA5888596F7A2584F6B0B09CAF1C29F7E
          1ABEF670705DC7762FAF76808EE4499E85EB626F29DDA2574B942B1DBA56DA20
          B567E9899F2BD666BABC7FB5FF9CC418B18656F0D914DB4115FED463D15FEC95
          B93E0C1E4B10CA351F33C45E40C37CCAB1F82376F80A3EE80D2D4BF20759FF41
          F43AFE02BE3B8A19CFBCBB1D0000000049454E44AE426082}
        ExplicitLeft = 430
        ExplicitTop = -3
        ExplicitHeight = 26
      end
      object sbExport: TPngSpeedButton
        Left = 273
        Top = 0
        Width = 40
        Height = 31
        Hint = #1069#1082#1089#1087#1086#1088#1090' '#1076#1072#1085#1085#1099#1093' '#1074' '#1055#1072#1088#1091#1089' 10'
        Margins.Top = 0
        Margins.Bottom = 5
        Align = alLeft
        Flat = True
        ParentShowHint = False
        ShowHint = True
        OnClick = sbExportClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
          F8000000017352474200AECE1CE9000000097048597300000EC300000EC301C7
          6FA864000001594944415478DAC595CF2B055114C7EF45292551A29485A46CD8
          4916FE066BE259F9AF448A850DFE09D95216D8A084283B0B3FDEF89CDE999C66
          DECCDC3B8FDEA94F4D77EE39DF3BDF3BF75CEFFE397CB704CA84938839B94963
          7002533AC99B847EB887391DBB8049F8D0F7EF3000B7B0988A6405CE60019EA1
          2740601A1E61084674FC05C6DB09782DFC0D13D02CB0C15A24CF7D70082B3A7E
          671CC8093C19AB424272B661138E61591758FA05A94052515C2CDC8355D8852D
          B882419B5F57C06BF135388075F7BBD1B2E9C39D08D8E2FBD030FB75AE73E6EB
          0A648B6F54591923105D3C4620A4786D8BE43FDF0958B9E4BFE973D426CFC265
          802DF61C459F0311B981AF0ABB73F99D1CB42281A4EC0B625B4536D205160A48
          27FC74E5CDAE5D4831691D0FD05B6491C4A96BF5F26CBB0E11686A6169F94B76
          D53646E108665CD8CDE532F3AE5DAB6DBFBA8AE4BA7775EEC7E8DAA5FF67F103
          BC3874199D93FC0F0000000049454E44AE426082}
        ExplicitLeft = 247
        ExplicitTop = 2
      end
    end
    object dgDetail: TDBGridEh
      Left = 0
      Top = 328
      Width = 934
      Height = 285
      Align = alBottom
      ColumnDefValues.Title.TitleButton = True
      ColumnDefValues.ToolTips = True
      Ctl3D = False
      DataSource = dsDetail
      DynProps = <>
      Flat = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Calibri'
      Font.Style = []
      GridLineParams.DataHorzColor = clSilver
      GridLineParams.DataVertColor = clSilver
      GridLineParams.VertEmptySpaceStyle = dessNonEh
      IndicatorOptions = [gioShowRowIndicatorEh, gioShowRecNoEh, gioShowRowselCheckboxesEh]
      IndicatorParams.HorzLineColor = clSilver
      IndicatorParams.VertLineColor = clSilver
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
      OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove]
      ParentCtl3D = False
      ParentFont = False
      ParentShowHint = False
      PopupMenu = pmDetail
      ReadOnly = True
      SearchPanel.FilterEnabled = False
      ShowHint = True
      SortLocal = True
      TabOrder = 3
      TitleParams.FillStyle = cfstGradientEh
      TitleParams.HorzLineColor = clSilver
      TitleParams.MultiTitle = True
      TitleParams.VertLineColor = clSilver
      OnDblClick = sbEditClick
      OnSortMarkingChanged = dgMainSortMarkingChanged
      Columns = <
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'doc_num'
          Footers = <>
          Title.Caption = #1053#1086#1084#1077#1088
          Width = 87
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'doc_date'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072
          Width = 80
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'account_number'
          Footers = <>
          Title.Caption = #1051#1080#1094#1077#1074#1086#1081' '#1089#1095#1077#1090
          Width = 131
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'service_name'
          Footers = <>
          Title.Caption = #1059#1089#1083#1091#1075#1072
          Width = 122
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'kbk'
          Footers = <>
          Title.Caption = #1050#1041#1050
          Width = 153
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'person_name'
          Footers = <>
          Title.Caption = #1054#1090#1074#1077#1090#1095#1080#1082
          Width = 224
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'note'
          Footers = <>
          Title.Caption = #1055#1086#1089#1090#1072#1085#1086#1074#1083#1077#1085#1080#1077
          Width = 128
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DisplayFormat = '### ### ##0.00'
          DynProps = <>
          EditButtons = <>
          FieldName = 'summa'
          Footers = <>
          Title.Caption = #1057#1091#1084#1084#1072
          Width = 94
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'uin'
          Footers = <>
          Title.Caption = #1059#1048#1053
          Width = 196
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'subdiv_name'
          Footers = <>
          Title.Caption = #1055#1086#1076#1088#1072#1079#1076#1077#1083#1077#1085#1080#1077
          Width = 214
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'oktmo'
          Footers = <>
          Title.Caption = #1054#1050#1058#1052#1054
          Width = 58
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'date_create'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072'/'#1074#1088#1077#1084#1103' '#1089#1086#1079#1076#1072#1085#1080#1103
          Width = 114
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'register_cancelled'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072'/'#1074#1088#1077#1084#1103' '#1086#1090#1084#1077#1085#1099' '#1088#1077#1075#1080#1089#1090#1088#1072#1094#1080#1080
          Width = 129
        end>
      object RowDetailData: TRowDetailPanelControlEh
      end
    end
    object plFilter: TPanel
      Left = 0
      Top = 31
      Width = 934
      Height = 19
      Align = alTop
      BevelOuter = bvNone
      Color = 15790320
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 9191194
      Font.Height = -12
      Font.Name = 'Calibri'
      Font.Style = []
      ParentBackground = False
      ParentFont = False
      TabOrder = 4
      StyleElements = [seFont, seBorder]
      ExplicitWidth = 932
    end
  end
  inherited FDUpdateSet: TFDUpdateSQL
    InsertSQL.Strings = (
      'INSERT INTO public.reestr'
      '(reestr_num, reestr_date)'
      'VALUES( :reestr_num, :reestr_date)'
      'RETURNING id;')
    FetchRowSQL.Strings = (
      'select * from reestr'
      'where id = :id')
  end
  inherited FDDataQr: TFDQuery
    BeforeOpen = FDDataQrBeforeOpen
    AfterOpen = FDDataQrAfterScroll
    AfterScroll = FDDataQrAfterScroll
    SQL.Strings = (
      'select r.*, '
      'to_char(r.reestr_date, '#39'dd.mm.yyyy'#39') as reestr_date_str '
      'from reestr r'
      'where r.reestr_date >= :date_begin '
      'and r.reestr_date <= :date_end '
      'order by r.reestr_date, r.reestr_num;')
    ParamData = <
      item
        Name = 'DATE_BEGIN'
        DataType = ftDateTime
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'DATE_END'
        DataType = ftDateTime
        ParamType = ptInput
        Value = Null
      end>
  end
  inherited pmMain: TPopupMenu
    object N2: TMenuItem [3]
      Caption = '-'
    end
    object N3: TMenuItem [4]
      Caption = #1054#1090#1088#1072#1073#1086#1090#1072#1090#1100
      OnClick = N3Click
    end
    object N4: TMenuItem [5]
      Caption = #1057#1085#1103#1090#1100' '#1086#1090#1088#1072#1073#1086#1090#1082#1091
      OnClick = N4Click
    end
  end
  inherited qrAux: TFDQuery
    SQL.Strings = (
      'select f_getnextdocnum(:docdate);')
    ParamData = <
      item
        Name = 'DOCDATE'
        DataType = ftDate
        ParamType = ptInput
        Value = Null
      end>
  end
  object FDDetailQuery: TFDQuery [7]
    BeforeOpen = FDDetailQueryBeforeOpen
    Connection = dm.FDConnMain
    UpdateOptions.AssignedValues = [uvEInsert, uvRefreshMode, uvRefreshDelete]
    UpdateOptions.KeyFields = 'id'
    SQL.Strings = (
      'select  '
      'j.*,'
      'p.code as person_code,'
      's.code as service_code,'
      's.name as service_name,'
      'sd.code as subdiv_code,'
      'sd.name as subdiv_name,'
      's.KBK  as kbk,'
      'a.code as account_number,'
      'p.type as person_type,'
      'p.name as person_name,'
      'p.jtcode,'
      'p.inn,'
      'p.snils,'
      'p.surname,'
      'p.firstname,'
      'p.secondname,'
      'p.sex,'
      'p.birthdate,'
      'p.address,'
      'p.doctype,'
      'p.docnumber,'
      'p.docseria,'
      'p.docwhen,'
      'p.docwho,'
      'p.contact_phone,'
      'p.region_id,'
      'p.area_id,'
      'p.town_id,'
      'p.street_id,'
      'p.house,'
      'p.building,'
      'p.flat,'
      'p.postindex'
      'from jdmain j'
      'left outer join persons p on (p.id  = j.person_id)'
      'left outer join services s on (s.id  = j.service_id )'
      'left outer join subdivs sd on (sd.id  = j.subdiv_id )'
      'left outer join accounts a on (a.id  = j.account_id )'
      'where reestr_id = :reestr_id'
      'order by j.doc_date;')
    Left = 320
    Top = 468
    ParamData = <
      item
        Name = 'REESTR_ID'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
  end
  object dsDetail: TDataSource [8]
    DataSet = FDDetailQuery
    Left = 400
    Top = 464
  end
  object FDGetLastNumber: TFDQuery [9]
    Connection = dm.FDConnMain
    UpdateOptions.AssignedValues = [uvEInsert, uvRefreshMode, uvRefreshDelete]
    UpdateOptions.KeyFields = 'id'
    SQL.Strings = (
      'select * from reestr'
      'where reestr_date >= :date_begin '
      'and reestr_date <= :date_end '
      'order by reestr_date, reestr_num;')
    Left = 392
    Top = 116
    ParamData = <
      item
        Name = 'DATE_BEGIN'
        DataType = ftDate
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'DATE_END'
        DataType = ftDate
        ParamType = ptInput
        Value = Null
      end>
  end
  object pmDetail: TPopupMenu [10]
    Left = 496
    Top = 468
    object N1: TMenuItem
      Caption = #1048#1089#1082#1083#1102#1095#1080#1090#1100' '#1080#1079' '#1088#1077#1077#1089#1090#1088#1072
      OnClick = N1Click
    end
    object MenuItem4: TMenuItem
      Caption = '-'
    end
    object MenuItem5: TMenuItem
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100
      ShortCut = 116
      OnClick = MenuItem5Click
    end
  end
end
