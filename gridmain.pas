unit gridmain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, FireDAC.Stan.Intf, FireDAC.Stan.Param,
  FireDAC.Phys.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Vcl.ExtCtrls, DBCtrlsEh,
  Vcl.StdCtrls, Vcl.DBCtrls, Vcl.Menus, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, Vcl.Buttons, PngSpeedButton, EhLibVCL, GridsEh,
  DBAxisGridsEh, DBGridEh, functions, EXLReportExcelTLB, EXLReportBand,
  EXLReport, Vcl.Mask, MemTableDataEh, MemTableEh, Xml.xmldom, Xml.XMLIntf,
  Xml.XMLDoc, StrUtils, System.Types;

type
  TFormGridMain = class(TFormGrid)
    plLeft: TPanel;
    lbFilter: TLabel;
    grpPeriod: TGroupBox;
    sbChangePeriod: TSpeedButton;
    grpServices: TGroupBox;
    btApply: TButton;
    rgStates: TDBRadioGroupEh;
    Splitter1: TSplitter;
    sbConfirm: TPngSpeedButton;
    meServices: TMemo;
    dtStart: TDBDateTimeEditEh;
    dtEnd: TDBDateTimeEditEh;
    sbExport: TPngSpeedButton;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    edReestr: TDBEditEh;
    procedure sbChangePeriodClick(Sender: TObject);
    procedure sbPrintClick(Sender: TObject);
    procedure meServicesChange(Sender: TObject);
    procedure btApplyClick(Sender: TObject);
    procedure sbConfirmClick(Sender: TObject);
    procedure FDDataQrBeforeOpen(DataSet: TDataSet);
    procedure sbExportClick(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure FDDataQrAfterEdit(DataSet: TDataSet);
    procedure FDDataQrAfterRefresh(DataSet: TDataSet);
    procedure FDDataQrAfterScroll(DataSet: TDataSet);
    procedure FDDataQrAfterCancel(DataSet: TDataSet);
    procedure FDDataQrAfterPost(DataSet: TDataSet);
    procedure FDDataQrBeforePost(DataSet: TDataSet);
    procedure FDDataQrAfterInsert(DataSet: TDataSet);
    procedure sbDeleteClick(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure N5Click(Sender: TObject);
    procedure edReestrEditButtons0Click(Sender: TObject; var Handled: Boolean);
    procedure edReestrEditButtons1Click(Sender: TObject; var Handled: Boolean);
  private
    //
  public
    procedure Init; override;
    procedure AfterCopyRecord; override;
  end;

var
  FormGridMain: TFormGridMain;

implementation

{$R *.dfm}

uses editjd, setperiod, selecttemplate, register, dmu, exportJD, Settings, EditJD2, reestrs;


procedure TFormGridMain.btApplyClick(Sender: TObject);
begin
  FDDataQr.Close;
  FDDataQr.Open;
  btApply.Enabled := false;
  lbFilter.Show;
end;

procedure TFormGridMain.edReestrEditButtons0Click(Sender: TObject; var Handled: Boolean);
var formReestr: TFormReestrs;
begin
   Application.CreateForm(TFormReestrs, formReestrs);
   formReestrs.Init;
   formReestrs.plBottom.Show;
   formReestrs.ShowModal;
   if formReestrs.ModalResult = mrOk then
   begin
    edReestr.Text := formReestrs.FDDataQr.FieldByName('reestr_num').AsString+' �� '+formReestrs.FDDataQr.FieldByName('reestr_date_str').AsString;
    edReestr.Tag := formReestrs.FDDataQr.FieldByName('id').AsInteger;
   end;
end;

procedure TFormGridMain.edReestrEditButtons1Click(Sender: TObject;
  var Handled: Boolean);
begin
  edReestr.Text := '';
  edReestr.Tag := 0;
end;

procedure TFormGridMain.FDDataQrAfterCancel(DataSet: TDataSet);
begin
  inherited;
  FDDataQr.UpdateOptions.ReadOnly := false;
end;

procedure TFormGridMain.FDDataQrAfterEdit(DataSet: TDataSet);
begin
  inherited;
  FDDataQr.UpdateOptions.ReadOnly := false;
  if FDDataQr.FieldByName('doc_date').Value <> null then
  begin
    //FDDataQr.UpdateOptions.ReadOnly := true;
   self.FormEditLocal := FormEditJD2;
  end;
end;

procedure TFormGridMain.FDDataQrAfterInsert(DataSet: TDataSet);
begin
  FDDataQr.FieldByName('account_id').Value := FormSettings.default_account_id;
  FDDataQr.FieldByName('subdiv_id').Value := FormSettings.default_subdiv_id;
end;

procedure TFormGridMain.FDDataQrAfterPost(DataSet: TDataSet);
begin
  inherited;
  FDDataQr.UpdateOptions.ReadOnly := false;
end;

procedure TFormGridMain.FDDataQrAfterRefresh(DataSet: TDataSet);
begin
  inherited;
  FDDataQr.UpdateOptions.ReadOnly := false;
end;

procedure TFormGridMain.FDDataQrAfterScroll(DataSet: TDataSet);
begin
  inherited;
  FDDataQr.UpdateOptions.ReadOnly := false;
end;

procedure TFormGridMain.FDDataQrBeforeOpen(DataSet: TDataSet);
begin
  FDDataQr.Params.ParamByName('nIsConfirmed').Value := rgStates.ItemIndex;
  FDDataQr.Params.ParamByName('sServices').Value := meServices.Text;
  FDDataQr.Params.ParamByName('dStart').DataType :=  TFieldType.ftDateTime;
  FDDataQr.Params.ParamByName('dEnd').DataType :=  TFieldType.ftDateTime;
  FDDataQr.Params.ParamByName('dStart').Value := self.dtStart.Value;
  FDDataQr.Params.ParamByName('dEnd').Value := self.dtEnd.Value;
  FDDataQr.Params.ParamByName('nReestrId').Value := self.edReestr.Tag;
end;

procedure TFormGridMain.FDDataQrBeforePost(DataSet: TDataSet);
begin
  inherited;
  if FDDataQr.UpdateOptions.ReadOnly then
  begin
    FDDataQr.Cancel;
  end;
end;

procedure TFormGridMain.Init;
begin
  self.FormEditLocal := FormEditJD;
  self.dtStart.Value := now()-5;
  btApplyClick(nil);
end;

procedure TFormGridMain.AfterCopyRecord;
begin
  FDDataQr.FieldByName('UIN').Value := null;
  FDDataQr.FieldByName('doc_num').Value := null;
  FDDataQr.FieldByName('doc_date').Value := null;
  FDDataQr.FieldByName('register_cancelled').Value := null;
end;


procedure TFormGridMain.meServicesChange(Sender: TObject);
begin
  btApply.Enabled := true;
  lbFilter.Hide;
end;

procedure TFormGridMain.N2Click(Sender: TObject);
begin
  if fQYN('������������� ������ �������� �����������?') then
  begin
    dm.qrCommon.Close;
    dm.qrCommon.SQL.Clear;
    dm.qrCommon.SQL.Add('update jdmain set doc_date = null, doc_num = null, uin = null, register_cancelled = now()');
    dm.qrCommon.SQL.Add('where id='+FDDataQr.FieldByName('id').AsString);
    dm.qrCommon.ExecSQL;
    FDDataQr.RefreshRecord;
  end;
end;

procedure TFormGridMain.N4Click(Sender: TObject);
var formReestr: TFormReestrs;
begin
   Application.CreateForm(TFormReestrs, formReestrs);
   formReestrs.Init;
   formReestrs.plBottom.Show;
   formReestrs.ShowModal;
   if formReestrs.ModalResult = mrOk then
   begin

     formReestrs.FDDataQr.RefreshRecord();
     if (formReestrs.FDDataQr.FieldByName('date_confirm').Value <> null) then
     begin
       ShowMessage('���������� �������� ������ � ������������ ������.');
       exit;
     end;

     GetIds(dgMain, FDDataQr);
     dm.qrReestrOp.Close;
     dm.qrReestrOp.Params.ParamByName('sIds').Value := Ids;
     dm.qrReestrOp.Params.ParamByName('nReestrId').Value := 0;
     dm.qrReestrOp.Params.ParamByName('nCheckSign').Value := 1;
     dm.qrReestrOp.Open;
     if dm.qrReestrOp.Fields[0].AsInteger>0 then
     begin
      ShowMessage('�� ��� ��������� ������ ����� ���� �������� � ������. �������� ��������.');
      exit;
     end;
     dm.qrReestrOp.Close;
     dm.qrReestrOp.Params.ParamByName('sIds').Value := Ids;
     dm.qrReestrOp.Params.ParamByName('nReestrId').Value := formReestrs.FDDataQr.FieldByName('id').AsInteger;
     dm.qrReestrOp.Params.ParamByName('nCheckSign').Value := 0;
     try
       dm.qrReestrOp.Open;
       FDDataQr.Close;
       FDDataQr.Open;
       ShowMessage('��������� ������� �������� � ��������� ������.');
     except
      on E: Exception do
        ShowMessage(E.Message);
     end;
   end;
   dgMain.Selection.Clear;
end;

procedure TFormGridMain.N5Click(Sender: TObject);
var a_ids: TStringDynArray;
begin
  if fQYN('��������� ��������� ������ �� ��������?') then
  begin
    GetIds(dgMain, FDDataQr);
    a_ids := SplitString(Ids, ',');
    dm.qrReestrOp.Close;
    dm.qrReestrOp.Params.ParamByName('sIds').Value := Ids;
    dm.qrReestrOp.Params.ParamByName('nReestrId').Value := 0;
    dm.qrReestrOp.Params.ParamByName('nCheckSign').Value := 0;
    try
     dm.qrReestrOp.Open;
     if dm.qrReestrOp.Fields[0].AsInteger=Length(a_ids) then
     begin
      ShowMessage('��������� ������� ��������� �� ��������.');
     end else
     if dm.qrReestrOp.Fields[0].AsInteger=0 then
     begin
      ShowMessage('��������� �� ���� ��������� �� ��������.');
     end else
     if dm.qrReestrOp.Fields[0].AsInteger<Length(a_ids) then
     begin
      ShowMessage('��������� �������� ��������� �� ��������.');
     end;
     FDDataQr.Close;
     FDDataQr.Open;
    except
    on E: Exception do
      ShowMessage(E.Message);
    end;
  end;
  dgMain.Selection.Clear;
end;

procedure TFormGridMain.sbChangePeriodClick(Sender: TObject);
begin

  FormSetPeriod.ShowModal;
  if FormSetPeriod.ModalResult = mrOk then
  begin
    dtStart.Value := FormSetPeriod.dtStart.Value;
    dtEnd.Value := FormSetPeriod.dtEnd.Value;
  end;
end;


procedure TFormGridMain.sbConfirmClick(Sender: TObject);
var docnum: string; ordnum: integer;
begin

  if FDDataQr.FieldByName('doc_num').AsString <> '' then
  begin
    ShowMessage('�������� ��� ���������������.');
    exit;
  end;

  FormRegister.dtRegister.Value := now();
  FormRegister.ShowModal;
  if FormRegister.ModalResult = mrOk then
  begin

    qrAux.Close;
    qrAux.Params.ParamByName('DOCDATE').Value := FormRegister.dtRegister.Value;
    qrAux.Open;

    ordnum := qrAux.Fields[0].AsInteger+1;

    docnum := trim(FormSettings.jdnumber)+RightStr('00'+FormSettings.jpnumber,2)+RightStr('0000'+IntToStr(ordnum),4);

    dm.qrCommon.Close;
    dm.qrCommon.SQL.Clear;
    dm.qrCommon.SQL.Add('update jdmain set doc_date = :docdate, doc_num = :doc_num, docdaynum = :docdaynum, ');
    dm.qrCommon.SQL.Add('uin = :uin, register_cancelled = null');
    dm.qrCommon.SQL.Add('where id='+FDDataQr.FieldByName('id').AsString);
    dm.qrCommon.Params.ParamByName('uin').Value := CreateUIN(FormSettings.urn, docnum, FormRegister.dtRegister.Value);
    dm.qrCommon.Params.ParamByName('doc_num').Value := docnum;;
    dm.qrCommon.Params.ParamByName('docdaynum').Value := ordnum;
    dm.qrCommon.Params.ParamByName('docdate').Value := FormRegister.dtRegister.Value;
    dm.qrCommon.ExecSQL;

    FDDataQr.RefreshRecord;

  end;
end;

procedure TFormGridMain.sbDeleteClick(Sender: TObject);
begin
  if FDDataQr.FieldByName('doc_date').Value <> null then
  begin
    exit;
  end;
  inherited;
end;

procedure TFormGridMain.sbExportClick(Sender: TObject);
begin
  FormExportJD.meLog.Lines.Clear;
  FormExportJD.Grid := dgMain;
  FormExportJD.Query := FDDataQr;
  FormExportJD.ShowModal;
end;

procedure TFormGridMain.sbPrintClick(Sender: TObject);
begin
  unit_num1 := 0;
  unit_num2 := 1;
  PrintReport;
end;

end.

