inherited FormEditCodeName: TFormEditCodeName
  Caption = #1044#1086#1073#1072#1074#1080#1090#1100'/'#1080#1079#1084#1077#1085#1080#1090#1100
  ClientHeight = 185
  ClientWidth = 480
  ParentFont = True
  ExplicitWidth = 494
  ExplicitHeight = 224
  TextHeight = 15
  inherited plBottom: TPanel
    Top = 144
    Width = 480
    ExplicitTop = 144
    ExplicitWidth = 478
    inherited btnCancel: TButton
      Left = 364
      ExplicitLeft = 362
    end
    inherited btnOk: TButton
      Left = 245
      ExplicitLeft = 243
    end
  end
  object edCode: TDBEditEh [1]
    Left = 8
    Top = 30
    Width = 241
    Height = 27
    ControlLabel.Width = 24
    ControlLabel.Height = 18
    ControlLabel.Caption = #1050#1086#1076
    ControlLabel.Font.Charset = DEFAULT_CHARSET
    ControlLabel.Font.Color = clWindowText
    ControlLabel.Font.Height = -15
    ControlLabel.Font.Name = 'Calibri'
    ControlLabel.Font.Style = []
    ControlLabel.ParentFont = False
    ControlLabel.Visible = True
    DataField = 'code'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    Visible = True
  end
  object edName: TDBEditEh [2]
    Left = 8
    Top = 93
    Width = 457
    Height = 27
    ControlLabel.Width = 96
    ControlLabel.Height = 18
    ControlLabel.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
    ControlLabel.Font.Charset = DEFAULT_CHARSET
    ControlLabel.Font.Color = clWindowText
    ControlLabel.Font.Height = -15
    ControlLabel.Font.Name = 'Calibri'
    ControlLabel.Font.Style = []
    ControlLabel.ParentFont = False
    ControlLabel.Visible = True
    DataField = 'name'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    Visible = True
  end
  inherited dsLocal: TDataSource
    Left = 72
    Top = 136
  end
  inherited qrAux: TFDQuery
    Left = 24
    Top = 128
  end
end
