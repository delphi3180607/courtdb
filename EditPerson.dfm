inherited FormEditPerson: TFormEditPerson
  Caption = #1050#1086#1085#1090#1088#1072#1075#1077#1085#1090
  ClientHeight = 617
  ClientWidth = 552
  ParentFont = False
  Font.Height = -15
  OnShow = FormShow
  ExplicitWidth = 566
  ExplicitHeight = 656
  TextHeight = 20
  inherited plBottom: TPanel
    Top = 576
    Width = 552
    TabOrder = 7
    ExplicitTop = 576
    ExplicitWidth = 550
    inherited btnCancel: TButton
      Left = 436
      ExplicitLeft = 434
    end
    inherited btnOk: TButton
      Left = 317
      ParentFont = True
      OnClick = btnOkClick
      ExplicitLeft = 315
    end
  end
  inherited edCode: TDBEditEh
    Width = 273
    Height = 28
    Color = 14875388
    ControlLabel.Width = 170
    ControlLabel.Height = 19
    ControlLabel.Caption = #1050#1086#1088#1086#1090#1082#1086#1077' '#1085#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
    ControlLabel.Font.Height = -16
    ControlLabel.ExplicitLeft = 8
    ControlLabel.ExplicitTop = 8
    ControlLabel.ExplicitWidth = 170
    ControlLabel.ExplicitHeight = 19
    ParentFont = True
    StyleElements = [seFont, seBorder]
    TabOrder = 0
    ExplicitWidth = 273
    ExplicitHeight = 28
  end
  inherited edName: TDBEditEh
    Top = 83
    Width = 534
    Height = 28
    Color = 14875388
    ControlLabel.Width = 105
    ControlLabel.Height = 19
    ControlLabel.Font.Height = -16
    ControlLabel.ExplicitLeft = 8
    ControlLabel.ExplicitTop = 61
    ControlLabel.ExplicitWidth = 105
    ControlLabel.ExplicitHeight = 19
    ParentFont = True
    StyleElements = [seFont, seBorder]
    OnChange = edNameChange
    ExplicitTop = 83
    ExplicitWidth = 534
    ExplicitHeight = 28
  end
  object edINN: TDBEditEh [3]
    Left = 301
    Top = 30
    Width = 241
    Height = 28
    Color = 14875388
    ControlLabel.Width = 30
    ControlLabel.Height = 19
    ControlLabel.Caption = #1048#1053#1053
    ControlLabel.Font.Charset = DEFAULT_CHARSET
    ControlLabel.Font.Color = clWindowText
    ControlLabel.Font.Height = -16
    ControlLabel.Font.Name = 'Calibri'
    ControlLabel.Font.Style = []
    ControlLabel.ParentFont = False
    ControlLabel.Visible = True
    DataField = 'INN'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    MaxLength = 12
    StyleElements = [seFont, seBorder]
    TabOrder = 1
    Visible = True
    OnKeyPress = edINNKeyPress
  end
  object pcVars: TPageControl [4]
    Left = 8
    Top = 240
    Width = 534
    Height = 323
    ActivePage = TabSheet2
    TabOrder = 6
    object TabSheet1: TTabSheet
      Caption = 'TabSheet1'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Calibri'
      Font.Style = []
      ParentFont = False
      TabVisible = False
      object edDocSeria: TDBEditEh
        Left = 165
        Top = 229
        Width = 102
        Height = 27
        ControlLabel.Width = 42
        ControlLabel.Height = 19
        ControlLabel.Caption = #1057#1077#1088#1080#1103
        ControlLabel.Font.Charset = DEFAULT_CHARSET
        ControlLabel.Font.Color = clWindowText
        ControlLabel.Font.Height = -16
        ControlLabel.Font.Name = 'Calibri'
        ControlLabel.Font.Style = []
        ControlLabel.ParentFont = False
        ControlLabel.Visible = True
        DataField = 'docseria'
        DataSource = dsLocal
        DynProps = <>
        EditButtons = <>
        MaxLength = 5
        TabOrder = 7
        Visible = True
      end
      object edDocNumber: TDBEditEh
        Left = 283
        Top = 229
        Width = 94
        Height = 27
        ControlLabel.Width = 45
        ControlLabel.Height = 19
        ControlLabel.Caption = #1053#1086#1084#1077#1088
        ControlLabel.Font.Charset = DEFAULT_CHARSET
        ControlLabel.Font.Color = clWindowText
        ControlLabel.Font.Height = -16
        ControlLabel.Font.Name = 'Calibri'
        ControlLabel.Font.Style = []
        ControlLabel.ParentFont = False
        ControlLabel.Visible = True
        DataField = 'docnumber'
        DataSource = dsLocal
        DynProps = <>
        EditButtons = <>
        MaxLength = 8
        TabOrder = 8
        Visible = True
        OnKeyPress = edDocNumberKeyPress
      end
      object dtDocWhen: TDBDateTimeEditEh
        Left = 393
        Top = 229
        Width = 124
        Height = 27
        ControlLabel.Width = 46
        ControlLabel.Height = 19
        ControlLabel.Caption = #1042#1099#1076#1072#1085
        ControlLabel.Visible = True
        DataField = 'docwhen'
        DataSource = dsLocal
        DynProps = <>
        EditButtons = <>
        Kind = dtkDateEh
        TabOrder = 9
        Visible = True
      end
      object edDocWho: TDBEditEh
        Left = 7
        Top = 280
        Width = 510
        Height = 27
        ControlLabel.Width = 77
        ControlLabel.Height = 19
        ControlLabel.Caption = #1050#1077#1084' '#1074#1099#1076#1072#1085
        ControlLabel.Font.Charset = DEFAULT_CHARSET
        ControlLabel.Font.Color = clWindowText
        ControlLabel.Font.Height = -16
        ControlLabel.Font.Name = 'Calibri'
        ControlLabel.Font.Style = []
        ControlLabel.ParentFont = False
        ControlLabel.Visible = True
        DataField = 'docwho'
        DataSource = dsLocal
        DynProps = <>
        EditButtons = <>
        TabOrder = 10
        Visible = True
      end
      object dtBirthDate: TDBDateTimeEditEh
        Left = 7
        Top = 176
        Width = 124
        Height = 27
        ControlLabel.Width = 106
        ControlLabel.Height = 19
        ControlLabel.Caption = #1044#1072#1090#1072' '#1088#1086#1078#1076#1077#1085#1080#1103
        ControlLabel.Visible = True
        DataField = 'birthdate'
        DataSource = dsLocal
        DynProps = <>
        EditButtons = <>
        Kind = dtkDateEh
        TabOrder = 3
        Visible = True
      end
      object edSNILS: TDBEditEh
        Left = 283
        Top = 176
        Width = 234
        Height = 27
        Color = 14875388
        ControlLabel.Width = 48
        ControlLabel.Height = 19
        ControlLabel.Caption = #1057#1053#1048#1051#1057
        ControlLabel.Font.Charset = DEFAULT_CHARSET
        ControlLabel.Font.Color = clWindowText
        ControlLabel.Font.Height = -16
        ControlLabel.Font.Name = 'Calibri'
        ControlLabel.Font.Style = []
        ControlLabel.ParentFont = False
        ControlLabel.Visible = True
        DataField = 'snils'
        DataSource = dsLocal
        DynProps = <>
        EditButtons = <>
        MaxLength = 14
        TabOrder = 5
        Visible = True
        OnKeyPress = edSNILSKeyPress
      end
      object cbSex: TDBComboBoxEh
        Left = 142
        Top = 176
        Width = 130
        Height = 27
        ControlLabel.Width = 26
        ControlLabel.Height = 19
        ControlLabel.Caption = #1055#1086#1083
        ControlLabel.Font.Charset = DEFAULT_CHARSET
        ControlLabel.Font.Color = clWindowText
        ControlLabel.Font.Height = -16
        ControlLabel.Font.Name = 'Calibri'
        ControlLabel.Font.Style = []
        ControlLabel.ParentFont = False
        ControlLabel.Visible = True
        DataField = 'sex'
        DataSource = dsLocal
        DynProps = <>
        EditButtons = <>
        Items.Strings = (
          #1052#1091#1078#1089#1082#1086#1081
          #1046#1077#1085#1089#1082#1080#1081)
        KeyItems.Strings = (
          '0'
          '1')
        TabOrder = 4
        Visible = True
      end
      object edSurName: TDBEditEh
        Left = 7
        Top = 24
        Width = 510
        Height = 27
        Color = 14875388
        ControlLabel.Width = 64
        ControlLabel.Height = 19
        ControlLabel.Caption = #1060#1072#1084#1080#1083#1080#1103
        ControlLabel.Font.Charset = DEFAULT_CHARSET
        ControlLabel.Font.Color = clWindowText
        ControlLabel.Font.Height = -16
        ControlLabel.Font.Name = 'Calibri'
        ControlLabel.Font.Style = []
        ControlLabel.ParentFont = False
        ControlLabel.Visible = True
        DataField = 'surname'
        DataSource = dsLocal
        DynProps = <>
        EditButtons = <>
        StyleElements = [seFont, seBorder]
        TabOrder = 0
        Visible = True
        OnChange = edSurNameChange
      end
      object edFirstName: TDBEditEh
        Left = 7
        Top = 72
        Width = 510
        Height = 27
        Color = 14875388
        ControlLabel.Width = 29
        ControlLabel.Height = 19
        ControlLabel.Caption = #1048#1084#1103
        ControlLabel.Font.Charset = DEFAULT_CHARSET
        ControlLabel.Font.Color = clWindowText
        ControlLabel.Font.Height = -16
        ControlLabel.Font.Name = 'Calibri'
        ControlLabel.Font.Style = []
        ControlLabel.ParentFont = False
        ControlLabel.Visible = True
        DataField = 'firstname'
        DataSource = dsLocal
        DynProps = <>
        EditButtons = <>
        StyleElements = [seFont, seBorder]
        TabOrder = 1
        Visible = True
        OnChange = edSurNameChange
      end
      object edSecondName: TDBEditEh
        Left = 7
        Top = 123
        Width = 510
        Height = 27
        Color = 14875388
        ControlLabel.Width = 62
        ControlLabel.Height = 19
        ControlLabel.Caption = #1054#1090#1095#1077#1089#1090#1074#1086
        ControlLabel.Font.Charset = DEFAULT_CHARSET
        ControlLabel.Font.Color = clWindowText
        ControlLabel.Font.Height = -16
        ControlLabel.Font.Name = 'Calibri'
        ControlLabel.Font.Style = []
        ControlLabel.ParentFont = False
        ControlLabel.Visible = True
        DataField = 'secondname'
        DataSource = dsLocal
        DynProps = <>
        EditButtons = <>
        StyleElements = [seFont, seBorder]
        TabOrder = 2
        Visible = True
        OnChange = edSurNameChange
      end
      object cbDocType: TDBComboBoxEh
        Left = 7
        Top = 229
        Width = 144
        Height = 27
        ControlLabel.Width = 101
        ControlLabel.Height = 19
        ControlLabel.Caption = #1058#1080#1087' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
        ControlLabel.Visible = True
        DataField = 'doctype'
        DataSource = dsLocal
        DynProps = <>
        EditButtons = <>
        Items.Strings = (
          #1055#1072#1089#1087#1086#1088#1090
          #1042#1086#1077#1085#1085#1099#1081' '#1073#1080#1083#1077#1090)
        TabOrder = 6
        Visible = True
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'TabSheet2'
      ImageIndex = 1
      TabVisible = False
      object edJTCode: TDBEditEh
        Left = 10
        Top = 26
        Width = 212
        Height = 28
        ControlLabel.Width = 95
        ControlLabel.Height = 15
        ControlLabel.Caption = #1055#1088#1072#1074#1086#1074#1072#1103' '#1092#1086#1088#1084#1072
        ControlLabel.Font.Charset = DEFAULT_CHARSET
        ControlLabel.Font.Color = clWindowText
        ControlLabel.Font.Height = -13
        ControlLabel.Font.Name = 'Calibri'
        ControlLabel.Font.Style = []
        ControlLabel.ParentFont = False
        ControlLabel.Visible = True
        DataField = 'jtcode'
        DataSource = dsLocal
        DynProps = <>
        EditButtons = <>
        TabOrder = 0
        Visible = True
      end
    end
  end
  object edAddress: TDBEditEh [5]
    Left = 8
    Top = 188
    Width = 536
    Height = 45
    AutoSize = False
    ControlLabel.Width = 41
    ControlLabel.Height = 19
    ControlLabel.Caption = #1040#1076#1088#1077#1089
    ControlLabel.Font.Charset = DEFAULT_CHARSET
    ControlLabel.Font.Color = clWindowText
    ControlLabel.Font.Height = -16
    ControlLabel.Font.Name = 'Calibri'
    ControlLabel.Font.Style = []
    ControlLabel.ParentFont = False
    ControlLabel.Visible = True
    DataField = 'address'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
        Style = ebsEllipsisEh
        Width = 20
        OnClick = edAddressEditButtons0Click
      end>
    ReadOnly = True
    TabOrder = 5
    Visible = True
    WordWrap = True
  end
  object cbType: TDBComboBoxEh [6]
    Left = 8
    Top = 135
    Width = 257
    Height = 28
    ControlLabel.Width = 111
    ControlLabel.Height = 19
    ControlLabel.Caption = #1058#1080#1087' '#1082#1086#1085#1090#1088#1072#1075#1077#1085#1090#1072
    ControlLabel.Font.Charset = DEFAULT_CHARSET
    ControlLabel.Font.Color = clWindowText
    ControlLabel.Font.Height = -16
    ControlLabel.Font.Name = 'Calibri'
    ControlLabel.Font.Style = []
    ControlLabel.ParentFont = False
    ControlLabel.Visible = True
    DataField = 'type'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Items.Strings = (
      #1060#1080#1079#1080#1095#1077#1089#1082#1086#1077' '#1083#1080#1094#1086
      #1070#1088#1080#1076#1080#1095#1077#1089#1082#1086#1077' '#1083#1080#1094#1086)
    KeyItems.Strings = (
      '0'
      '1')
    TabOrder = 3
    Visible = True
    OnChange = rgPersonKindChange
  end
  object edContacts: TDBEditEh [7]
    Left = 299
    Top = 134
    Width = 243
    Height = 28
    ControlLabel.Width = 64
    ControlLabel.Height = 19
    ControlLabel.Caption = #1050#1086#1085#1090#1072#1082#1090#1099
    ControlLabel.Font.Charset = DEFAULT_CHARSET
    ControlLabel.Font.Color = clWindowText
    ControlLabel.Font.Height = -16
    ControlLabel.Font.Name = 'Calibri'
    ControlLabel.Font.Style = []
    ControlLabel.ParentFont = False
    ControlLabel.Visible = True
    DataField = 'contact_phone'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 4
    Visible = True
  end
  inherited dsLocal: TDataSource
    Left = 336
    Top = 233
  end
  inherited qrAux: TFDQuery
    Left = 288
    Top = 241
  end
end
