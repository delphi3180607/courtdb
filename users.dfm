inherited FormUsers: TFormUsers
  Caption = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1080
  ClientWidth = 920
  ExplicitWidth = 934
  TextHeight = 16
  inherited plMain: TPanel
    Width = 920
    inherited dgMain: TDBGridEh
      Columns = <
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'user_login'
          Footers = <>
          Title.Caption = #1051#1086#1075#1080#1085
          Width = 117
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'user_name'
          Footers = <>
          Title.Caption = #1048#1084#1103
          Width = 330
        end
        item
          CellButtons = <>
          Checkboxes = True
          DynProps = <>
          EditButtons = <>
          FieldName = 'isadmin'
          Footers = <>
          Title.Caption = #1040#1076#1084#1080#1085#1080#1089#1090#1088#1072#1090#1086#1088
          Width = 220
        end
        item
          CellButtons = <>
          Checkboxes = True
          DynProps = <>
          EditButtons = <>
          FieldName = 'isblocked'
          Footers = <>
          Title.Caption = #1047#1072#1073#1083#1086#1082#1080#1088#1086#1074#1072#1085
          Width = 150
        end>
    end
  end
  inherited FDUpdateSet: TFDUpdateSQL
    InsertSQL.Strings = (
      'INSERT INTO public.users'
      '(user_login, user_name, "password", isadmin, isblocked)'
      'select :user_login, :user_name, :password, :isadmin, false'
      'where not exists (select 1 from public.users where not isadmin)'
      'returning id;')
    ModifySQL.Strings = (
      'UPDATE public.users'
      'SET user_login = :user_login,'
      'user_name = :user_name,'
      '"password" = :password,'
      'isblocked = :isblocked'
      'WHERE not password_changed and id = :id;')
    DeleteSQL.Strings = (
      'delete from users where id = :id;')
    FetchRowSQL.Strings = (
      'select * from users where id  = :id;')
  end
  inherited FDDataQr: TFDQuery
    AfterEdit = FDDataQrAfterEdit
    SQL.Strings = (
      'select * from users order by user_login')
  end
end
