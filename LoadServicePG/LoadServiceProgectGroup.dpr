program LoadServiceProgectGroup;

uses
  Vcl.SvcMgr,
  Dialogs,
  servicemain in 'servicemain.pas' {JDLoadService: TService},
  main in 'main.pas' {FormMain},
  Vcl.Themes,
  Vcl.Styles,
  functions in 'functions.pas',
  QYN in 'QYN.pas' {FormQYN};

{$R *.RES}

begin
  // Windows 2003 Server requires StartServiceCtrlDispatcher to be
  // called before CoRegisterClassObject, which can be called indirectly
  // by Application.Initialize. TServiceApplication.DelayInitialize allows
  // Application.Initialize to be called from TService.Main (after
  // StartServiceCtrlDispatcher has been called).
  //
  // Delayed initialization of the Application object may affect
  // events which then occur prior to initialization, such as
  // TService.OnCreate. It is only recommended if the ServiceApplication
  // registers a class object with OLE and is intended for use with
  // Windows 2003 Server.
  //
  // Application.DelayInitialize := True;
  //

  if ParamStr(1)='-runasservice' then
  begin

    if not Application.DelayInitialize or Application.Installing then
      Application.Initialize;
  Application.CreateForm(TFormMain, FormMain);
      Application.CreateForm(TJDLoadService, JDLoadService);
      JDLoadService.Init;

      if not FormMain.TryOpenConnection then
      begin
        JDLoadService.AddRowLog('�� ������� ������������ � ���� ������ �����. ������ ����������.');
        JDLoadService.Timer1.Enabled := false;
        exit;
      end else
      begin
        JDLoadService.AddRowLog('����� ������.');
      end;

  end else
  begin
    Application.CreateForm(TFormQYN, FormQYN);
    Application.CreateForm(TJDLoadService, JDLoadService);
    Application.CreateForm(TFormMain, FormMain);
    FormMain.ShowModal;
  end;
  Application.Run;
end.
