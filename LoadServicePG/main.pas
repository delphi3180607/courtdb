unit main;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Mask, DBCtrlsEh, ADODB,
  Vcl.ExtCtrls, IniFiles, functions, servicemain, Vcl.ComCtrls, DBSQLLookUp,
  Data.DB, Data.SqlExpr, MemTableDataEh, MemTableEh, Vcl.Buttons;

type
  TFormMain = class(TForm)
    plBottom: TPanel;
    btCancel: TButton;
    btApply: TButton;
    btSaveAndClose: TButton;
    pcMain: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    edDir: TDBEditEh;
    Label1: TLabel;
    meFiles: TMemo;
    btLoad: TButton;
    meLog: TMemo;
    laSupplier: TDBSQLLookUp;
    lbMessage: TLabel;
    ConnMain: TADOConnection;
    ssSupplier: TADOLookUpSqlSet;
    meTemp: TMemTableEh;
    dsTemp: TDataSource;
    laCatalog: TDBSQLLookUp;
    ssCatalogs: TADOLookUpSqlSet;
    qrAux: TADOQuery;
    qrAux1: TADOQuery;
    qrDict: TADOQuery;
    laPersonCatalog: TDBSQLLookUp;
    laJPersonCatalog: TDBSQLLookUp;
    ssPersonCatalogs: TADOLookUpSqlSet;
    ssJPersonCatalogs: TADOLookUpSqlSet;
    luDocument: TDBSQLLookUp;
    ssDocument: TADOLookUpSqlSet;
    sd: TFileOpenDialog;
    plTop: TPanel;
    edConnString: TDBEditEh;
    sbCheck: TSpeedButton;
    procedure edDirEditButtons0Click(Sender: TObject; var Handled: Boolean);
    procedure btSaveAndCloseClick(Sender: TObject);
    procedure btCancelClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edDirChange(Sender: TObject);
    procedure btLoadClick(Sender: TObject);
    procedure btCheckClick(Sender: TObject);
    procedure edConnStringEditButtons0Click(Sender: TObject;
      var Handled: Boolean);
    procedure pcMainChange(Sender: TObject);
    procedure btApplyClick(Sender: TObject);
    procedure meFilesClick(Sender: TObject);
    procedure edDirEnter(Sender: TObject);
    procedure edDirExit(Sender: TObject);
  private
    procedure RefreshList;
  public
    procedure SetConnection;
    procedure SaveConfig;
    function TryOpenConnection: boolean;
  end;

var
  FormMain: TFormMain;

implementation

{$R *.dfm}

procedure TFormMain.btApplyClick(Sender: TObject);
begin
 SaveConfig;
end;

procedure TFormMain.btCancelClick(Sender: TObject);
begin
  ShowMessage('������������ �� ���������.');
end;

function TFormMain.TryOpenConnection: boolean;
var inf: TIniFile;
begin

  result := true;
  lbMessage.Caption := '';

  ConnMain.Connected := false;
  ConnMain.ConnectionString := edConnString.Text;

  try

    ConnMain.Open;

    if laSupplier.KeyValue=null then
    begin
      SetCurrentDir(ExtractFileDir(Application.ExeName));
      inf := TIniFile.Create(ExtractFileDir(Application.ExeName)+'/settings.ini');
      laSupplier.KeyValue := inf.ReadString('main','DefaultSupplier','');
      luDocument.KeyValue := inf.ReadString('main','DocumentCode','');
      laCatalog.KeyValue := inf.ReadString('main','DefaultCatalog','');
      laPersonCatalog.KeyValue := inf.ReadString('main','PersonCatalog','');
      laJPersonCatalog.KeyValue := inf.ReadString('main','JPersonCatalog','');
    end;

  except
    on E:Exception do
    begin
      lbMessage.Caption := '������: '+E.Message;
      result := false;
    end;
  end;

end;

procedure TFormMain.SetConnection;
var inf: TIniFile;
begin

 SetCurrentDir(ExtractFileDir(Application.ExeName));
 inf := TIniFile.Create(ExtractFileDir(Application.ExeName)+'/settings.ini');
 edConnString.Text := inf.ReadString('main','ConnectionString','');
 edDir.Text := inf.ReadString('main','UnloadFolder','');

end;



procedure TFormMain.btCheckClick(Sender: TObject);
begin

    if not TryOpenConnection then
    begin
      ShowMessage('�� ������� ������������ � ���� ������, ���������� �������� ��������� �����������.');
    end else
    begin
      ShowMessage('�������� �����������');
    end;

end;

procedure TFormMain.btLoadClick(Sender: TObject);
begin

  if fQYN('������������� ��������� ����� �� ���������� ��������?') then
  begin
    JDLoadService.catalog := laCatalog.Text;
    JDLoadService.supplier := laSupplier.Text;
    JDLoadService.document := luDocument.Text;
    JDLoadService.dirname := edDir.Text;
    JDLoadService.connStr := edConnString.Text;
    JDLoadService.LoadFilesFromUploadDir(edDir.Text);
    edDirChange(nil);
  end;

end;

procedure TFormMain.btSaveAndCloseClick(Sender: TObject);
begin
 SaveConfig;
end;


procedure TFormMain.SaveConfig;
var inf: TIniFile;
begin
 SetCurrentDir(ExtractFileDir(Application.ExeName));
 inf := TIniFile.Create(ExtractFileDir(Application.ExeName)+'/settings.ini');
 inf.WriteString('main','UnloadFolder',edDir.Text);
 inf.WriteString('main','ConnectionString',edConnString.Text);
 inf.WriteString('main','DefaultSupplier',VarToStr(laSupplier.KeyValue));
 inf.WriteString('main','DocumentCode',VarToStr(luDocument.KeyValue));
 inf.WriteString('main','DefaultCatalog',VarToStr(laCatalog.KeyValue));
 inf.WriteString('main','PersonCatalog',VarToStr(laPersonCatalog.KeyValue));
 inf.WriteString('main','JPersonCatalog',VarToStr(laJPersonCatalog.KeyValue));
end;

procedure TFormMain.edConnStringEditButtons0Click(Sender: TObject;
  var Handled: Boolean);
begin
  edConnString.Text := PromptDataSource(0,edConnString.Text);
end;

procedure TFormMain.edDirChange(Sender: TObject);
begin

  RefreshList;

end;

procedure TFormMain.RefreshList;
var NFound: integer; SR: TSearchRec;
begin
  meFiles.Lines.Clear;

  NFound := FindFirst(edDir.Text + '\*.xml', faAnyFile, SR);

  while NFound = 0 do
  begin
    meFiles.Lines.Add(SR.Name);
    NFound := FindNext(SR);
  end;

  FindClose(SR);
end;


procedure TFormMain.edDirEditButtons0Click(Sender: TObject; var Handled: Boolean);
begin
  RefreshList;
  if sd.Execute then
  begin
    edDir.Text := sd.FileName;
  end;
  RefreshList;
end;

procedure TFormMain.edDirEnter(Sender: TObject);
begin
  RefreshList;
end;

procedure TFormMain.edDirExit(Sender: TObject);
begin
  RefreshList;
end;

procedure TFormMain.FormShow(Sender: TObject);
begin
 SetConnection;
end;

procedure TFormMain.meFilesClick(Sender: TObject);
begin
  RefreshList;
end;

procedure TFormMain.pcMainChange(Sender: TObject);
begin

  if pcMain.ActivePageIndex = 0 then
  begin
    ConnMain.Close;
    exit;
  end;

  if not TryOpenConnection then
  begin
    ShowMessage('�� ������� ������������ � ���� ������.');
    pcMain.ActivePageIndex := 0;
    exit;
  end;

end;

end.
