object dm: Tdm
  Height = 405
  Width = 723
  object FDConnMain: TFDConnection
    LoginDialog = loginDialog
    LoginPrompt = False
    Left = 80
    Top = 48
  end
  object loginDialog: TFDGUIxLoginDialog
    Provider = 'Forms'
    Left = 72
    Top = 120
  end
  object ssAccounts: TDBXLookUpSqlSet
    TmplSql.Strings = (
      'select * from accounts '
      'where (code like '#39'%@pattern%'#39' or name like '#39'%@pattern%'#39')'
      'order by code')
    DownSql.Strings = (
      'select * from accounts order by code')
    InitSql.Strings = (
      'select * from accounts where id = @id')
    KeyName = 'id'
    DisplayName = 'code'
    SQLConnection = FDConnMain
    Left = 160
    Top = 48
  end
  object ssPersons: TDBXLookUpSqlSet
    TmplSql.Strings = (
      'select * from persons'
      'where (code like '#39'%@pattern%'#39' or name like '#39'%@pattern%'#39')'
      'order by code')
    DownSql.Strings = (
      'select * from persons order by code'
      '')
    InitSql.Strings = (
      'select * from persons where id = @id')
    KeyName = 'id'
    DisplayName = 'code'
    SQLConnection = FDConnMain
    Left = 224
    Top = 48
  end
  object ssServices: TDBXLookUpSqlSet
    TmplSql.Strings = (
      
        'select s.id, s.name||'#39', '#39'||COALESCE (s.kbk,'#39'???'#39') as snamekbk, s' +
        '.note from services s'
      'where (code like '#39'%@pattern%'#39' or name like '#39'%@pattern%'#39')'
      'order by code')
    DownSql.Strings = (
      
        'select s.id, s.name||'#39', '#39'||COALESCE (s.kbk,'#39'???'#39') as snamekbk, s' +
        '.note from services s'
      'order by code'
      '')
    InitSql.Strings = (
      
        'select s.id, s.name||'#39', '#39'||COALESCE (s.kbk,'#39'???'#39') as snamekbk, s' +
        '.note from services s'
      'where id = @id')
    KeyName = 'id'
    DisplayName = 'snamekbk'
    SQLConnection = FDConnMain
    Left = 296
    Top = 48
  end
  object qrCommon: TFDQuery
    Connection = FDConnMain
    UpdateOptions.AssignedValues = [uvEInsert, uvRefreshMode, uvRefreshDelete]
    UpdateOptions.KeyFields = 'id'
    SQL.Strings = (
      'select j.*, p.person_name , s.service_name from jdmain j'
      'left outer join persons p on (p.id  = j.person_id)'
      'left outer join services s on (s.id  = j.service_id )'
      'order by j.doc_date;')
    Left = 304
    Top = 124
  end
  object ssSubdivs: TDBXLookUpSqlSet
    TmplSql.Strings = (
      'select * from subdivs'
      'where (code like '#39'%@pattern%'#39' or name like '#39'%@pattern%'#39')'
      'order by code')
    DownSql.Strings = (
      'select * from subdivs order by code')
    InitSql.Strings = (
      'select * from subdivs where id = @id')
    KeyName = 'id'
    DisplayName = 'code'
    SQLConnection = FDConnMain
    Left = 152
    Top = 120
  end
  object qrReestrOp: TFDQuery
    Connection = FDConnMain
    UpdateOptions.AssignedValues = [uvEInsert, uvRefreshMode, uvRefreshDelete]
    UpdateOptions.KeyFields = 'id'
    SQL.Strings = (
      'select public.p_includeinreestr(:sIds, :nReestrId, :nCheckSign);')
    Left = 224
    Top = 116
    ParamData = <
      item
        Name = 'SIDS'
        DataType = ftString
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'NREESTRID'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'NCHECKSIGN'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
  end
  object qrUpdateDataBase: TFDQuery
    Connection = FDConnMain
    UpdateOptions.AssignedValues = [uvEInsert, uvRefreshMode, uvRefreshDelete]
    UpdateOptions.KeyFields = 'id'
    SQL.Strings = (
      'do $$'
      'declare'
      'nExists numeric(2);'
      'begin'
      #9
      '  CREATE TABLE if not exists public.reestr ('
      #9#9'id serial4 NOT NULL,'
      #9#9'reestr_num varchar(50) NULL,'
      #9#9'reestr_date date NULL,'
      #9#9'date_create date DEFAULT now() NULL,'
      #9#9'date_confirm date NULL,'
      #9#9'date_export date NULL,'
      #9#9'CONSTRAINT reestr_pk PRIMARY KEY (id)'
      #9');'
      #9
      
        '  ALTER TABLE public.printforms ADD if not exists unit_num int N' +
        'ULL;'
      
        '  ALTER TABLE public.jdmain ADD if not exists date_eif date NULL' +
        ';'
      
        '  ALTER TABLE public.jdmain ADD if not exists reestr_id int4 NUL' +
        'L;'
      '  begin'
      
        '   ALTER TABLE public.jdmain ADD CONSTRAINT jdmain_reestr_fk FOR' +
        'EIGN KEY (reestr_id) REFERENCES reestr(id) ON DELETE RESTRICT;'
      '  exception'
      '   WHEN duplicate_object THEN'
      '   nExists := 1;'
      '  end;'
      ' '
      ' select count(*) into nExists'
      ' from public.printforms p;'
      ''
      ' if nExists = 1 then'
      ' '#9'update public.printforms '
      ' '#9'set name = '#39#1055#1088#1086#1089#1090#1086#1081' '#1088#1077#1077#1089#1090#1088' '#1087#1086' '#1086#1090#1086#1073#1088#1072#1085#1085#1099#1084' '#1079#1072#1087#1080#1089#1103#1084#39
      ' '#9'where unit_num is null;'
      ' '
      
        #9'INSERT INTO public.printforms ("name","template",group_field,sq' +
        'l_text,unit_num) VALUES'
      #9' ('#39#1056#1077#1077#1089#1090#1088#39','#39'reestr_doc.xlsx'#39',NULL,'#39'select j.*,'
      
        'substring((select reestr_num||'#39#39' '#1086#1090' '#39#39'||to_char(reestr_date, '#39#39'd' +
        'd.mm.yyyy'#39#39') from reestr r where r.id = j.reestr_id), 1, 80)::va' +
        'rchar(80) as reestr_num,'
      'p.code as person_code,'
      's.code as service_code,'
      's.name as service_name,'
      'sd.code as subdiv_code,'
      'sd.name as subdiv_name,'
      's.KBK  as kbk,'
      'a.code as account_number,'
      'p.type as person_type,'
      'p.name as person_name,'
      'p.jtcode,'
      'p.inn,'
      'p.snils,'
      'p.surname,'
      'p.firstname,'
      'p.secondname,'
      'p.sex,'
      'p.birthdate,'
      'p.address,'
      'p.doctype,'
      'p.docnumber,'
      'p.docseria,'
      'p.docwhen,'
      'p.docwho,'
      'p.contact_phone,'
      'p.region_id,'
      'p.area_id,'
      'p.town_id,'
      'p.street_id,'
      'p.house,'
      'p.building,'
      'p.flat,'
      'p.postindex'
      'from jdmain j'
      'left outer join persons p on (p.id  = j.person_id)'
      'left outer join services s on (s.id  = j.service_id )'
      'left outer join subdivs sd on (sd.id  = j.subdiv_id )'
      'left outer join accounts a on (a.id  = j.account_id )'
      'where j.reestr_id = :docid;'
      #39',2);'
      '    '
      ' end if;'
      ''
      ' SELECT 1 into nExists '
      ' FROM pg_proc WHERE proname = '#39'f_getnextdocnum'#39';'
      ' if coalesce(nExists,0)=0 then'
      #9'CREATE FUNCTION public.f_getnextdocnum(ddate date)'
      #9' RETURNS integer'
      #9' LANGUAGE plpgsql'
      #9'AS $function$'
      #9#9'declare nNextNum int4;'
      #9'begin'
      
        #9#9'select max(f_try_cast_int(r.reestr_num,0)) into nNextNum from ' +
        'reestr r'
      
        #9#9'where date_part('#39'year'#39', r.reestr_date) = date_part('#39'year'#39', dDa' +
        'te);'
      #9#9'return nNextNum+1;'
      #9'end;'
      #9'$function$'
      #9';'
      ' end if;'
      ''
      ' SELECT 1 into nExists '
      ' FROM pg_proc WHERE proname = '#39'f_try_cast_int'#39';'
      ' if coalesce(nExists,0)=0 then'
      
        #9'CREATE OR REPLACE FUNCTION public.f_try_cast_int(p_in text, p_d' +
        'efault integer DEFAULT NULL::integer)'
      #9' RETURNS integer'
      #9' LANGUAGE plpgsql'
      #9'AS $function$'
      #9'begin'
      #9'  begin'
      #9'    return p_in::int;'
      #9'  exception'
      #9'    when others then'
      #9'       return p_default;'
      #9'  end;'
      #9'end;'
      #9'$function$'
      #9';'
      ' end if;'
      ''
      ' SELECT 1 into nExists '
      ' FROM pg_proc WHERE proname = '#39'p_includeinreestr'#39';'
      ' if coalesce(nExists,0)=0 then'
      
        #9'CREATE OR REPLACE FUNCTION public.p_includeinreestr(sIds varcha' +
        'r, nReestrId int4, nCheckSign int)'
      #9'returns int4'
      #9' LANGUAGE plpgsql'
      #9'AS $procedure$'
      #9'declare '
      #9#9'rows_affected int4; '
      #9#9'rows_applicable int4;'
      #9#9'rows_total int4;'
      #9'begin'
      #9#9'if coalesce(nCheckSign,0) = 0 then'
      #9#9#9'if nReestrId>0 then'
      #9#9#9#9'update jdmain set reestr_id = nReestrId'
      #9#9#9#9'where id = any(string_to_array(sIds,'#39','#39')::int[])'
      #9#9#9#9'and not doc_num is null'
      #9#9#9#9'and reestr_id is null'
      #9#9#9#9'and not exists ('
      #9#9#9#9#9'select 1 from reestr r '
      #9#9#9#9#9'where r.id = reestr_id'
      #9#9#9#9#9'and not r.date_confirm is null'
      #9#9#9#9');'
      #9#9#9'else'
      #9#9#9#9'update jdmain set reestr_id = null'
      #9#9#9#9'where id = any(string_to_array(sIds,'#39','#39')::int[])'
      #9#9#9#9'and not exists ('
      #9#9#9#9#9'select 1 from reestr r '
      #9#9#9#9#9'where r.id = reestr_id'
      #9#9#9#9#9'and not r.date_confirm is null'
      #9#9#9#9');'
      #9#9#9'end if;'
      #9#9#9'GET DIAGNOSTICS rows_affected = ROW_COUNT;'
      #9#9#9'return rows_affected; '
      #9#9'else'
      #9#9#9'select '
      
        #9#9#9'sum(case when not doc_num is null and reestr_id is null then ' +
        '1 else 0 end),'
      #9#9#9'count(*) into rows_applicable, rows_total'
      #9#9#9'from jdmain'
      #9#9#9'where id = any(string_to_array(sIds,'#39','#39')::int[]);'
      #9#9#9'return rows_total - rows_applicable;'
      #9#9'end if;'
      #9'END;'
      #9'$procedure$'
      #9';'
      ' end if;'
      ''
      'end;'
      '$$;'
      '')
    Left = 72
    Top = 204
  end
end
