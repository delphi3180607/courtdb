unit setperiod;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Data.DB, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Mask, DBCtrlsEh,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TFormSetPeriod = class(TFormEdit)
    btMonth: TButton;
    bt3Month: TButton;
    grpPeriod: TGroupBox;
    dtStart: TDBDateTimeEditEh;
    dtEnd: TDBDateTimeEditEh;
    procedure btMonthClick(Sender: TObject);
    procedure bt3MonthClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormSetPeriod: TFormSetPeriod;

implementation

{$R *.dfm}

procedure TFormSetPeriod.bt3MonthClick(Sender: TObject);
begin
  dtStart.Value := IncMonth(now(),-3);
  dtEnd.Value := null;
end;

procedure TFormSetPeriod.btMonthClick(Sender: TObject);
begin
  dtStart.Value := IncMonth(now(),-1);
  dtEnd.Value := null;
end;

end.
