inherited FormPersons: TFormPersons
  Caption = #1054#1090#1074#1077#1090#1095#1080#1082#1080
  ClientWidth = 932
  ExplicitWidth = 946
  TextHeight = 16
  inherited plMain: TPanel
    Width = 932
    ExplicitWidth = 930
    inherited dgMain: TDBGridEh
      Width = 932
      OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghRowHighlight, dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove]
      Columns = <
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'typecode'
          Footers = <>
          Title.Caption = #1058#1080#1087
          Width = 163
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'code'
          Footers = <>
          Title.Caption = #1050#1086#1076
          Width = 142
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'name'
          Footers = <>
          Title.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
          Width = 262
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'inn'
          Footers = <>
          Title.Caption = #1048#1053#1053
          Width = 111
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'address'
          Footers = <>
          Title.Caption = #1040#1076#1088#1077#1089
          Width = 604
        end>
    end
    inherited plTop: TPanel
      Width = 932
      ExplicitWidth = 930
    end
    inherited plBottom: TPanel
      Width = 932
      ExplicitWidth = 930
      inherited btnCancel: TButton
        Left = 816
        ExplicitLeft = 814
      end
      inherited btnOk: TButton
        Left = 697
        ExplicitLeft = 695
      end
    end
  end
  inherited FDUpdateSet: TFDUpdateSQL
    InsertSQL.Strings = (
      'insert into persons'
      '('
      
        'code, name, "type", jtcode, inn, snils, surname, firstname, seco' +
        'ndname, sex, birthdate, address,'
      'doctype, docnumber, docseria, docwhen, docwho, contact_phone,'
      'region_id, area_id, town_id, street_id, '
      'region, area, town, street, '
      'house, building, flat, postindex'
      ')'
      'values'
      '('
      
        ':code, :name, :type, :jtcode, :inn, :snils, :surname, :firstname' +
        ', :secondname, :sex, :birthdate, :address,'
      
        ':doctype, :docnumber, :docseria, :docwhen, :docwho, :contact_pho' +
        'ne,'
      ':region_id, :area_id, :town_id, :street_id, '
      ':region, :area, :town, :street, '
      ':house, :building, :flat, :postindex'
      ')'
      'returning id;')
    ModifySQL.Strings = (
      'update persons set'
      'code = :code,'
      'name = :name,'
      '"type" = :type,'
      'jtcode = :jtcode,'
      'inn = :inn,'
      'snils = :snils,'
      'surname = :surname,'
      'firstname = :firstname,'
      'secondname = :secondname,'
      'sex = :sex,'
      'address = :address,'
      'doctype = :doctype,'
      'docnumber = :docnumber,'
      'docseria = :docseria,'
      'docwhen = :docwhen,'
      'docwho = :docwho,'
      'contact_phone = :contact_phone,'
      'region_id = :region_id,'
      'area_id = :area_id,'
      'town_id = :town_id,'
      'street_id = :street_id,'
      'region = :region,'
      'area = :area,'
      'town = :town,'
      'street = :street,'
      'house = :house,'
      'building = :building,'
      'flat = :flat,'
      'postindex = :postindex'
      'where id = :id;')
    DeleteSQL.Strings = (
      'delete from persons where id = :id;')
    FetchRowSQL.Strings = (
      'select p.*, '
      '('
      'case when p.type = '#39'0'#39' then '#39#1060#1080#1079#1080#1095#1077#1089#1082#1086#1077' '#1083#1080#1094#1086#39
      'when p.type = '#39'1'#39' then '#39#1070#1088#1080#1076#1080#1095#1077#1089#1082#1086#1077' '#1083#1080#1094#1086#39
      'else '#39#1053#1077' '#1086#1087#1088#1077#1076#1077#1083#1077#1085#1086#39' end'
      ')::varchar(50) as typecode'
      'from persons p'
      'where id = :id;')
  end
  inherited FDDataQr: TFDQuery
    AfterInsert = FDDataQrAfterInsert
    AfterPost = FDDataQrAfterPost
    SQL.Strings = (
      'select p.*, '
      '('
      'case when p.type = '#39'0'#39' then '#39#1060#1080#1079#1080#1095#1077#1089#1082#1086#1077' '#1083#1080#1094#1086#39
      'when p.type = '#39'1'#39' then '#39#1070#1088#1080#1076#1080#1095#1077#1089#1082#1086#1077' '#1083#1080#1094#1086#39
      'else '#39#1053#1077' '#1086#1087#1088#1077#1076#1077#1083#1077#1085#1086#39' end'
      ')::varchar(50) as typecode'
      'from persons p '
      'order by code;')
  end
end
