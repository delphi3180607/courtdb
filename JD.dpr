program JD;

uses
  Vcl.Forms,
  Dialogs,
  main in 'main.pas' {FormMain},
  Vcl.Themes,
  Vcl.Styles,
  edit in 'edit.pas' {FormEdit},
  dmu in 'dmu.pas' {dm: TDataModule},
  editjd in 'editjd.pas' {FormEditJD},
  setperiod in 'setperiod.pas' {FormSetPeriod},
  functions in 'functions.pas',
  QYN in 'QYN.pas' {FormQYN},
  Settings in 'Settings.pas' {FormSettings},
  base in 'base.pas' {FormBase},
  grid in 'grid.pas' {FormGrid},
  gridmain in 'gridmain.pas' {FormGridMain},
  login in 'login.pas' {FormLogin},
  persons in 'persons.pas' {FormPersons},
  accounts in 'accounts.pas' {FormAccounts},
  editcodename in 'editcodename.pas' {FormEditCodeName},
  services in 'services.pas' {FormServices},
  users in 'users.pas' {FormUsers},
  EditUser in 'EditUser.pas' {FormEditUser},
  ConnectionSettings in 'ConnectionSettings.pas' {FormConnectionSettings},
  wait in 'wait.pas' {FormWait},
  ChangePassword in 'ChangePassword.pas' {FormChangePassword},
  EditPrintForm in 'EditPrintForm.pas' {FormEditPrintForm},
  selectprintform in 'selectprintform.pas' {FormSelectPrintForm},
  PrintForms in 'PrintForms.pas' {FormPrintForms},
  selecttemplate in 'selecttemplate.pas' {FormSelectTemplate},
  register in 'register.pas' {FormRegister},
  import in 'import.pas' {FormImport},
  exportJD in 'exportJD.pas' {FormExportJD},
  EditPerson in 'EditPerson.pas' {FormEditPerson},
  editaccount in 'editaccount.pas' {FormEditAccount},
  editservice in 'editservice.pas' {FormEditService},
  subdivs in 'subdivs.pas' {FormSubDivs},
  EditSubDiv in 'EditSubDiv.pas' {FormEditSubDiv},
  SQLQuery in 'SQLQuery.pas' {FormSQLQuery},
  EditJD2 in 'EditJD2.pas' {FormEditJD2},
  FillAddress in 'FillAddress.pas' {FormFillAddress},
  Reestrs in 'Reestrs.pas' {FormReestrs},
  EditReestr in 'EditReestr.pas' {FormEditReestr},
  FilterReestr in 'FilterReestr.pas' {FormFilterReestr};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  TStyleManager.TrySetStyle('Waikawa Light 2');
  Application.Title := '�������� �������';
  Application.CreateForm(TFormMain, FormMain);
  Application.CreateForm(TFormEditCodeName, FormEditCodeName);
  Application.CreateForm(Tdm, dm);
  Application.CreateForm(TFormEditJD, FormEditJD);
  Application.CreateForm(TFormSetPeriod, FormSetPeriod);
  Application.CreateForm(TFormQYN, FormQYN);
  Application.CreateForm(TFormSettings, FormSettings);
  Application.CreateForm(TFormBase, FormBase);
  Application.CreateForm(TFormGrid, FormGrid);
  Application.CreateForm(TFormGridMain, FormGridMain);
  Application.CreateForm(TFormLogin, FormLogin);
  Application.CreateForm(TFormPersons, FormPersons);
  Application.CreateForm(TFormAccounts, FormAccounts);
  Application.CreateForm(TFormServices, FormServices);
  Application.CreateForm(TFormUsers, FormUsers);
  Application.CreateForm(TFormEditUser, FormEditUser);
  Application.CreateForm(TFormConnectionSettings, FormConnectionSettings);
  Application.CreateForm(TFormWait, FormWait);
  Application.CreateForm(TFormChangePassword, FormChangePassword);
  Application.CreateForm(TFormEditPrintForm, FormEditPrintForm);
  Application.CreateForm(TFormSelectPrintForm, FormSelectPrintForm);
  Application.CreateForm(TFormPrintForms, FormPrintForms);
  Application.CreateForm(TFormSelectTemplate, FormSelectTemplate);
  Application.CreateForm(TFormRegister, FormRegister);
  Application.CreateForm(TFormImport, FormImport);
  Application.CreateForm(TFormExportJD, FormExportJD);
  Application.CreateForm(TFormEditPerson, FormEditPerson);
  Application.CreateForm(TFormEditAccount, FormEditAccount);
  Application.CreateForm(TFormEditService, FormEditService);
  Application.CreateForm(TFormSubDivs, FormSubDivs);
  Application.CreateForm(TFormEditSubDiv, FormEditSubDiv);
  Application.CreateForm(TFormSQLQuery, FormSQLQuery);
  Application.CreateForm(TFormEditJD2, FormEditJD2);
  Application.CreateForm(TFormFillAddress, FormFillAddress);
  Application.CreateForm(TFormReestrs, FormReestrs);
  Application.CreateForm(TFormFilterReestr, FormFilterReestr);
  Application.CreateForm(TFormEditReestr, FormEditReestr);
  if not FormMain.TryOpenConnection then
  begin
    FormMain.SetConnection;
  end else
  begin
    FormWait.Label1.Caption := '�������� ��...';
    Application.ProcessMessages;
    dm.qrUpdateDataBase.ExecSQL;
  end;

  FormWait.Label1.Caption := '�����������...';
  Application.ProcessMessages;

  if (not FormMain.Login) then
  begin
    Application.ShowMainForm := false;
    Application.Terminate;
  end else
  begin
    try
      FormSettings.Init;
    except
      begin
        ShowMessage('�� ������� ���������������� ���������. ��������� ���������� ��������� ������.');
      end;
    end;
    FormMain.Init;
  end;

  Application.Run;

end.
