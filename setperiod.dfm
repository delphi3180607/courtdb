inherited FormSetPeriod: TFormSetPeriod
  Caption = #1055#1077#1088#1080#1086#1076
  ClientHeight = 241
  ClientWidth = 597
  ExplicitWidth = 603
  ExplicitHeight = 269
  PixelsPerInch = 96
  TextHeight = 16
  inherited plBottom: TPanel
    Top = 200
    Width = 597
    TabOrder = 3
    ExplicitTop = 200
    ExplicitWidth = 597
    inherited btnCancel: TButton
      Left = 481
      ExplicitLeft = 481
    end
    inherited btnOk: TButton
      Left = 362
      Caption = #1059#1089#1090#1072#1085#1086#1074#1080#1090#1100
      ExplicitLeft = 362
    end
  end
  object btMonth: TButton [1]
    Left = 8
    Top = 16
    Width = 169
    Height = 168
    Caption = #1052#1077#1089#1103#1094
    TabOrder = 0
    OnClick = btMonthClick
  end
  object bt3Month: TButton [2]
    Left = 183
    Top = 16
    Width = 169
    Height = 168
    Caption = #1058#1088#1080' '#1084#1077#1089#1103#1094#1072
    TabOrder = 1
    OnClick = bt3MonthClick
  end
  object grpPeriod: TGroupBox [3]
    AlignWithMargins = True
    Left = 366
    Top = 9
    Width = 219
    Height = 176
    Caption = #1055#1088#1086#1080#1079#1074#1086#1083#1100#1085#1086
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Calibri'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    object dtStart: TDBDateTimeEditEh
      Left = 16
      Top = 63
      Width = 185
      Height = 27
      ControlLabel.Width = 71
      ControlLabel.Height = 19
      ControlLabel.Caption = #1053#1072#1095#1080#1085#1072#1103' '#1089
      ControlLabel.Visible = True
      DynProps = <>
      EditButtons = <>
      Kind = dtkDateEh
      TabOrder = 0
      Visible = True
    end
    object dtEnd: TDBDateTimeEditEh
      Left = 16
      Top = 120
      Width = 185
      Height = 27
      ControlLabel.Width = 16
      ControlLabel.Height = 19
      ControlLabel.Caption = #1087#1086
      ControlLabel.Visible = True
      DynProps = <>
      EditButtons = <>
      Kind = dtkDateEh
      TabOrder = 1
      Visible = True
    end
  end
  inherited dsLocal: TDataSource
    Left = 144
    Top = 208
  end
  inherited qrAux: TFDQuery
    Left = 72
    Top = 216
  end
end
