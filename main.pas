unit main;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh,
  Vcl.ExtCtrls, Vcl.Buttons, PngSpeedButton, Vcl.ComCtrls, Vcl.Menus, IniFiles,
  Vcl.StdCtrls, Vcl.DBCtrls, DBCtrlsEh, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, MemTableDataEh, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, functions, base, grid;

type
  TFormMain = class(TForm)
    mm: TMainMenu;
    N1: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    N7: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    N11: TMenuItem;
    N12: TMenuItem;
    N13: TMenuItem;
    pmPC: TPopupMenu;
    pc: TPageControl;
    btClose: TPngSpeedButton;
    N2: TMenuItem;
    N14: TMenuItem;
    N10: TMenuItem;
    N15: TMenuItem;
    N16: TMenuItem;
    SQL1: TMenuItem;
    N17: TMenuItem;
    N18: TMenuItem;
    procedure N7Click(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure btCloseClick(Sender: TObject);
    procedure N12Click(Sender: TObject);
    procedure N6Click(Sender: TObject);
    procedure N8Click(Sender: TObject);
    procedure N11Click(Sender: TObject);
    procedure N14Click(Sender: TObject);
    procedure N9Click(Sender: TObject);
    procedure N15Click(Sender: TObject);
    procedure SQL1Click(Sender: TObject);
    procedure N18Click(Sender: TObject);
    procedure pcChange(Sender: TObject);
  private
    { Private declarations }
  public

    currentUser: string;
    isAdmin: boolean;
    currentUserId: integer;
    passwordChanged: boolean;
    currentPassword: string;

    procedure AddForm(f: TFormBase);
    procedure AddFormGrid(f: TFormGrid);
    procedure AddFormGrid1(var f: TFormGrid; c: TComponentClass);
    function Login: boolean;
    function CheckUser(login: string; password: string; weak: boolean = true): boolean;
    function TryOpenConnection: boolean;
    procedure SetConnection;
    function CheckAdmin: boolean;

    procedure Init;

  end;

var
  FormMain: TFormMain;

implementation

{$R *.dfm}

uses dmu, gridmain, services, login, users, wait, Settings, ConnectionSettings,
  ChangePassword, accounts, persons, PrintForms, subdivs, SQLQuery, Reestrs;

procedure TFormMain.Init;
begin
  if not isAdmin then
  begin
    AddFormGrid(FormGridMain);
    pc.ActivePage.Name := 'TabStart';
  end;
end;

procedure TFormMain.N11Click(Sender: TObject);
begin
  AddFormGrid(FormAccounts);
end;

procedure TFormMain.N12Click(Sender: TObject);
begin
  AddForm(FormSettings);
end;

procedure TFormMain.N14Click(Sender: TObject);
begin

  FormChangePassword.ShowModal;
  if FormChangePassword.ModalResult = mrOk then
  begin
    self.currentPassword := FormChangePassword.edPasswordNew.Text;
    dm.qrCommon.Close;
    dm.qrCommon.SQL.Clear;
    dm.qrCommon.SQL.Add('update users set password_changed = true, password = '''+self.currentPassword+'''');
    dm.qrCommon.SQL.Add('where id='+IntToStr(self.currentUserId));
    dm.qrCommon.ExecSQL;
    ShowMessage('������ ������� ����������.');
    exit;
  end else
  begin
    ShowMessage('������ �� �������.');
    exit;
  end;

end;

procedure TFormMain.N15Click(Sender: TObject);
begin
  AddFormGrid(FormSubDivs);
end;

procedure TFormMain.N18Click(Sender: TObject);
begin
  AddFormGrid(FormReestrs);
end;

procedure TFormMain.N4Click(Sender: TObject);
begin
  if CheckAdmin then AddFormGrid(FormUsers);
end;

procedure TFormMain.N6Click(Sender: TObject);
begin
  if CheckAdmin then SetConnection;
end;

procedure TFormMain.N7Click(Sender: TObject);
begin
  AddFormGrid(FormServices);
end;

procedure TFormMain.N8Click(Sender: TObject);
begin
  AddFormGrid(FormPersons);
end;

procedure TFormMain.N9Click(Sender: TObject);
begin
  AddFormGrid(FormPrintForms);
end;

procedure TFormMain.pcChange(Sender: TObject);
var t: TTabSheet; f: TFormGrid;
begin
  t := pc.ActivePage;
  f := TFormGrid(t.Controls[0]);
  f.Init;
end;

procedure TFormMain.AddForm(f: TFormBase);
var t: TTabSheet;
begin

   pc.Show;

  // ������� ���� ��� �����

  if Assigned(f.Parent) then
  begin
    t := TTabSheet(f.Parent);
    pc.ActivePage := t;
    t.Caption := f.Caption;
    exit;
  end;

  t := TTabSheet.Create(pc);
  t.Caption := f.Caption;
  t.PageControl := pc;
  t.PopupMenu := pmPc;
  t.Show;
  f.Parent := t;
  f.BorderStyle := bsNone;
  f.Align := alClient;
  f.Init;
  f.Show;
  btClose.Show;
end;


procedure TFormMain.AddFormGrid(f: TFormGrid);
var t: TTabSheet; i: integer;
begin

   pc.Show;

  // ������� ���� ��� �����

  if Assigned(f) then
  if Assigned(f.Parent) then
  begin
    t := TTabSheet(f.Parent);
    pc.ActivePage := t;
    exit;
  end;

  t := TTabSheet.Create(pc);
  t.Caption := f.Caption;
  t.PageControl := pc;
  t.PopupMenu := pmPc;
  t.Show;

  if not Assigned(f) then
  exit;

  f.Parent := t;
  f.BorderStyle := bsNone;
  f.Align := alClient;
  f.Show;
  f.BorderStyle := bsNone;
  f.Align := alClient;
  f.Init;

  btClose.Show;

end;


procedure TFormMain.AddFormGrid1(var f: TFormGrid; c: TComponentClass);
var t: TTabSheet; i: integer;
begin

   pc.Show;

  // ������� ���� ��� �����

  if Assigned(f) then
  if Assigned(f.Parent) then
  begin
    t := TTabSheet(f.Parent);
    pc.ActivePage := t;
    exit;
  end;

  if not Assigned(f) then
  Application.CreateForm(c,f);

  t := TTabSheet.Create(pc);
  t.Caption := f.Caption;
  t.PageControl := pc;
  t.PopupMenu := pmPc;
  t.Show;

  f.Parent := t;
  f.BorderStyle := bsNone;
  f.Align := alClient;
  f.Show;
  f.Init;
  btClose.Show;
end;


procedure TFormMain.btCloseClick(Sender: TObject);
var t: TTabSheet; f: TWinControl;
begin
 t := pc.ActivePage;
 if t.Name = 'TabStart' then exit;

 t.Hide;
 f := TWinControl(t.Controls[0]);
 f.Hide;
 f.Parent := nil;
 t.Free;

 if pc.PageCount = 0 then
 begin
   btClose.Hide;
   pc.Hide;
 end;

end;

function TFormMain.Login: boolean;
var c: TLabel;
begin

   result := false;

   if not dm.FDConnMain.Connected then
   begin
     exit;
   end;

   dm.qrCommon.Close;
   dm.qrCommon.SQL.Clear;
   dm.qrCommon.SQL.Add('select user_login from users where not isblocked order by 1');
   dm.qrCommon.Open;

   FormLogin.leLogin.Items.Clear;

   while not dm.qrCommon.Eof do
   begin
    FormLogin.leLogin.Items.Add(dm.qrCommon.Fields[0].AsString);
    dm.qrCommon.Next;
   end;

   while true do
    begin
      FormLogin.ShowModal;
      if FormLogin.ModalResult = mrOk then
      begin
        if (FormLogin.leLogin.Text = '�������������') and  (FormLogin.lePassword.Text = 'root147896325')
        then begin
          self.currentUser := 'root';
          self.isAdmin := true;
          result := true;
          break;
        end else
        begin
          if (not CheckUser(FormLogin.leLogin.Text, FormLogin.lePassword.Text, true)) then
          begin
            ShowMessage('�������� ���� �����/������');
            result := false;
          end else begin
            result := true;
            break;
          end;
        end;
      end;
      if (FormLogin.ModalResult <> mrOk) then
      begin
        result := false;
        break;
      end;
    end;
end;

function TFormMain.CheckUser(login: string; password: string; weak: boolean = true): boolean;
var rolename: string;
begin
  result := false;

  dm.qrCommon.Close;
  dm.qrCommon.SQL.Clear;
  dm.qrCommon.SQL.Add('select u.password, u.user_name, u.user_login, u.isAdmin, u.id, password_changed');
  dm.qrCommon.SQL.Add('from users u where UPPER(u.user_login)='''+AnsiUpperCase(login)+'''');
  dm.qrCommon.Open;

  if dm.qrCommon.RecordCount>0 then
  begin

    self.currentPassword := dm.qrCommon.Fields[0].AsString;
    self.currentUser := dm.qrCommon.Fields[1].AsString;
    self.isAdmin := dm.qrCommon.Fields[3].AsBoolean;
    self.currentUserId := dm.qrCommon.Fields[4].AsInteger;
    self.passwordChanged := dm.qrCommon.Fields[5].AsBoolean;

    if not self.passwordChanged and not self.isAdmin then
    begin

      FormChangePassword.ShowModal;
      if FormChangePassword.ModalResult = mrOk then
      begin
        self.currentPassword := FormChangePassword.edPasswordNew.Text;
        dm.qrCommon.Close;
        dm.qrCommon.SQL.Clear;
        dm.qrCommon.SQL.Add('update users set password_changed = true, password = '''+self.currentPassword+'''');
        dm.qrCommon.SQL.Add('where id='+IntToStr(self.currentUserId));
        dm.qrCommon.ExecSQL;
        ShowMessage('������ ������� ����������.');
        result := true;
        exit;
      end else
      begin
        ShowMessage('������ �� ����������.');
        exit;
      end;

    end;

    if (self.currentPassword = password) then
    begin

      if not dm.qrCommon.Fields[3].AsBoolean then
      begin
        self.N5.Visible := false;
      end;

      self.Caption := '�������� ������� v.2.0 ('+dm.qrCommon.Fields[1].AsString +')';

      result := true;
    end;
  end;
end;

function TFormMain.TryOpenConnection: boolean;
var inf: TIniFile;
begin

  result := true;

  dm.fdConnMain.Connected := false;

  SetCurrentDir(ExtractFileDir(Application.ExeName));
  inf := TIniFile.Create(ExtractFileDir(Application.ExeName)+'/dbsettings.ini');

  dm.fdConnMain.DriverName := 'PG';
  dm.FDConnMain.Params.Clear;
  dm.FDConnMain.Params.Add('DriverID=PG');
  dm.FDConnMain.Params.Add('Database='+inf.ReadString('main','db_name',''));
  dm.FDConnMain.Params.Add('Server='+inf.ReadString('main','server_name',''));
  dm.FDConnMain.Params.Add('User_Name='+inf.ReadString('main','user_name',''));
  dm.FDConnMain.Params.Add('Port='+inf.ReadString('main','port','5432'));
  dm.FDConnMain.Params.Add('Password='+inf.ReadString('main','user_password',''));

  try
    FormWait.Show;
    Application.ProcessMessages;
    dm.FDConnMain.Open;
  except
    on E:Exception do
    begin
      ShowMessage('������: '+E.Message);
      result := false;
    end;
  end;

  if not FormWait.canClose then Delay(500);
  if not FormWait.canClose then Delay(500);
  if not FormWait.canClose then Delay(500);
  if not FormWait.canClose then Delay(500);
  FormWait.Hide;

end;

procedure TFormMain.SetConnection;
var inf: TIniFile;
begin

 SetCurrentDir(ExtractFileDir(Application.ExeName));
 inf := TIniFile.Create(ExtractFileDir(Application.ExeName)+'/dbsettings.ini');
 FormConnectionSettings.cmbServer.Text := inf.ReadString('main','server_name','');
 FormConnectionSettings.cmbDBName.Text := inf.ReadString('main','db_name','');
 FormConnectionSettings.leLogin.Text := inf.ReadString('main','user_name','');
 FormConnectionSettings.lePassword.Text := inf.ReadString('main','user_password','');
 FormConnectionSettings.lePort.Text := inf.ReadString('main','port','5432');
 FormConnectionSettings.ShowModal;

 if FormConnectionSettings.ModalResult = mrOk then
 begin

    inf.WriteString('main','server_name',FormConnectionSettings.cmbServer.Text);
    inf.WriteString('main','db_name',FormConnectionSettings.cmbDBName.Text);
    inf.WriteString('main','user_name',FormConnectionSettings.leLogin.Text);
    inf.WriteString('main','user_password',FormConnectionSettings.lePassword.Text);
    inf.WriteString('main','port',FormConnectionSettings.lePort.Text);

    if not TryOpenConnection then
    begin
      ShowMessage('�� ������� ������������ � ���� ������, ���������� �������� ��������� �����������.');
    end else
    begin
      ShowMessage('�������� �����������');
    end;

 end;

end;

procedure TFormMain.SQL1Click(Sender: TObject);
begin
  if CheckAdmin then FormSQLQuery.ShowModal;
end;

function TFormMain.CheckAdmin: boolean;
begin

  if isAdmin then
  begin
    result := true;
    exit;
  end else
  begin
    ShowMessage('������� (��� ������) �������� ������ ��������������.');
    result := false;
    exit;
  end;

end;

end.
