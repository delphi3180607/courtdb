object FormExists: TFormExists
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = #1042#1085#1080#1084#1072#1085#1080#1077'!'
  ClientHeight = 261
  ClientWidth = 274
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object lbMessage: TLabel
    Left = 16
    Top = 16
    Width = 239
    Height = 30
    Caption = #1041#1072#1079#1072' JD '#1091#1078#1077' '#1089#1091#1097#1077#1089#1090#1074#1091#1077#1090' '#1085#1072' '#1074#1099#1073#1088#1072#1085#1085#1086#1084' '#1089#1077#1088#1074#1077#1088#1077'.'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Calibri'
    Font.Style = []
    ParentFont = False
    WordWrap = True
  end
  object rgSelect: TRadioGroup
    Left = 15
    Top = 68
    Width = 243
    Height = 112
    Caption = #1042#1099#1073#1077#1088#1080#1090#1077' '#1074#1072#1088#1080#1072#1085#1090#1099
    Items.Strings = (
      #1042#1099#1073#1088#1072#1090#1100' '#1076#1088#1091#1075#1086#1081' '#1089#1077#1088#1074#1077#1088' '#1073#1072#1079#1099' '#1076#1072#1085#1085#1099#1093
      #1059#1076#1072#1083#1080#1090#1100' '#1089#1091#1097#1077#1089#1090#1074#1091#1102#1097#1091#1102' '#1073#1072#1079#1091' '#1076#1072#1085#1085#1099#1093)
    TabOrder = 0
  end
  object btOK: TButton
    Left = 50
    Top = 196
    Width = 169
    Height = 53
    Caption = #1054#1050
    ModalResult = 2
    TabOrder = 1
    OnClick = btOKClick
  end
end
