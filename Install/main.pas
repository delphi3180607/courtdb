unit main;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls, Vcl.ComCtrls,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, IniFiles,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.UI.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Phys, FireDAC.Phys.PG, FireDAC.Phys.PGDef,
  FireDAC.VCLUI.Wait, Data.DB, FireDAC.Comp.Client, FireDAC.Comp.DataSet,
  Vcl.Mask, DBCtrlsEh, ShellApi, ShlObj, ComObj, ActiveX;

type
  TFormMain = class(TForm)
    plBottom: TPanel;
    btCancel: TButton;
    btFwd: TButton;
    pcMain: TPageControl;
    t1: TTabSheet;
    t5: TTabSheet;
    Label1: TLabel;
    lePassword: TLabeledEdit;
    leLogin: TLabeledEdit;
    cmbServer: TComboBox;
    lePort: TLabeledEdit;
    Label3: TLabel;
    t4: TTabSheet;
    meSQLDBCreate: TMemo;
    qrCommon: TFDQuery;
    FDConnMain: TFDConnection;
    t2: TTabSheet;
    meSQLTablesCreate: TMemo;
    od: TFileOpenDialog;
    edDir: TDBEditEh;
    btBkw: TButton;
    btSetup: TButton;
    t3: TTabSheet;
    meLog: TMemo;
    btExit: TButton;
    qrExists: TFDQuery;
    qrDelete: TFDQuery;
    t6: TTabSheet;
    meSQLData: TMemo;
    procedure btCancelClick(Sender: TObject);
    procedure btFwdClick(Sender: TObject);
    procedure btBkwClick(Sender: TObject);
    procedure pcMainChange(Sender: TObject);
    procedure edDirEditButtons0Click(Sender: TObject; var Handled: Boolean);
    procedure btSetupClick(Sender: TObject);
    procedure btExitClick(Sender: TObject);
  private
    procedure ExecLongQuery(sl: TStrings);
  public
    function TryOpenConnection(dbname: string; trydelete: boolean = true): boolean;
    function CreateLink(FileName, DestDirectory: string; OverwriteExisting, AddNumberIfExists: Boolean): string;
  end;

var
  FormMain: TFormMain;

implementation

{$R *.dfm}

uses exists;

procedure TFormMain.btBkwClick(Sender: TObject);
begin
  if pcMain.ActivePage.PageIndex>0
  then pcMain.ActivePage := pcMain.Pages[pcMain.ActivePage.PageIndex-1];
  btSetup.Hide;
  btFwd.Enabled := true;
end;

procedure TFormMain.btCancelClick(Sender: TObject);
var temp: integer;
begin

  temp:=MessageBox(handle, PChar('�������� ���������?'), PChar('��������'), MB_YESNO+MB_ICONQUESTION);
  case temp of
    idyes:Application.Terminate;
  end;

end;

procedure TFormMain.btExitClick(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TFormMain.btFwdClick(Sender: TObject);
begin
  if pcMain.ActivePage=t1  then
  begin
    if TryOpenConnection('postgres', true) then
    begin
      pcMain.ActivePage := t2;
      btFwd.Enabled := false;
    end;

    if edDir.Text<>'' then
    btSetup.Show;

  end;
end;

procedure TFormMain.btSetupClick(Sender: TObject);
var inf: TIniFile; currdir, destdir: string; desktopPath: array [0..MAX_PATH] of Char;
begin

    meLog.Lines.Clear;

    SetCurrentDir(ExtractFileDir(Application.ExeName));

    pcMain.ActivePage := t3;
    Application.ProcessMessages;

    meLog.Lines.Add('�������� ���� ������...');
    Application.ProcessMessages;

    qrCommon.Close;
    qrCommon.SQL.Clear;
    qrCommon.SQL.AddStrings(meSQLDBCreate.Lines);
    qrCommon.ExecSQL;

    // ���������������� � ���� ����
    if not TryOpenConnection('jd', false) then
    begin
      exit;
    end;

    meLog.Lines.Add('��������� ���������...');
    Application.ProcessMessages;

    // ������� �������
    qrCommon.Close;
    qrCommon.SQL.Clear;
    qrCommon.SQL.AddStrings(meSQLTablesCreate.Lines);
    qrCommon.ExecSQL;

    // ������ ������
    //qrCommon.Close;
    //qrCommon.SQL.Clear;
    //qrCommon.SQL.AddStrings
    //qrCommon.ExecSQL;

    ExecLongQuery(meSQLData.Lines);

    // �������� ����������� ����
    meLog.Lines.Add('����������� ������������ �����...');
    Application.ProcessMessages;

    currdir := ExtractFileDir(Application.ExeName)+'\Assets\';
    destdir := edDir.Text+'\';

    CopyFile(PChar(currdir+'JD.exe'),PChar(destdir+'JD.exe'),false);
    CopyFile(PChar(currdir+'libpq.dll'),PChar(destdir+'libpq.dll'),false);

    // ����������� ������������
    meLog.Lines.Add('��������� ������������...');
    Application.ProcessMessages;

    inf := TIniFile.Create(destdir+'/dbsettings.ini');
    inf.WriteString('main','server_name',cmbServer.Text);
    inf.WriteString('main','db_name','jd');
    inf.WriteString('main','user_name',leLogin.Text);
    inf.WriteString('main','user_password',lePassword.Text);
    inf.WriteString('main','port',lePort.Text);

    // ������� �����
    meLog.Lines.Add('�������� ������ �� ������� �����...');
    Application.ProcessMessages;

    ShGetSpecialFolderPath(Application.Handle, desktopPath, CSIDL_DESKTOPDIRECTORY, False);

    CreateLink(destdir+'JD.exe', desktopPath, true,false);

    meLog.Lines.Add('������.');
    Application.ProcessMessages;

    btSetup.Hide;
    btExit.Show;
    btCancel.Hide;

end;

procedure TFormMain.edDirEditButtons0Click(Sender: TObject;
  var Handled: Boolean);
begin
  if od.Execute then
  begin
    self.edDir.Text := od.FileName;
    btSetup.Show;
  end;
end;

procedure TFormMain.pcMainChange(Sender: TObject);
begin

  if pcMain.TabIndex=1 then
    btFwd.Enabled := false
  else
    btFwd.Enabled := true;

end;

function TFormMain.TryOpenConnection(dbname: string; trydelete: boolean): boolean;
begin

  result := true;

  fdConnMain.Connected := false;

  fdConnMain.DriverName := 'PG';
  FDConnMain.Params.Clear;
  FDConnMain.Params.Add('DriverID=PG');
  FDConnMain.Params.Add('Database='+dbname);
  FDConnMain.Params.Add('Server='+cmbServer.Text);
  FDConnMain.Params.Add('User_Name='+leLogin.Text);
  FDConnMain.Params.Add('Port='+lePort.Text);
  FDConnMain.Params.Add('Password='+lePassword.Text);

  try
    FDConnMain.Open;
  except
    on E:Exception do
    begin
      ShowMessage('������: '+E.Message);
      result := false;
    end;
  end;

  if trydelete then
  begin

    qrExists.Close;
    qrExists.Open;

    if qrExists.RecordCount>0 then
    begin

      result := false;

      FormExists.ShowModal;
      if FormExists.ModalResult = mrOk then
      begin
        qrDelete.ExecSQL;
        result := true;
      end;

    end;

  end;

end;


function TFormMain.CreateLink(FileName, DestDirectory: string; OverwriteExisting, AddNumberIfExists: Boolean): string;
var
  MyObject: IUnknown;
  MySLink: IShellLink;
  MyPFile: IPersistFile;
  WFileName: WideString;
  X: INTEGER;
begin
  //���������� RESULT = ''
  Result := '';
  //���� ������, ��� �������� �������� ����� �� ����������, ��� �� ��
  // ���������� ����������, ��� ������ ���� ������ ����� �����, �� EXIT
  if (FileExists(FileName) = FALSE) or (DirectoryExists(DestDirectory) = FALSE)
    then
    exit;
  MyObject := CreateComObject(CLSID_SHELLLINK);
  MyPFile := MyObject as IPersistFile;
  MySLink := MyObject as IShellLink;
  with MySLink do
  begin
    SetArguments('');
    SetPath(PChar(FileName));
    SetWorkingDirectory(PChar(ExtractFilePath(FileName)));
  end;

  //�������������� ������������ ������������ '\' � ���� ����������
  //������������ ������������ ������
  if DestDirectory[length(DestDirectory)] <> '\' then
    DestDirectory := DestDirectory + '\';
  // ��������� ���������� ��������� ����� ������
  //WFileName := DestDirectory+ChangeFileExt(ExtractFileName(FileName), '') + '.lnk';
  WFileName := DestDirectory+ '�������� �������.lnk';
  //���� ����� � ����� ������ ��� ����������, ��
  if (FileExists(WFileName)) then
  begin
    // ���� �� ���� ������������ ������������ �����, � ���� ��������
    // ���������� ����� ������������� � ����� ������������ ������, ��������
    // blobby1.lnk, blobby2.lnk
    if (OverwriteExisting = FALSE) and (AddNumberIfExists = TRUE) then
    begin
      // ���������� ����� ������ ���������� ����� ���� �������� �
      // ����� ������
      X := 0;
      repeat
        X := X + 1;
        WFileName := DestDirectory+ChangeFileExt(ExtractFileName(FileName), '')+ IntToStr(X) + '.lnk';
      until FileExists(WFileName) = FALSE;
      // � ��������� �����
      MyPFile.Save(PWChar(WFileName), FALSE);
      Result := WFileName;
    end;
    //���� ���� ������������ ������������ �����
    if OverwriteExisting = TRUE then
    begin
      //..., �� ������������ ��� :)
      MyPFile.Save(PWChar(WFileName), FALSE);
      Result := WFileName;
    end;
  end
  else
  begin
    //� ������, ���� ������ � �������� ������� ��� ��� � �����
    //����������, �� ������ �����
    MyPFile.Save(PWChar(WFileName), FALSE);
    Result := WFileName;
  end;
end;

procedure TFormMain.ExecLongQuery(sl: TStrings);
var i: integer;
begin

  for i  := 0 to sl.Count-1 do
  begin

      qrCommon.Close;
      qrCommon.SQL.Clear;
      qrCommon.SQL.Text := sl[i];
      if trim(qrCommon.SQL.Text)<>'' then
      begin
        try
          qrCommon.ExecSQL;
        except
          on E:Exception do
          begin
            meLog.Lines.Add('������: '+E.Message+', '+IntToStr(i)+': '+sl[i]);
          end;
        end;
      end;

  end;

end;



end.
