program SetupJD;

uses
  Vcl.Forms,
  main in 'main.pas' {FormMain},
  Vcl.Themes,
  Vcl.Styles,
  exists in 'exists.pas' {FormExists};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  TStyleManager.TrySetStyle('RadiantModified');
  Application.CreateForm(TFormMain, FormMain);
  Application.CreateForm(TFormExists, FormExists);
  Application.Run;
end.
