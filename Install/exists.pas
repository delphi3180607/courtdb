unit exists;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls;

type
  TFormExists = class(TForm)
    lbMessage: TLabel;
    rgSelect: TRadioGroup;
    btOK: TButton;
    procedure btOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormExists: TFormExists;

implementation

{$R *.dfm}

procedure TFormExists.btOKClick(Sender: TObject);
begin

  if rgSelect.ItemIndex = 1 then
    self.ModalResult := mrOk
  else
    self.ModalResult := mrCancel;

end;

end.
