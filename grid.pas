unit grid;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, base, FireDAC.Stan.Intf,
  FireDAC.Stan.Param, FireDAC.Phys.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.Stan.Async,
  FireDAC.DApt, DBGridEhGrouping, ToolCtrlsEh, DBGridEhToolCtrls, DynVarsEh,
  Vcl.Buttons, PngSpeedButton, EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh,
  Vcl.ExtCtrls, Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.Menus,
  edit, functions, MemTableDataEh, MemTableEh, Vcl.StdCtrls, EXLReportExcelTLB,
  EXLReportBand, EXLReport, EXLReportDictionary, EhLibMTE;

type
  TFormGrid = class(TFormBase)
    plMain: TPanel;
    dgMain: TDBGridEh;
    plTop: TPanel;
    sbDelete: TPngSpeedButton;
    sbEdit: TPngSpeedButton;
    sbAdd: TPngSpeedButton;
    pmMain: TPopupMenu;
    N14: TMenuItem;
    N15: TMenuItem;
    N16: TMenuItem;
    N17: TMenuItem;
    N18: TMenuItem;
    sbPrint: TPngSpeedButton;
    sbExcel: TPngSpeedButton;
    sbImport: TPngSpeedButton;
    sbCopy: TPngSpeedButton;
    meBuffer: TMemTableEh;
    plBottom: TPanel;
    btnCancel: TButton;
    btnOk: TButton;
    exReport: TEXLReport;
    qrReport: TFDQuery;
    meReport: TMemTableEh;
    procedure sbAddClick(Sender: TObject);
    procedure N18Click(Sender: TObject);
    procedure sbDeleteClick(Sender: TObject);
    procedure sbEditClick(Sender: TObject);
    procedure sbExcelClick(Sender: TObject);
    procedure sbImportClick(Sender: TObject);
    procedure sbCopyClick(Sender: TObject);
    procedure dgMainSortMarkingChanged(Sender: TObject);
  private
    { Private declarations }
  protected
    unit_num1: integer;
    unit_num2: integer;
  public
    Ids: string;
    FormEditLocal: TFormEdit;
    procedure ImportSheet(data:Variant; DimX:integer; DimY:integer); virtual;
    procedure AfterAppendRecord; virtual;
    procedure AfterCopyRecord; virtual;
    procedure Init; override;
    procedure GetIds(grid: TDBGridEh; query: TFDQuery);
    procedure PrintReport;
  end;

var
  FormGrid: TFormGrid;

implementation

{$R *.dfm}

uses import, selecttemplate, Settings;

procedure TFormGrid.dgMainSortMarkingChanged(Sender: TObject);
begin
  FDDataQr.IndexFieldNames := dgMain.SortMarkedColumns.Items[0].FieldName;
  if dgMain.SortMarkedColumns.Items[0].Title.SortMarker = smUpEh then
  FDDataQr.IndexFieldNames := FDDataQr.IndexFieldNames+':D';
end;

procedure TFormGrid.GetIds(grid: TDBGridEh; query: TFDQuery);
var i: integer; d: string;
begin

  Ids := '';

  if grid.Selection.SelectionType = gstNon then
  begin
    Ids := query.FieldByName('id').AsString;
  end;

  if grid.Selection.SelectionType = gstRecordBookmarks then
  begin
    d := '';
    for i := 0 to grid.Selection.Rows.Count-1 do
    begin
      query.Bookmark := grid.Selection.Rows[i];
      Ids := Ids+d+query.FieldByName('id').AsString;
      d := ',';
    end;
  end;

  if grid.Selection.SelectionType = gstAll then
  begin
    d := '';
    query.First;
    while not query.Eof do
    begin
      Ids := Ids+d+query.FieldByName('id').AsString;
      d := ',';
      query.Next;
    end;
  end;
end;

procedure TFormGrid.Init;
begin
  FDDataQr.Close;
  FDDataQr.Open;
end;

procedure TFormGrid.N18Click(Sender: TObject);
begin
  FDDataQr.Close;
  FDDataQr.Open;
end;

procedure TFormGrid.PrintReport;
var groupfield: string; i: integer; field: TField; rf: TEXLReportField;
begin

  FormSelectTemplate.qrAux.Close;
  FormSelectTemplate.qrAux.Params.ParamByName('unit_num1').Value := unit_num1;
  FormSelectTemplate.qrAux.Params.ParamByName('unit_num2').Value := unit_num2;
  FormSelectTemplate.qrAux.Open;

  if FormSelectTemplate.qrAux.RecordCount = 0 then
  begin
    ShowMessage('��� �� ������ ������������ ������� ������ ��� ������� �������.');
    exit;
  end;

  FormSelectTemplate.ShowModal;
  if FormSelectTemplate.ModalResult = mrOk then
  begin

    exReport.Dictionary.Clear;
    for field in FDDataQr.Fields do
    begin
      rf := exReport.Dictionary.Add;
      rf.FieldName := field.FieldName;
      rf.ValueAsString := field.AsString;
    end;

    for field in FormSettings.FDDataQr.Fields do
    begin
      rf := exReport.Dictionary.Add;
      rf.FieldName := field.FieldName;
      rf.ValueAsString := field.AsString;
    end;

    qrReport.Close;
    qrReport.SQL.Clear;
    qrReport.Params.Clear;
    qrReport.SQL.Text := FormSelectTemplate.qrAux.FieldByName('sql_text').AsString;

    try

      if Assigned(qrReport.Params.FindParam('docid')) then
      begin
        qrReport.Params.ParamByName('docid').Value := FDDataQr.FieldByName('id').AsInteger;
        exReport.DataSet := qrReport;
        qrReport.Open;
      end else
      begin
        meReport.LoadFromDataSet(FDDataQr,1,lmCopy,false);
        meReport.Open;
        meReport.Delete;
        if (dgMain.Selection.SelectionType = gstAll) or (dgMain.Selection.SelectionType = gstNon) then
        begin
           meReport.LoadFromDataSet(FDDataQr,0,lmAppend,false);
        end else
        if (dgMain.Selection.SelectionType = gstRecordBookmarks) then
        begin
            for i := 0 to dgMain.Selection.Rows.Count-1 do
            begin
              try
                FDDataQr.Bookmark := dgMain.Selection.Rows[i];
                meReport.LoadFromDataSet(FDDataQr,1,lmAppend,false);
              except
                //--
              end;
            end;
        end;
      end;

      exReport.Template := ExtractFilePath(Application.ExeName) +'/templates/'+FormSelectTemplate.qrAux.FieldByName('template').AsString;
      exReport.Template := StringReplace(exReport.Template, '\/', '\', [rfReplaceAll]);
      exReport.Template := StringReplace(exReport.Template, '\\', '\', [rfReplaceAll]);
      exReport.Template := StringReplace(exReport.Template, '//', '\', [rfReplaceAll]);
      groupfield := FormSelectTemplate.qrAux.FieldByName('group_field').AsString;
      exReport.Show('', false, groupfield, '');

    finally
      Screen.Cursor := crDefault;
    end;

  end;

end;

procedure TFormGrid.sbAddClick(Sender: TObject);
var i: integer;
begin

  FormEditLocal.current_mode := 'insert';
  FDDataQr.Append;
  AfterAppendRecord;
  FormEditLocal.dsLocal.DataSet := FDDataQr;

  for i := 0 to FDDataQr.FieldDefs.Count-1 do
  begin
    if FDDataQr.FieldDefs[i].DataType = ftBoolean then
      if FDDataQr.Fields[i].Value = null then FDDataQr.Fields[i].Value := 0;
  end;

  FormEditLocal.ShowModal;

  if FormEditLocal.ModalResult = mrOk then
  begin
    FDDataQr.Post;
    FdUpdateSet.Commands[arFetchRow].Params.AssignValues(FDDataQr.Params);
    FDDataQr.RefreshRecord(false);
  end
  else
    FDDataQr.Cancel;
end;

procedure TFormGrid.sbCopyClick(Sender: TObject);
begin
  meBuffer.LoadFromDataSet(FDDataQr, 1, lmCopy, false);
  meBuffer.Open;
  FDDataQr.Append;
  FDDataQr.CopyRecord(meBuffer);
  AfterCopyRecord;
  sbEditClick(nil);
  meBuffer.Close;
end;

procedure TFormGrid.sbDeleteClick(Sender: TObject);
begin
   if fQYN('������������� ������� ������ �� ������� '+self.Caption+'?') then
   begin
     FDDataQr.Delete;
   end;
end;

procedure TFormGrid.sbEditClick(Sender: TObject);
var i: integer;
begin

  FormEditLocal.current_mode := 'update';
  FDDataQr.Edit;
  FormEditLocal.dsLocal.DataSet := FDDataQr;

  for i := 0 to FDDataQr.FieldDefs.Count-1 do
  begin
    if FDDataQr.FieldDefs[i].DataType = ftBoolean then
      if FDDataQr.Fields[i].Value = null then FDDataQr.Fields[i].Value := 0;
  end;

  FormEditLocal.ShowModal;

  if FormEditLocal.ModalResult = mrOk then
    FDDataQr.Post
  else
    FDDataQr.Cancel;

  FdUpdateSet.Commands[arFetchRow].Params.AssignValues(FDDataQr.Params);
  FDDataQr.RefreshRecord(false);

end;

procedure TFormGrid.sbExcelClick(Sender: TObject);
begin

  if fQYN('�������������� ������ � Excel ?') then
    ExportExcel(dgMain, self.Caption);

end;

procedure TFormGrid.sbImportClick(Sender: TObject);
begin
  FormImport.ShowModal;
  if FormImport.ModalResult = mrOk then
  begin
    ImportSheet(FormImport.FData, FormImport.DimX, FormImport.DimY);
    FDDataQr.Close;
    FDDataQr.Open;
  end;
end;


procedure TFormGrid.ImportSheet(data:Variant; DimX:integer; DimY:integer);
begin
 //--
end;

procedure TFormGrid.AfterAppendRecord;
begin
  //--
end;

procedure TFormGrid.AfterCopyRecord;
begin
  //--
end;


end.
