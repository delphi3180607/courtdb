inherited FormChangePassword: TFormChangePassword
  Caption = #1048#1079#1084#1077#1085#1080#1090#1100' '#1087#1072#1088#1086#1083#1100
  ClientHeight = 197
  ClientWidth = 278
  Position = poScreenCenter
  ExplicitWidth = 294
  ExplicitHeight = 236
  TextHeight = 16
  inherited plBottom: TPanel
    Top = 156
    Width = 278
    TabOrder = 2
    ExplicitTop = 156
    ExplicitWidth = 278
    inherited btnCancel: TButton
      Left = 176
      ExplicitLeft = 162
    end
    inherited btnOk: TButton
      Left = 57
      Caption = #1059#1089#1090#1072#1085#1086#1074#1080#1090#1100
      OnClick = btnOkClick
      ExplicitLeft = 43
    end
  end
  object edPasswordNew: TDBEditEh [1]
    Left = 16
    Top = 30
    Width = 241
    Height = 27
    ControlLabel.Width = 47
    ControlLabel.Height = 18
    ControlLabel.Caption = #1055#1072#1088#1086#1083#1100
    ControlLabel.Font.Charset = DEFAULT_CHARSET
    ControlLabel.Font.Color = clWindowText
    ControlLabel.Font.Height = -15
    ControlLabel.Font.Name = 'Calibri'
    ControlLabel.Font.Style = []
    ControlLabel.ParentFont = False
    ControlLabel.Visible = True
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    Visible = True
  end
  object edPasswordNew1: TDBEditEh [2]
    Left = 16
    Top = 94
    Width = 241
    Height = 27
    ControlLabel.Width = 117
    ControlLabel.Height = 18
    ControlLabel.Caption = #1055#1086#1074#1090#1086#1088#1080#1090#1077' '#1087#1072#1088#1086#1083#1100
    ControlLabel.Font.Charset = DEFAULT_CHARSET
    ControlLabel.Font.Color = clWindowText
    ControlLabel.Font.Height = -15
    ControlLabel.Font.Name = 'Calibri'
    ControlLabel.Font.Style = []
    ControlLabel.ParentFont = False
    ControlLabel.Visible = True
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    Visible = True
  end
end
