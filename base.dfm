object FormBase: TFormBase
  Left = 0
  Top = 0
  Caption = 'FormBase'
  ClientHeight = 654
  ClientWidth = 934
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Verdana'
  Font.Style = []
  TextHeight = 16
  object FDUpdateSet: TFDUpdateSQL
    Connection = dm.FDConnMain
    FetchRowSQL.Strings = (
      'select j.*, p.person_name , s.service_name from jdmain j'
      'left outer join persons p on (p.id  = j.person_id)'
      'left outer join services s on (s.id  = j.service_id )'
      'where j.id = :id;')
    Left = 496
    Top = 116
  end
  object FDDataQr: TFDQuery
    Connection = dm.FDConnMain
    UpdateOptions.AssignedValues = [uvEInsert, uvRefreshMode, uvRefreshDelete]
    UpdateOptions.KeyFields = 'id'
    UpdateObject = FDUpdateSet
    Left = 576
    Top = 116
  end
  object dsData: TDataSource
    DataSet = FDDataQr
    Left = 640
    Top = 112
  end
  object qrAux: TFDQuery
    Connection = dm.FDConnMain
    UpdateOptions.AssignedValues = [uvEInsert, uvRefreshMode, uvRefreshDelete]
    UpdateOptions.KeyFields = 'id'
    Left = 480
    Top = 212
  end
end
