inherited FormAccounts: TFormAccounts
  Caption = #1051#1080#1094#1077#1074#1099#1077' '#1089#1095#1077#1090#1072
  TextHeight = 16
  inherited plMain: TPanel
    inherited dgMain: TDBGridEh
      Columns = <
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'code'
          Footers = <>
          Title.Caption = #1053#1086#1084#1077#1088
          Width = 207
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'name'
          Footers = <>
          Title.Caption = #1054#1087#1080#1089#1072#1085#1080#1077
          Width = 445
        end>
    end
  end
  inherited FDUpdateSet: TFDUpdateSQL
    InsertSQL.Strings = (
      'INSERT INTO public.accounts'
      '(code, "name")'
      'VALUES(:code, :name)'
      'returning id;')
    ModifySQL.Strings = (
      'UPDATE public.accounts'
      'SET code=:code, "name"=:name'
      'where id = :id;')
    DeleteSQL.Strings = (
      'delete from accounts where id = :id')
    FetchRowSQL.Strings = (
      'select * from accounts where id = :id')
  end
  inherited FDDataQr: TFDQuery
    SQL.Strings = (
      'select * from accounts')
  end
end
