unit functions;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.Buttons, edit, QYN, DBCtrlsEh, DBGridEh,
  Data.DB, ComObj, DBSQLLookUp, StrUtils, Math;


function fQYN(mess: string):boolean;
procedure ExportExcel(grid: TDbGridEh; title: string; formats: TStringList = nil);
procedure Delay (dwMilliseconds: Longint);
function SFDE(c: TComponentClass; ef: TDBSQLLookUp; maximize: boolean = false): boolean;
function VarToInt(v: Variant):integer;
function VarToReal(v: Variant): Real;
function GetUNIFOControl(value: string): string;
function CreateUIN(urn, docnumber: string; docdate: TDateTime): string;

implementation

uses grid;

function fQYN(mess: string): boolean;
begin
  FormQYN.Label1.Caption := mess;
  FormQYN.Height := FormQYN.Label1.Height + 100;
  FormQYN.ShowModal;
  result := (FormQYN.ModalResult = mrOk);
end;


procedure ExportExcel(grid: TDbGridEh; title: string; formats: TStringList = nil);
var i,j,index: Integer; ds: TDataSet;
ExcelApp,sheet: Variant;
Range, Cell1, Cell2, ArrayData: Variant;
BeginCol, BeginRow: integer;
RowCount, ColCount : integer;
begin
 Screen.Cursor := crHourGlass;
 try
   Application.ProcessMessages;
   ds := grid.DataSource.DataSet;
   ds.DisableControls;
   ExcelApp := CreateOleObject('Excel.Application');
   ExcelApp.Visible := False;
   ExcelApp.WorkBooks.Add(-4167);
   ExcelApp.WorkBooks[1].WorkSheets[1].name := 'Export';
   sheet:=ExcelApp.WorkBooks[1].WorkSheets['Export'];

   sheet.cells[1,1] := title;
   sheet.cells[1,1].Font.Bold:=1;
   sheet.cells[1,1].Font.Size:=14;

   for i := 1 to grid.Columns.Count do
   begin
     sheet.columns.columns[i].ColumnWidth := IntToStr(trunc(grid.Columns[i-1].Width/8));
     //if formats<>nil then
     sheet.columns.columns[i].NumberFormatLocal := '##'; //formats[i-1];
     sheet.columns.columns[i].WrapText:= true;
     sheet.cells[2,i] := grid.Columns[i-1].Title.Caption;
     sheet.cells[2,i].Interior.Color:=$00DDFFFF;
     sheet.cells[2,i].WrapText:= true;
     sheet.cells[2,i].Borders.LineStyle:=1;
     sheet.cells[2,i].Borders.Weight:=2;
     sheet.cells[2,i].VerticalAlignment := -4108;
   end;

   sheet.cells[1,1].WrapText:= false;

   index := 3;
   ds.First;

   BeginRow := 3;
   BeginCol := 1;

   RowCount := ds.RecordCount;
   ColCount := grid.Columns.Count;

  // ������� ���������� ������, ������� �������� ��������� �������
  ArrayData := VarArrayCreate([1, RowCount, 1, ColCount], varVariant);

   // ��������� ������
   for i:=1 to  ds.RecordCount do
   begin
      for j:=1 to grid.Columns.Count do
      begin
        {sheet.cells[index,j].WrapText:= true;
        sheet.cells[index,j].Borders.LineStyle:=1;
        sheet.cells[index,j].Borders.Weight:=2;
        sheet.cells[index,j].VerticalAlignment := -4108;
        sheet.cells[index,j]:=ds.DataSet.FieldByName(grid.Columns[j-1].FieldName).AsString;}
        try
          if grid.Columns[j-1].FieldName<>'' then ArrayData[i, j] := ds.FieldByName(grid.Columns[j-1].FieldName).AsString;
        except
         //
        end;
        // ������ � ������ ����

        {Cell1 := sheet.Cells[i, 1];
        Cell2 := sheet.Cells[i, ColCount];
        Range := sheet.Range[Cell1, Cell2];
        Range.Interior.Color := clYellow;}

      end;
      inc(index);
      ds.Next;
   end;

  // ����� ������� ������ �������, � ������� ����� �������� ������
    Cell1 := sheet.Cells[BeginRow, BeginCol];

  // ������ ������ ������ �������, � ������� ����� �������� ������
    Cell2 := sheet.Cells[BeginRow  + RowCount - 1, BeginCol + ColCount - 1];

  // �������, � ������� ����� �������� ������
    Range := sheet.Range[Cell1, Cell2];

  // � ��� � ��� ����� ������
    // ������� ������� ����������� ����������
    Range.Value := ArrayData;
    Range.Borders.LineStyle:=1;
    Range.Borders.Weight:=2;
    Range.VerticalAlignment := -4108;

    ExcelApp.Visible := true;
    ds.EnableControls;

 except
    on E : Exception do begin
      ds.EnableControls;
      ShowMessage('������: '+E.Message);
    end;
 end;
 Screen.Cursor := crDefault;
end;


procedure Delay (dwMilliseconds: Longint);
var  iStart, iStop: DWORD;
begin
  iStart := GetTickCount;
  repeat    iStop := GetTickCount;
  Sleep(1); // ����������� ��������� �������� ����������
  Application.ProcessMessages;
  until (iStop - iStart) >= dwMilliseconds;
end;


function VarToInt(v: Variant):integer;
begin
  if v = null then
  begin
    result := 0;
    exit;
  end;
  if v <= 0 then
  begin
    result := 0;
    exit;
  end;

  result := v;

end;

function VarToReal(v: Variant): Real;
begin
  if v = null then
  begin
    result := 0;
    exit;
  end;
  if VarToStr(v)='' then
  begin
    result := 0;
    exit;
  end;
  result := Real(v);
end;

function SFDE(c: TComponentClass; ef: TDBSQLLookUp; maximize: boolean = false): boolean;
var n: TFormGrid;
begin
  Application.CreateForm(c,n);
  n.Init;
  n.Position := poMainFormCenter;
  n.Width := Screen.Width-200;
  n.Height := Screen.Height-300;
  n.Parent := nil;
  n.plBottom.Show;
  n.FDDataQr.Locate('id', VarToInt(ef.KeyValue),[]);
  result := false;
  if maximize then
  n.WindowState := wsMaximized;
  n.ShowModal;
  if n.ModalResult = mrOk then
  begin
     ef.KeyValue := n.FDDataQr.FieldByName('id').AsInteger;
     ef.Refresh;
     result := true;
  end;
  n.Free;
end;


function CreateUIN(urn, docnumber: string; docdate: TDateTime): string;
var fs: TFormatSettings; s: string;
begin
  fs.ShortDateFormat := 'yymmdd';
  s := RightStr('00000000'+IntToStr(StrToInt('$'+urn)),8)+DateToStr(docdate, fs)+RightStr('0000000000'+docnumber,10);
  //s := '031972672204200026280001';
  result := s+GetUNIFOControl(s);
end;


function GetUNIFOControl(value: string): string;
var sum, res, len, i, j: integer; currentSymbol: string;
begin

   result := '';
   if value ='' then exit;

   sum := 0;

   value := AnsiUpperCase(trim(value));
   len := Length(value);

   j := 1;

   for i := 1 to len do
   begin

       currentSymbol := Copy(value, i, 1);
       sum := sum + StrToInt(currentSymbol)*j;

       j := j+1;

       if j = 11 then j := 1;

   end;

   res := sum mod 11;

   if res <10 then
   begin
     result := IntToStr(res);
     exit;
   end;

   j := 3;

   sum := 0;

   for i := 1 to len do
   begin

       currentSymbol := Copy(value, i, 1);
       sum := sum+StrToInt(currentSymbol)*j;

       j := j+1;

       if j = 11 then j := 1;

   end;

   res := sum mod 11;

   if res=10 then
   begin
     res := 0;
   end;

  result := IntToStr(res);

end;



end.
