inherited FormEditSubDiv: TFormEditSubDiv
  Caption = #1055#1086#1076#1088#1072#1079#1076#1077#1083#1077#1085#1080#1077
  ClientHeight = 259
  ClientWidth = 481
  ParentFont = False
  Font.Height = -13
  ExplicitWidth = 495
  ExplicitHeight = 298
  TextHeight = 17
  inherited plBottom: TPanel
    Top = 218
    Width = 481
    TabOrder = 3
    ExplicitTop = 218
    ExplicitWidth = 479
    inherited btnCancel: TButton
      Left = 365
      ExplicitLeft = 363
    end
    inherited btnOk: TButton
      Left = 246
      ParentFont = True
      ExplicitLeft = 244
    end
  end
  inherited edCode: TDBEditEh
    Height = 25
    ParentFont = True
    TabOrder = 0
    ExplicitHeight = 25
  end
  inherited edName: TDBEditEh
    Height = 25
    ParentFont = True
    TabOrder = 1
    ExplicitHeight = 25
  end
  object edOKTMO: TDBEditEh [3]
    Left = 8
    Top = 165
    Width = 457
    Height = 25
    ControlLabel.Width = 47
    ControlLabel.Height = 18
    ControlLabel.Caption = #1054#1050#1058#1052#1054
    ControlLabel.Font.Charset = DEFAULT_CHARSET
    ControlLabel.Font.Color = clWindowText
    ControlLabel.Font.Height = -15
    ControlLabel.Font.Name = 'Calibri'
    ControlLabel.Font.Style = []
    ControlLabel.ParentFont = False
    ControlLabel.Visible = True
    DataField = 'oktmo'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 2
    Visible = True
  end
  inherited dsLocal: TDataSource
    Left = 80
    Top = 216
  end
  inherited qrAux: TFDQuery
    Left = 32
    Top = 216
  end
end
