unit users;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, FireDAC.Stan.Intf, FireDAC.Stan.Param,
  FireDAC.Phys.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Vcl.Menus, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.Buttons, PngSpeedButton,
  EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh, Vcl.ExtCtrls, MemTableDataEh,
  MemTableEh, Vcl.StdCtrls;

type
  TFormUsers = class(TFormGrid)
    procedure FDDataQrAfterEdit(DataSet: TDataSet);
  private
    { Private declarations }
  public
    procedure Init; override;
  end;

var
  FormUsers: TFormUsers;

implementation

{$R *.dfm}

uses EditUser;

procedure TFormUsers.FDDataQrAfterEdit(DataSet: TDataSet);
begin
  inherited;
  if DataSet.FieldByName('password_changed').AsBoolean then
  DataSet.FieldByName('password').AsString := '000000000000000000';
end;

procedure TFormUsers.Init;
begin
  inherited;
  self.FormEditLocal := FormEditUser;
end;


end.
