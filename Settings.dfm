inherited FormSettings: TFormSettings
  Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1072' '#1088#1072#1073#1086#1095#1077#1075#1086' '#1084#1077#1089#1090#1072
  ClientHeight = 670
  ClientWidth = 914
  ExplicitWidth = 928
  ExplicitHeight = 709
  TextHeight = 16
  object lbMessage: TLabel [0]
    Left = 479
    Top = 32
    Width = 149
    Height = 16
    Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1080' '#1089#1086#1093#1088#1072#1085#1077#1085#1099
    StyleElements = [seClient, seBorder]
  end
  object laDefaultAccount: TDBSQLLookUp [1]
    Left = 16
    Top = 110
    Width = 273
    Height = 24
    ControlLabel.Width = 197
    ControlLabel.Height = 16
    ControlLabel.Caption = #1051#1080#1094#1077#1074#1086#1081' '#1089#1095#1077#1090' '#1087#1086' '#1091#1084#1086#1083#1095#1072#1085#1080#1102
    ControlLabel.Visible = True
    DataField = 'default_account_id'
    DataSource = dsData
    DynProps = <>
    EditButtons = <
      item
      end>
    TabOrder = 1
    Visible = True
    OnChange = laDefaultAccountChange
    SqlSet = dm.ssAccounts
    KeyValue = '16'
    RowCount = 0
    TextEditing = False
    OnKeyValueChange = laDefaultAccountChange
  end
  object edOKTMO: TDBEditEh [2]
    Left = 16
    Top = 178
    Width = 273
    Height = 24
    ControlLabel.Width = 48
    ControlLabel.Height = 16
    ControlLabel.Caption = #1054#1050#1058#1052#1054
    ControlLabel.Visible = True
    DataField = 'oktmo'
    DataSource = dsData
    DynProps = <>
    EditButtons = <>
    TabOrder = 2
    Visible = True
    OnChange = laDefaultAccountChange
  end
  object edJDNumber: TDBEditEh [3]
    Left = 16
    Top = 246
    Width = 273
    Height = 24
    ControlLabel.Width = 177
    ControlLabel.Height = 16
    ControlLabel.Caption = #1053#1086#1084#1077#1088' '#1089#1091#1076#1077#1073#1085#1086#1075#1086' '#1091#1095#1072#1089#1090#1082#1072
    ControlLabel.Visible = True
    DataField = 'jdnumber'
    DataSource = dsData
    DynProps = <>
    EditButtons = <>
    TabOrder = 3
    Visible = True
    OnChange = laDefaultAccountChange
  end
  object edJPNumber: TDBEditEh [4]
    Left = 16
    Top = 314
    Width = 273
    Height = 24
    ControlLabel.Width = 86
    ControlLabel.Height = 16
    ControlLabel.Caption = #1053#1086#1084#1077#1088' '#1089#1091#1076#1100#1080
    ControlLabel.Visible = True
    DataField = 'jpnumber'
    DataSource = dsData
    DynProps = <>
    EditButtons = <>
    TabOrder = 4
    Visible = True
    OnChange = laDefaultAccountChange
  end
  object btSave: TButton [5]
    Left = 432
    Top = 73
    Width = 249
    Height = 75
    Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
    Enabled = False
    TabOrder = 6
    OnClick = btSaveClick
  end
  object edURN: TDBEditEh [6]
    Left = 16
    Top = 42
    Width = 273
    Height = 24
    ControlLabel.Width = 102
    ControlLabel.Height = 16
    ControlLabel.Caption = #1059#1056#1053' '#1091#1095#1072#1089#1090#1085#1080#1082#1072
    ControlLabel.Visible = True
    DataField = 'urn'
    DataSource = dsData
    DynProps = <>
    EditButtons = <>
    TabOrder = 0
    Visible = True
    OnChange = laDefaultAccountChange
  end
  object luSubDiv: TDBSQLLookUp [7]
    Left = 16
    Top = 383
    Width = 273
    Height = 24
    ControlLabel.Width = 206
    ControlLabel.Height = 16
    ControlLabel.Caption = #1055#1086#1076#1088#1072#1079#1076#1077#1083#1077#1085#1080#1077' '#1087#1086' '#1091#1084#1086#1083#1095#1072#1085#1080#1102
    ControlLabel.Visible = True
    DataField = 'default_subdiv_id'
    DataSource = dsData
    DynProps = <>
    EditButtons = <
      item
      end>
    TabOrder = 5
    Visible = True
    OnChange = laDefaultAccountChange
    SqlSet = dm.ssSubdivs
    KeyValue = Null
    RowCount = 0
    TextEditing = False
    OnKeyValueChange = laDefaultAccountChange
  end
  inherited FDUpdateSet: TFDUpdateSQL
    ModifySQL.Strings = (
      'UPDATE public.settings SET '
      'default_account_id = :default_account_id, '
      'default_subdiv_id = :default_subdiv_id, '
      'oktmo = :oktmo, '
      'jdnumber = :jdnumber, '
      'jpnumber = :jpnumber,'
      'urn = :urn;')
    FetchRowSQL.Strings = (
      'select * from settings')
    Left = 560
    Top = 212
  end
  inherited FDDataQr: TFDQuery
    SQL.Strings = (
      'select * from settings')
    Left = 640
    Top = 212
  end
  inherited dsData: TDataSource
    Left = 720
    Top = 216
  end
end
