inherited FormEditUser: TFormEditUser
  Caption = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1100
  ClientHeight = 306
  ClientWidth = 522
  ParentFont = False
  Font.Height = -13
  ExplicitWidth = 536
  ExplicitHeight = 345
  TextHeight = 17
  inherited plBottom: TPanel
    Top = 265
    Width = 522
    ExplicitTop = 265
    ExplicitWidth = 520
    inherited btnCancel: TButton
      Left = 406
      ExplicitLeft = 404
    end
    inherited btnOk: TButton
      Left = 287
      ParentFont = True
      ExplicitLeft = 285
    end
  end
  inherited edCode: TDBEditEh
    Height = 25
    ControlLabel.Width = 38
    ControlLabel.Caption = #1051#1086#1075#1080#1085
    ControlLabel.ExplicitWidth = 38
    DataField = 'user_login'
    ParentFont = True
    ExplicitHeight = 25
  end
  inherited edName: TDBEditEh
    Height = 25
    ControlLabel.Width = 118
    ControlLabel.Caption = #1048#1084#1103' '#1087#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1103
    ControlLabel.ExplicitWidth = 118
    DataField = 'user_name'
    ParentFont = True
    ExplicitHeight = 25
  end
  object DBCheckBoxEh1: TDBCheckBoxEh [3]
    Left = 272
    Top = 35
    Width = 129
    Height = 17
    Caption = #1040#1076#1084#1080#1085#1080#1089#1090#1088#1072#1090#1086#1088
    DataField = 'isAdmin'
    DataSource = dsLocal
    DynProps = <>
    TabOrder = 3
  end
  object cbBlocked: TDBCheckBoxEh [4]
    Left = 8
    Top = 219
    Width = 129
    Height = 17
    Caption = #1047#1072#1073#1083#1086#1082#1080#1088#1086#1074#1072#1085
    DataField = 'isBlocked'
    DataSource = dsLocal
    DynProps = <>
    TabOrder = 4
  end
  object edPassword: TDBEditEh [5]
    Left = 8
    Top = 158
    Width = 241
    Height = 25
    ControlLabel.Width = 47
    ControlLabel.Height = 18
    ControlLabel.Caption = #1055#1072#1088#1086#1083#1100
    ControlLabel.Font.Charset = DEFAULT_CHARSET
    ControlLabel.Font.Color = clWindowText
    ControlLabel.Font.Height = -15
    ControlLabel.Font.Name = 'Calibri'
    ControlLabel.Font.Style = []
    ControlLabel.ParentFont = False
    ControlLabel.Visible = True
    DataField = 'password'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    PasswordChar = '*'
    TabOrder = 5
    Visible = True
  end
  inherited dsLocal: TDataSource
    Left = 192
    Top = 240
  end
  inherited qrAux: TFDQuery
    Left = 240
    Top = 232
  end
end
