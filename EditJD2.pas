unit EditJD2;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  DBCtrlsEh, DBSQLLookUp, Vcl.StdCtrls, Vcl.Mask, Vcl.Buttons, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.ExtCtrls, functions, persons, services;

type
  TFormEditJD2 = class(TFormEdit)
    sbPersons: TSpeedButton;
    sbServices: TSpeedButton;
    edDocNum: TDBEditEh;
    dtDocDate: TDBDateTimeEditEh;
    laService: TDBSQLLookUp;
    laPerson: TDBSQLLookUp;
    meNote: TDBMemoEh;
    edUIN: TDBEditEh;
    laAccount: TDBSQLLookUp;
    neSumma: TDBNumberEditEh;
    edSubDiv: TDBSQLLookUp;
    edOKTMO: TDBEditEh;
    dtEif: TDBDateTimeEditEh;
    procedure sbPersonsClick(Sender: TObject);
    procedure sbServicesClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditJD2: TFormEditJD2;

implementation

{$R *.dfm}

procedure TFormEditJD2.sbPersonsClick(Sender: TObject);
begin
  SFDE(TFormPersons, self.laPerson);
end;

procedure TFormEditJD2.sbServicesClick(Sender: TObject);
begin
  SFDE(TFormServices, self.laService);
end;

end.
