unit EditReestr;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.StdCtrls, Vcl.ExtCtrls,
  DBCtrlsEh, Vcl.Mask;

type
  TFormEditReestr = class(TFormEdit)
    edDocNum: TDBEditEh;
    dtDocDate: TDBDateTimeEditEh;
    procedure dtDocDateChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditReestr: TFormEditReestr;

implementation

uses reestrs;

{$R *.dfm}

procedure TFormEditReestr.dtDocDateChange(Sender: TObject);
begin
  if current_mode = 'insert' then
  begin
    with FormReestrs do
    begin
      qrAux.Close;
      qrAux.Params.ParamByName('docdate').Value := dtDocDate.Value;
      qrAux.Open;
      if FDDataQr.State in [dsEdit, dsInsert] then
      begin
        FDDataQr.FieldByName('reestr_num').Value := qrAux.Fields[0].AsString;
      end;
    end;
  end;
end;

end.
