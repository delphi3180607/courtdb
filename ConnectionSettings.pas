unit ConnectionSettings;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, FileCtrl, xmldom,
  XMLIntf, msxmldom, XMLDoc, Vcl.Mask;

type
  TFormConnectionSettings = class(TForm)
    lePassword: TLabeledEdit;
    leLogin: TLabeledEdit;
    cmbDBName: TComboBox;
    Label2: TLabel;
    cmbServer: TComboBox;
    Label1: TLabel;
    plBottom: TPanel;
    btOk: TSpeedButton;
    btCancel: TSpeedButton;
    lePort: TLabeledEdit;
    procedure btOkClick(Sender: TObject);
    procedure btCancelClick(Sender: TObject);
  private
    function GetParameterValue(paramName: string; var sl: TStringList): string;
  public
    { Public declarations }
  end;

var
  FormConnectionSettings: TFormConnectionSettings;

implementation

{$R *.dfm}

function TFormConnectionSettings.GetParameterValue(paramName: string; var sl: TStringList): string;
var i,p: integer; s: string;
begin

  result := '';

  for  i := 1 to sl.Count -1 do
  begin
     if pos(paramName, trim(sl.Strings[i]))>0 then
     begin
       p:= pos('=',sl.Strings[i]);
       s := copy(sl.Strings[i],p+1,255);
       result := trim(s);
       exit;
     end;
  end;

end;

procedure TFormConnectionSettings.btOkClick(Sender: TObject);
begin
  self.ModalResult := mrOk;
end;

procedure TFormConnectionSettings.btCancelClick(Sender: TObject);
begin
  self.ModalResult := mrCancel;
end;

end.
