unit EditSubDiv;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, editcodename, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.StdCtrls, Vcl.Mask,
  DBCtrlsEh, Vcl.ExtCtrls;

type
  TFormEditSubDiv = class(TFormEditCodeName)
    edOKTMO: TDBEditEh;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditSubDiv: TFormEditSubDiv;

implementation

{$R *.dfm}

end.
