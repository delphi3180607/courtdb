inherited FormServices: TFormServices
  Caption = #1059#1089#1083#1091#1075#1080
  ClientWidth = 932
  ExplicitWidth = 946
  TextHeight = 16
  inherited plMain: TPanel
    Width = 932
    ExplicitWidth = 930
    inherited dgMain: TDBGridEh
      Width = 932
      OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghRowHighlight, dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove]
      Columns = <
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'code'
          Footers = <>
          Title.Caption = #1050#1086#1076
          Width = 167
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'name'
          Footers = <>
          Title.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
          Width = 263
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'kbk'
          Footers = <>
          Title.Caption = #1050#1041#1050
          Width = 217
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'note'
          Footers = <>
          Title.Caption = #1053#1072#1079#1085#1072#1095#1077#1085#1080#1077' '#1087#1083#1072#1090#1077#1078#1072
          Width = 551
        end>
    end
    inherited plTop: TPanel
      Width = 932
      ExplicitWidth = 930
      inherited sbExcel: TPngSpeedButton
        ExplicitLeft = 230
      end
    end
    inherited plBottom: TPanel
      Width = 932
      ExplicitWidth = 930
      inherited btnCancel: TButton
        Left = 816
        ExplicitLeft = 814
      end
      inherited btnOk: TButton
        Left = 697
        ExplicitLeft = 695
      end
    end
  end
  inherited FDUpdateSet: TFDUpdateSQL
    InsertSQL.Strings = (
      'insert into services (code, name, note, kbk)'
      'values (:code, :name, :note, :kbk) returning id;')
    ModifySQL.Strings = (
      'update services set '
      'code = :code, '
      'name = :name,'
      'note = :note,'
      'kbk = :kbk'
      'where id = :id;')
    DeleteSQL.Strings = (
      'delete from services where id = :id;')
    FetchRowSQL.Strings = (
      'select * from services where id = :id;')
  end
  inherited FDDataQr: TFDQuery
    SQL.Strings = (
      'select * from services order by code')
  end
end
