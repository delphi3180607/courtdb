unit SQLQuery;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.StdCtrls, Vcl.ExtCtrls,
  DBGridEhGrouping, ToolCtrlsEh, DBGridEhToolCtrls, DynVarsEh, EhLibVCL,
  GridsEh, DBAxisGridsEh, DBGridEh, SynEdit, Vcl.ComCtrls;

type
  TFormSQLQuery = class(TFormEdit)
    plTool: TPanel;
    btExec: TButton;
    btSelect: TButton;
    se: TSynEdit;
    Splitter1: TSplitter;
    pc: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    dg: TDBGridEh;
    meLog: TMemo;
    btFile: TButton;
    od: TOpenDialog;
    procedure btSelectClick(Sender: TObject);
    procedure btExecClick(Sender: TObject);
    procedure btFileClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormSQLQuery: TFormSQLQuery;

implementation

{$R *.dfm}

procedure TFormSQLQuery.btExecClick(Sender: TObject);
var sl: TStringList; i: integer;
begin
  pc.ActivePageIndex := 1;
  meLog.Lines.Clear;

  sl := TStringList.Create;
  sl.QuoteChar := '~';
  sl.StrictDelimiter := true;
  sl.Delimiter := ';';
  sl.DelimitedText := se.Text;

  for i  := 0 to sl.Count-1 do
  begin

      qrAux.Close;
      qrAux.SQL.Clear;
      qrAux.SQL.Text := sl[i];
      if trim(qrAux.SQL.Text)<>'' then
      begin
        try
          qrAux.ExecSQL;
        except
          on E:Exception do
          begin
            meLog.Lines.Add('������: '+E.Message);
          end;
        end;
      end;

  end;

end;

procedure TFormSQLQuery.btFileClick(Sender: TObject);
begin
  if od.Execute then
  begin
    se.Lines.LoadFromFile(od.FileName);
  end;
end;

procedure TFormSQLQuery.btSelectClick(Sender: TObject);
begin
  pc.ActivePageIndex := 0;
  meLog.Lines.Clear;
  qrAux.Close;
  qrAux.SQL.Clear;
  qrAux.SQL.Text := se.Text;
  try
    pc.ActivePageIndex := 0;
    qrAux.Active := true;
    meLog.Lines.Add('������ �������� �������.');
  except
    on E:Exception do
    begin
      pc.ActivePageIndex := 1;
      meLog.Lines.Add('������: '+E.Message);
    end;
  end;
end;

end.
