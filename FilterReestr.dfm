inherited FormFilterReestr: TFormFilterReestr
  Caption = #1054#1090#1073#1086#1088' '#1088#1077#1077#1089#1090#1088#1086#1074
  ClientHeight = 296
  ClientWidth = 474
  ExplicitWidth = 488
  ExplicitHeight = 335
  TextHeight = 16
  inherited plBottom: TPanel
    Top = 255
    Width = 474
    TabOrder = 2
    ExplicitTop = 255
    ExplicitWidth = 472
    inherited btnCancel: TButton
      Left = 358
      ExplicitLeft = 356
    end
    inherited btnOk: TButton
      Left = 239
      Caption = #1054#1090#1086#1073#1088#1072#1090#1100
      ExplicitLeft = 237
    end
  end
  object cbSelectPeriod: TDBComboBoxEh [1]
    AlignWithMargins = True
    Left = 120
    Top = 20
    Width = 322
    Height = 24
    ControlLabel.Width = 101
    ControlLabel.Height = 16
    ControlLabel.Caption = #1055#1077#1088#1080#1086#1076' '#1086#1090#1073#1086#1088#1072
    ControlLabel.Visible = True
    ControlLabelLocation.Spacing = 10
    ControlLabelLocation.Position = lpLeftTextBaselineEh
    DynProps = <>
    EditButtons = <>
    Items.Strings = (
      #1047#1072' '#1090#1077#1082#1091#1097#1080#1081' '#1084#1077#1089#1103#1094
      #1047#1072' '#1090#1077#1082#1091#1097#1080#1081' '#1080' '#1087#1088#1077#1076#1099#1076#1091#1097#1080#1081' '#1084#1077#1089#1103#1094#1099
      #1047#1072' '#1087#1086#1089#1083#1077#1076#1085#1080#1077' 6 '#1084#1077#1089#1103#1094#1077#1074
      #1047#1072' '#1087#1086#1089#1083#1077#1076#1085#1080#1077' 12 '#1084#1077#1089#1103#1094#1077#1074
      #1055#1088#1086#1080#1079#1074#1086#1083#1100#1085#1099#1081' '#1087#1077#1088#1080#1086#1076)
    KeyItems.Strings = (
      '0'
      '1'
      '2'
      '3'
      '4')
    TabOrder = 0
    Text = #1047#1072' '#1090#1077#1082#1091#1097#1080#1081' '#1080' '#1087#1088#1077#1076#1099#1076#1091#1097#1080#1081' '#1084#1077#1089#1103#1094#1099
    Visible = True
    OnChange = cbSelectPeriodChange
  end
  object GroupBox1: TGroupBox [2]
    Left = 120
    Top = 62
    Width = 322
    Height = 171
    Caption = #1055#1088#1086#1080#1079#1074#1086#1083#1100#1085#1099#1081' '#1087#1077#1088#1080#1086#1076
    TabOrder = 1
    object dtBegin: TDBDateTimeEditEh
      Left = 168
      Top = 48
      Width = 121
      Height = 24
      ControlLabel.Width = 86
      ControlLabel.Height = 16
      ControlLabel.Caption = #1044#1072#1090#1072' '#1085#1072#1095#1072#1083#1072
      ControlLabel.Visible = True
      ControlLabelLocation.Spacing = 10
      ControlLabelLocation.Position = lpLeftTextBaselineEh
      DynProps = <>
      EditButtons = <>
      Kind = dtkDateEh
      TabOrder = 0
      Visible = True
      OnChange = dtBeginChange
    end
    object dtEnd: TDBDateTimeEditEh
      Left = 168
      Top = 104
      Width = 121
      Height = 24
      ControlLabel.Width = 110
      ControlLabel.Height = 16
      ControlLabel.Caption = #1044#1072#1090#1072' '#1086#1082#1086#1085#1095#1072#1085#1080#1103
      ControlLabel.Visible = True
      ControlLabelLocation.Spacing = 10
      ControlLabelLocation.Position = lpLeftTextBaselineEh
      DynProps = <>
      EditButtons = <>
      Kind = dtkDateEh
      TabOrder = 1
      Visible = True
      OnChange = dtBeginChange
    end
  end
  inherited dsLocal: TDataSource
    Left = 24
    Top = 120
  end
  inherited qrAux: TFDQuery
    Left = 21
    Top = 180
  end
end
