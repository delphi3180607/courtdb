unit edit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, Data.DB,
  Data.Win.ADODB, MemTableEh, Vcl.CheckLst, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TFormEdit = class(TForm)
    plBottom: TPanel;
    btnCancel: TButton;
    btnOk: TButton;
    dsLocal: TDataSource;
    qrAux: TFDQuery;
    procedure FormCreate(Sender: TObject);
  private
    //--
  protected
    flag: boolean;
  public
    ParentFormGrid: TForm;
    current_id: integer;
    current_mode: string;
    classif_panel: TWinControl;
  end;

var
  FormEdit: TFormEdit;

implementation

{$R *.dfm}

uses main, dmu;

procedure TFormEdit.FormCreate(Sender: TObject);
begin
 current_id := 0;
 flag := false;
end;

end.
