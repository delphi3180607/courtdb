unit dmu;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.PG,
  FireDAC.Phys.PGDef, FireDAC.VCLUI.Wait, Data.DB, FireDAC.Comp.Client,
  FireDAC.VCLUI.Login, FireDAC.Comp.UI, DBSQLLookUp, FireDAC.Stan.Param,
  FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt, FireDAC.Comp.DataSet;

type
  Tdm = class(TDataModule)
    FDConnMain: TFDConnection;
    loginDialog: TFDGUIxLoginDialog;
    ssAccounts: TDBXLookUpSqlSet;
    ssPersons: TDBXLookUpSqlSet;
    ssServices: TDBXLookUpSqlSet;
    qrCommon: TFDQuery;
    ssSubdivs: TDBXLookUpSqlSet;
    qrReestrOp: TFDQuery;
    qrUpdateDataBase: TFDQuery;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dm: Tdm;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

end.
