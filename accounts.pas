unit accounts;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, FireDAC.Stan.Intf, FireDAC.Stan.Param,
  FireDAC.Phys.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Vcl.Menus, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.Buttons, PngSpeedButton,
  EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh, Vcl.ExtCtrls, MemTableDataEh,
  MemTableEh, Vcl.StdCtrls;

type
  TFormAccounts = class(TFormGrid)
  private
    { Private declarations }
  public
    procedure Init; override;
    procedure ImportSheet(data:Variant; DimX:integer; DimY:integer); override;
  end;

var
  FormAccounts: TFormAccounts;

implementation

{$R *.dfm}

uses editcodename, editaccount;

procedure TFormAccounts.Init;
begin
  inherited;
  self.FormEditLocal := FormEditAccount;
end;


procedure TFormAccounts.ImportSheet(data:Variant; DimX:integer; DimY:integer);
var i, n_code, n_name, n_note: integer; code, name, note: string;
begin

  for i := 1 to DimX do
  begin

    if data[2,i]='����� �����' then
    n_code := i;

    if data[2,i]='��������' then
    n_name := i;

  end;

  qrAux.Close;
  qrAux.SQL.Clear;
  qrAux.SQL.Add('insert into accounts (code, name)');
  qrAux.SQL.Add('select :code, :name where not exists (select 1 from accounts a where a.code = :code1); ');

  for i := 3 to DimY do
  begin

      code := data[i,n_code];
      name := data[i,n_name];

      if trim(code) = '' then continue;

      qrAux.Params.ParamByName('code').Value := code;
      qrAux.Params.ParamByName('code1').Value := code;
      qrAux.Params.ParamByName('name').Value := name;

      try
        qrAux.ExecSQL;
      except
        on E : Exception do
        begin
          ShowMessage('������ ��� �������: '+E.Message);
          exit;
        end;
      end;
  end;

end;


end.
