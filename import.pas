unit import;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  Vcl.OleServer, Excel2000, Vcl.StdCtrls, Vcl.Mask, DBCtrlsEh, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.ExtCtrls, ComObj, ExcelXP;

type
  TFormImport = class(TFormEdit)
    edFileName: TDBEditEh;
    ExcelApplication1: TExcelApplication;
    od: TOpenDialog;
    procedure edFileNameEditButtons0Click(Sender: TObject;
      var Handled: Boolean);
    procedure btnOkClick(Sender: TObject);
  private
    { Private declarations }
  public
    FData: Variant;
    DimY : integer;
    DimX : integer;
  end;

var
  FormImport: TFormImport;

implementation

{$R *.dfm}

procedure TFormImport.btnOkClick(Sender: TObject);
const
  xlCellTypeLastCell = $0000000B;
var
  IIndex : OleVariant;
  WorkBk : Variant;
  WorkSheet : Variant;
  XLApp  : Variant;
  fileName : string;
begin
  fileName := edFileName.Text;
  Screen.Cursor:=crHOURGLASS;
  try
    IIndex := 1;
    XLApp  := CreateOleObject('Excel.Application');
    // ��������� ���� Excel
    XLApp.WorkBooks.Open(FileName,EmptyParam,true,EmptyParam,EmptyParam, EmptyParam,EmptyParam,
    EmptyParam,EmptyParam,EmptyParam,EmptyParam,EmptyParam,EmptyParam,0);
    WorkBk := XLApp.WorkBooks.Item[IIndex];
    WorkSheet := WorkBk.WorkSheets[1];
    // ��� ����, ����� ����� ������� ����� (WorkSheet), �� ���� ����� �����
    // � �������, �� ������������ ��������� �������� ������
    WorkSheet.Cells.SpecialCells(xlCellTypeLastCell,EmptyParam).Activate;
    // �������� �������� ��������� ������
    DimX := XLApp.ActiveCell.Column;
    // �������� �������� ��������� �������
    DimY := XLApp.ActiveCell.Row;
    // ���������  Variant ���������� ����� �  Delphi Variant Matrix
    FData := XLApp.Range['A1',XLApp.Cells.Item[DimY,DimX]].Value;
  finally
    // ��������� Excel � ������������� �� �������
    XLApp.Quit;
    XLApp := null;
    Screen.Cursor:=crDEFAULT;
  end
end;

procedure TFormImport.edFileNameEditButtons0Click(Sender: TObject;
  var Handled: Boolean);
begin
  if od.Execute then
  begin
    self.edFileName.Text := od.FileName;
  end;
end;

end.
